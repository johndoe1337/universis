import { FormioExamplesModule } from './formio-examples.module';

describe('FormioExamplesModule', () => {
  let formioExamplesModule: FormioExamplesModule;

  beforeEach(() => {
    formioExamplesModule = new FormioExamplesModule();
  });

  it('should create an instance', () => {
    expect(formioExamplesModule).toBeTruthy();
  });
});
