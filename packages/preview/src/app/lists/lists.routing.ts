import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {GroupListsComponent} from './components/group-lists/group-lists.component';
import {SimpleListComponent} from './components/simple-list/simple-list.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'group-lists'
  },
  {
    path: 'group-lists',
    component: GroupListsComponent,
    data: {
      title: 'Group List'
    }
  },
  {
    path: 'simple-list',
    component: SimpleListComponent,
    data: {
      title: 'Simple List'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListsRoutingModule {
  constructor() {}
}
