import { Component } from '@angular/core';
import { RouterModalOkCancel } from '@universis/common/routing';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
    selector: 'app-ok-cancel-action',
    templateUrl: '/ok-cancel-action.component.html'
})
export class OkCancelActionComponent extends RouterModalOkCancel {

    public lastError: any;
    // set ok button test
    public okButtonText = 'Proceed';
    // set cancel button text
    public cancelButtonText = 'Dismiss';

    constructor(protected router: Router, protected activatedRoute: ActivatedRoute) {
        // call super constructor
        super(router, activatedRoute);
    }

    async ok() {
        try {
            // noinspection ExceptionCaughtLocallyJS
            throw new Error('An error occurred while sending data.');
            // do something and close
            // noinspection UnreachableCodeJS
            return this.close();
        } catch (err) {
            this.lastError = err;
        }
    }

    async cancel() {
        // close
        return this.close();
    }
}
