import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RouterModalSimpleComponent} from './router-modal-simple.component';
import {OkCancelActionComponent} from './ok-cancel-action.component';
import {YesNoComponent} from './yes-no.component';
import {ModalEditComponent} from './modal-edit-form.component';
import {EditForm1Component} from './edit-form1.component';
import {RouterModalAdvancedComponent} from './router-modal-advanced.component';
import {AbortRetryIgnoreComponent} from './abort-retry-ignore.component';

const routes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        redirectTo: 'simple'
    },
    {
        path: 'simple',
        component: RouterModalSimpleComponent,
        data: {
            title: 'Simple Router Modals'
        },
        children: [
            {
                path: 'ok-cancel',
                pathMatch: 'full',
                component: OkCancelActionComponent,
                outlet: 'modal'
            },
            {
                path: 'yes-no',
                pathMatch: 'full',
                component: YesNoComponent,
                outlet: 'modal',
                data: {
                    yesButtonText: 'Complete'
                }
            },
            {
                path: 'abort-retry-ignore',
                pathMatch: 'full',
                component: AbortRetryIgnoreComponent,
                outlet: 'modal'
            }
        ]
    },
    {
        path: 'advanced',
        component: RouterModalAdvancedComponent,
        data: {
            title: 'Advanced Router Modals'
        },
        children: [
            {
                path: ':model/:id/edit',
                pathMatch: 'full',
                component: ModalEditComponent,
                outlet: 'modal'
            },
            {
                path: ':model/:id/form/edit',
                pathMatch: 'full',
                component: EditForm1Component,
                outlet: 'modal'
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    declarations: []
})
export class UseRouterModalsRoutingModule {
}
