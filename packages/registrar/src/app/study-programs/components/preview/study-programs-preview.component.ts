import {Component, OnDestroy, OnInit} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs';
import {AppEventService, ErrorService} from '@universis/common';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-study-programs-preview',
  templateUrl: './study-programs-preview.component.html'
})
export class StudyProgramsPreviewComponent implements OnInit, OnDestroy {
  private paramSubscription: Subscription;
  private fragmentSubscription: Subscription;
  private changeSubscription: Subscription;
  public studySpecializations: any[];
  private studyProgram;

  constructor(private _context: AngularDataContext,
              private _activatedRoute: ActivatedRoute,
              private _errorService: ErrorService,
              public _translateService: TranslateService,
              private _appEvent: AppEventService) {
    //
  }

  async ngOnInit() {
    this.paramSubscription = this._activatedRoute.params.subscribe(async (params) => {
      this.studyProgram = params.id;
      this.studySpecializations = await this.getSpecialties();
    });
    this.changeSubscription = this._appEvent.change.subscribe(async (event) => {
      if (event && event.model === 'StudyProgramSpecialties') {
        this.studySpecializations = await this.getSpecialties();
      }
    });
    this.fragmentSubscription = this._activatedRoute.fragment.subscribe(async (fragment) => {
      if (fragment && fragment === 'reload') {
        this.studySpecializations = await this.getSpecialties();
      }
    });
  }

  async getSpecialties() {
    return this._context.model('StudyProgramSpecialties')
      .where('studyProgram').equal(this.studyProgram)
      .orderBy('specialty')
      .getItems();
  }


  ngOnDestroy(): void {
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
    if (this.changeSubscription) {
      this.changeSubscription.unsubscribe();
    }
  }

}
