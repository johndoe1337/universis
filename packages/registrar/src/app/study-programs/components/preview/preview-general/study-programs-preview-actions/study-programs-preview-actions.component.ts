import {Component, OnDestroy, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import {Subscription} from 'rxjs';
import {AppEventService} from '@universis/common';

@Component({
  selector: 'app-study-programs-preview-actions',
  templateUrl: './study-programs-preview-actions.component.html'
})
export class StudyProgramsPreviewActionsComponent implements OnInit, OnDestroy {

  public actions: any;
  public studyProgramID: any;
  private subscription: Subscription;
  private changeSubscription: Subscription;

  constructor( private _activatedRoute: ActivatedRoute,
               private _context: AngularDataContext,
               private _appEvent: AppEventService) { }

  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.studyProgramID = params.id;
      this.actions =  await this.loadActions();
    });
    this.changeSubscription = this._appEvent.change.subscribe(async (event) => {
      if (event && event.model === 'StudyPrograms') {
        this.actions = await this.loadActions();
      }
    });
  }

  async loadActions() {
    return this._context.model('CopyProgramActions')
      .where('studyProgram').equal(this._activatedRoute.snapshot.params.id)
      .expand('createdBy', 'actionStatus', 'result')
      .orderByDescending('dateCreated')
      .prepare()
      .take(-1)
      .getItems();
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.changeSubscription) {
      this.changeSubscription.unsubscribe();
    }
  }

}
