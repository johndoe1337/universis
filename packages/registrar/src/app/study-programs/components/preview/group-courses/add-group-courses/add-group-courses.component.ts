import {Component, Input, OnInit, OnDestroy, ViewChild} from '@angular/core';
import {RouterModalOkCancel} from '@universis/common/routing';
import {Observable, Subscription} from 'rxjs';
import {ActivatedRoute, NavigationExtras, Router} from '@angular/router';
import {ErrorService, LoadingService, ModalService, ToastService} from '@universis/common';
import {AppEventService} from '@universis/common';
import {TranslateService} from '@ngx-translate/core';
import {AdvancedFormsService} from '@universis/forms';
import {AngularDataContext} from '@themost/angular';
import {SelectCourseComponent} from '../../../../../courses/components/select-course/select-course-component';
import * as GROUP_COURSE_EXEMPTION from './add-group-courses-table.config.list.json';
import * as REMOVE_GROUP_COURSE from './remove-group-courses-table.config.list.json';
import {AdvancedTableConfiguration} from '@universis/ngx-tables';
import {AdvancedFilterValueProvider} from '@universis/ngx-tables';

@Component({
  selector: 'app-add-group-courses',
  templateUrl: './add-group-courses.component.html'
})
export class AddGroupCoursesComponent extends RouterModalOkCancel implements OnInit, OnDestroy {
  @Input() execute: Observable<any>;
  public loading: boolean = false;
  public lastError;
  @ViewChild('selectComponent') selectComponent: SelectCourseComponent;
  @Input() items: Set<any> = new Set();
  @Input() programGroup: any;
  public config: any
  private selectedItemsSubscription: Subscription;
  @Input() program: any;
  @Input() removeCourses: boolean = true;

  constructor(router: Router,
              activatedRoute: ActivatedRoute,
              private _errorService: ErrorService,
              private _modalService: ModalService,
              private _appEvent: AppEventService,
              private _toastService: ToastService,
              private _loadingService: LoadingService,
              private _translateService: TranslateService,
              private _context: AngularDataContext,
              private _formService: AdvancedFormsService,
              private appFilter: AdvancedFilterValueProvider) {
    super(router, activatedRoute);
    this.modalClass = 'modal-lg';
    this.okButtonDisabled = true;
  }

  async ngOnInit(): Promise<any> {
    this.config = !this.removeCourses ? AdvancedTableConfiguration.cast(GROUP_COURSE_EXEMPTION) : AdvancedTableConfiguration.cast(REMOVE_GROUP_COURSE)
    if (!this.selectComponent.tableConfig || this.selectComponent.tableConfig.model !== 'StudyProgramCourses') {
      this.selectComponent.ngOnInit(GROUP_COURSE_EXEMPTION)
      this.selectComponent.tableConfig.defaults.filter = this.appFilter.buildFilter(this.selectComponent.tableConfig.defaults.filter);
    }
  }

  ok(): Promise<any> {
    try {
      return new Promise((resolve, reject) => {
        this.loading = false;
        this.lastError = null;
        // assign form attributes
        this.selectComponent.advancedTable.selected.forEach((item) => {
          const groupCourseData = {
              id: item.programCourseID,
              programGroup: !this.removeCourses ? this.programGroup : null,
              $state: 2
          };
          this.items.add(groupCourseData);
        });
        // execute add
        const executeSubscription = this.execute.subscribe((result) => {
          this.loading = false;
          executeSubscription.unsubscribe();
          // close modal
          if (this._modalService.modalRef) {
            this._modalService.modalRef.hide();
          }
          return resolve();
        }, (err) => {
          this.loading = false;
          // ensure that loading is hidden
          this._loadingService.hideLoading();
          // set last error
          this.lastError = err;
          return reject();
        });
      });
    } catch (err) {
      this.loading = false;
      this._loadingService.hideLoading();
      this.lastError = err;
    }
  }

  cancel(): Promise<any> {
    if (this.loading) {
      return;
    }
    // close
    if (this._modalService.modalRef) {
      return this._modalService.modalRef.hide();
    }
  }

  close(navigationExtras?: NavigationExtras): Promise<boolean> {
    this.ngOnDestroy();
    return super.close(navigationExtras);
  }

  ngOnDestroy(): void {
    if (this.selectedItemsSubscription) {
      this.selectedItemsSubscription.unsubscribe();
    }
  }

}
