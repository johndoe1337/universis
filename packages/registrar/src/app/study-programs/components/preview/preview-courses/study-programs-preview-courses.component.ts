import { Component, OnInit, ViewChild, Input, OnDestroy } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {
  AdvancedTableComponent,
  AdvancedTableConfiguration,
  AdvancedTableDataResult
} from '@universis/ngx-tables';
import { AngularDataContext } from '@themost/angular';
import * as STUDY_PROGRAMS_COURSES_LIST_CONFIG from './study-programs-preview-courses.config.list.json';
import {Observable, Subscription} from 'rxjs';
import {TranslateService} from '@ngx-translate/core';
import {AppEventService, DIALOG_BUTTONS, ErrorService, LoadingService, ModalService, UserActivityService} from '@universis/common';
import {EditCoursesComponent} from '../edit-courses/edit-courses.component';
import {AddCoursesComponent} from '../add-courses/add-courses.component';
import {AdvancedSearchFormComponent} from '@universis/ngx-tables';

@Component({
  selector: 'app-study-programs-preview-courses',
  templateUrl: './study-programs-preview-courses.component.html'
})
export class StudyProgramsPreviewCoursesComponent implements OnInit, OnDestroy {

  public readonly config = STUDY_PROGRAMS_COURSES_LIST_CONFIG;
  private dataSubscription: Subscription;
  private paramSubscription: Subscription;
  private fragmentSubscription: Subscription;
  private studyProgram: any;
  @Input() tableConfiguration: any;
  @Input() searchConfiguration: any;
  @ViewChild('courses') courses: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  public recordsTotal: any;
  private selectedItems = [];
  private specialization: any;
  private copiedRules = null;
  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _translateService: TranslateService,
              private _userActivityService: UserActivityService,
              private _router: Router,
              private _errorService: ErrorService,
              private _modalService: ModalService,
              private _loadingService: LoadingService,
              private _appEvent: AppEventService)  { }

  async ngOnInit() {

    this.dataSubscription = this._activatedRoute.data.subscribe(data => {
      // get query params
      if (this.paramSubscription) {
        this.paramSubscription.unsubscribe();
      }
      this.fragmentSubscription = this._activatedRoute.fragment.subscribe((fragment) => {
        if (fragment === 'reload') {
          this.courses.fetch();
        }
      });
      this.paramSubscription = this._activatedRoute.params.subscribe((params) => {
        if (data.tableConfiguration) {
          this.studyProgram = parseInt(params.id, 10) || 0;
          const queryParams = this._context.model('SpecializationCourses')
              .where('studyProgramCourse/studyProgram').equal(this.studyProgram)
              .and('specializationIndex').equal(params.specialization)
              .getParams();
          data.tableConfiguration.defaults.filter = queryParams.$filter;
          this.courses.config = AdvancedTableConfiguration.cast(data.tableConfiguration, true);
          this.courses.fetch(true);
        }
        if (data.searchConfiguration) {
          this.search.form = data.searchConfiguration;
          this.search.formComponent.formLoad.subscribe((res: any) => {
            Object.assign(res, { studyProgram: this._activatedRoute.snapshot.params.id, activeDepartment: data.department.id });
          });
        }
        // add user activity
        this._context.model('StudyProgramSpecialties')
            .where('studyProgram').equal(params.id)
            .and('specialty').equal(params.specialization)
            .select('id', 'specialty', 'studyProgram/id as studyProgram' , 'studyProgram/name as studyProgramName', 'name')
            .getItem().then((item) => {
              if (item) {
                this._userActivityService.setItem({
                  category: this._translateService.instant('StudyPrograms.TitlePlural'),
                  description: `${item.studyProgramName} ${item.name}`,
                  url: window.location.hash.substring(1), // get the path after the hash
                  dateCreated: new Date
                });
                this.specialization = item;
              }
        });

      });

    });

  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
  }

  executeEditAction() {
    return new Observable((observer) => {
      this._context.model('SpecializationCourses').save(this.selectedItems).then(() => {
        this.courses.fetch(true);
        observer.next();
      }).catch((err) => {
        observer.error(err);
      });
    });
  }


  editMany() {
        if (this.courses.selected.length === 1) {
          // open edit modal window
          const urlTree = this._router.createUrlTree([{
            outlets: {
              modal: [ 'item', this.courses.selected[0].id, 'edit' ]
            }
          }], {
            relativeTo: this._activatedRoute
          });
          return this._router.navigateByUrl(urlTree).catch((err) => {
            this._errorService.showError(err, {
              continueLink: '.'
            });
          });
        } else {
          this.selectedItems = this.courses.selected.map((x) => {
            return {
              id: x.id,
              units: x.units,
              coefficient: x.coefficient,
              semester: x.semester,
              ects: x.ects,
              courseType: x.courseType
            };
          });
          this._modalService.openModalComponent(EditCoursesComponent, {
            class: 'modal-lg',
            keyboard: false,
            ignoreBackdropClick: true,
            initialState: {
              items: this.selectedItems,
              modalTitle: 'StudyPrograms.EditCourses',
              execute: this.executeEditAction()
            }
          });
        }
    }

  async removeMany() {
    const items = this.courses.selected.map((x) => {
      return {
        id: x.id,
        courseStructureType: x.courseStructureType,
        course: x.courseId,
        specialtyId: x.specialtyId
      };
    });
    if (items.length > 0) {
      // check if items contains complex or course parts courses
      const complexSelected = items.filter(x => {
        return x.courseStructureType === 4;
      });
      const coursePartsSelected = items.filter(x => {
        return x.courseStructureType === 8;
      });
      let itemsAdded = false;
      if (coursePartsSelected.length > 0) {
        for (let i = 0; i < coursePartsSelected.length; i++) {
          // get parent course and add it to selectedItems
          const coursePartCourse = await this._context.model('Courses')
            .where('id').equal(coursePartsSelected[i].course).getItem();
          // check if complexCourse exist at complexSelected array
          const found = complexSelected.find(x => {
            return x.course === coursePartCourse.parentCourse;
          });
          if (!found) {
            // get Specialization course for parentCourse
            const specializationCourse = await this._context.model('SpecializationCourses')
              .where('studyProgramCourse/course').equal(coursePartCourse.parentCourse)
              .and('specialization/id').equal(coursePartsSelected[i].specialtyId)
              .getItem();
            if (specializationCourse) {
              const complexCourse = {
                id: specializationCourse.id,
                courseStructureType: 4,
                course: coursePartCourse.parentCourse,
                specialtyId: specializationCourse.specialization
              };
              // add parentCourse to arrays
              items.push(complexCourse);
              complexSelected.push(complexCourse);
              itemsAdded = true;
            }
          }
        }
      }

      if (complexSelected.length > 0 ) {
        for (let i = 0; i < complexSelected.length; i++) {
          const complexCourse = await this._context.model('Courses')
            .where('id').equal(complexSelected[i].course).expand('courseParts').getItem();
          if (complexCourse && complexCourse.courseParts && complexCourse.courseParts.length > 0) {
            // check if courseParts exist at coursePartsSelected array
            for (let j = 0; j < complexCourse.courseParts.length; j++) {
              const coursePart = complexCourse.courseParts[j];
              const found = coursePartsSelected.find(x => {
                return x.course === coursePart.id;
              });
              if (!found) {
                // get Specialization course
                const specializationCourse = await this._context.model('SpecializationCourses')
                  .where('studyProgramCourse/course').equal(coursePart.id)
                  .and('specialization/id').equal(complexSelected[i].specialtyId).getItem();
               if (specializationCourse) {
                 items.push(specializationCourse);
                 itemsAdded = true;
               }
              }
            }
          }
        }
      }
      const title = this._translateService.instant('StudyPrograms.RemoveCourses');
      // tslint:disable-next-line:max-line-length
      const  message = itemsAdded ? this._translateService.instant('StudyPrograms.RemoveCoursesAdditionalMessage') : this._translateService.instant('StudyPrograms.RemoveCoursesMessage');
      this._modalService.showDialog(title, message, DIALOG_BUTTONS.YesNo).then( dialogResult => {
        if (dialogResult === 'no') {
          return;
        }
        this._loadingService.showLoading();
        this._context.model('SpecializationCourses').remove(items).then(() => {
          this.courses.fetch(true);
          this._loadingService.hideLoading();
        }).catch((err) => {
          this._loadingService.hideLoading();
          this._errorService.showError(err);
        });
      });
    }
  }

  async copy() {
    // Only one row can be copied
    if (this.courses.selected.length === 1) {

      const courseID = this.courses.selected[0].courseId;

      this._context.model('ProgramCourses')
      .where('program').equal(this.studyProgram)
      .and('course').equal(courseID)
      .getItem().then(async (programCourseObject) => { 

        if (programCourseObject){ // If programCourse ID exists then get the rules      
          this._loadingService.showLoading();    
          const courseRules = await this._context.model(`ProgramCourses/${programCourseObject.id}/RegistrationRules`).getItems();
          if (courseRules.length === 0){
            const title = this._translateService.instant('StudyPrograms.CopyRules');
            const message = this._translateService.instant('StudyPrograms.CopyRulesAdditionalMessage');
            this._modalService.showDialog(title, message, DIALOG_BUTTONS.Ok).then( dialogResult => {
              return;
            });          
          }
          else {
            this.copiedRules = courseRules;
          }
          this._loadingService.hideLoading();
        }
        else { // programCourseObject is null - show error modal
          const title = this._translateService.instant('StudyPrograms.CopyRulesError');
          const message = this._translateService.instant('StudyPrograms.CopyRulesErrorMessage');
          this._modalService.showDialog(title, message, DIALOG_BUTTONS.Ok).then( dialogResult => {
            return;
          });          
        }
      });
      this.courses.selectNone();
    }   
  }

  async paste() {
    // Show confirmation modal first
    const title = this._translateService.instant('StudyPrograms.PasteRules');
    const message = this._translateService.instant('StudyPrograms.PasteRulesAdditionalMessage');
    this._modalService.showDialog(title, message, DIALOG_BUTTONS.YesNo).then( async dialogResult => {
      if (dialogResult === 'no') {
        this.courses.selectNone();
        return;
      }
      this._loadingService.showLoading();

      await Promise.all (this.courses.selected.map(async course => {
        const courseID = course.courseId;
  
        // Get programCourse ID for each of the selected courses
        const programCourseObject = await this._context.model('ProgramCourses')
        .where('program').equal(this.studyProgram)
        .and('course').equal(courseID)
        .getItem().then(async (programCourseObject) => { 

          if (programCourseObject){            
            // Get target rules and set state for delete
            const targetRules = await this._context.model(`ProgramCourses/${programCourseObject.id}/RegistrationRules`).getItems();
            targetRules.forEach(rule => {
              rule.$state = 4;
              return rule;
            });
            await this._context.model(`ProgramCourses/${programCourseObject.id}/RegistrationRules`).save(targetRules);
           
            // Get copied rules and set the new target
            this.copiedRules.forEach(rule => {
              delete rule.$state;
              delete rule.id;
              rule.target = programCourseObject.id;
              return rule;
            });
            await this._context.model(`ProgramCourses/${programCourseObject.id}/RegistrationRules`).save(this.copiedRules);
          }
          else { // programCourseObject is null - show error modal
            const title = this._translateService.instant('StudyPrograms.PasteRulesError');
            const message = this._translateService.instant('StudyPrograms.PasteRulesErrorMessage');
            this._modalService.showDialog(title, message, DIALOG_BUTTONS.Ok).then( dialogResult => {
              return;
            });          
          }

        });            
      }));

      this.courses.selectNone();
      this._loadingService.hideLoading();  
    });   
  }

  add() {
    try {
      this._modalService.openModalComponent(AddCoursesComponent, {
        class: 'modal-xl modal-table',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          target: this.specialization,
          modalTitle: 'StudyPrograms.AddCourses',
          execute: (() => {
            return new Observable((observer) => {
              this._loadingService.showLoading();
              // get add courses component
              const component = <AddCoursesComponent>this._modalService.modalRef.content;
              // and submit
              this._context.model('SpecializationCourses').save(component.items).then(() => {
                this.courses.fetch(true);
                this._loadingService.hideLoading();
                observer.next();
              }).catch((err) => {
                this._loadingService.hideLoading();
                observer.error(err);
              });
            });
          })()
        }
      });
    } catch (err) {
      if (this._modalService.modalRef) {
        this._modalService.modalRef.hide();
      }
          this._errorService.showError(err, {
            continueLink: '.'
          });
     }
  }

}
