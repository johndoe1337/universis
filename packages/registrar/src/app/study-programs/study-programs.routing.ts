import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {StudyProgramsHomeComponent} from './components/study-programs-home/study-programs-home.component';
import {StudyProgramsTableComponent} from './components/study-programs-table/study-programs-table.component';
import {StudyProgramsRootComponent} from './components/study-programs-root/study-programs-root.component';
import {StudyProgramsPreviewComponent} from './components/preview/study-programs-preview.component';
import {StudyProgramsPreviewGeneralComponent} from './components/preview/preview-general/study-programs-preview-general.component';
import {StudyProgramsPreviewCoursesComponent} from './components/preview/preview-courses/study-programs-preview-courses.component';
import { ProgramCoursePreviewComponent } from './components/program-course-preview/program-course-preview.component';
// tslint:disable-next-line:max-line-length
import { ProgramCoursePreviewGeneralComponent } from './components/program-course-preview/program-course-preview-general/program-course-preview-general.component';
import {
    AdvancedFormItemResolver,
    AdvancedFormModalComponent,
    AdvancedFormModalData,
    AdvancedFormParentItemResolver,
    AdvancedFormRouteItemResolver
} from '@universis/forms';
import {AdvancedFormStudyProgramWithLocalesResolver} from '../registrar-shared/advanced-form-router/advanced-form-router.component';
// tslint:disable-next-line:max-line-length
import {AdvancedFormItemWithLocalesResolver, AdvancedFormRouterComponent} from '../registrar-shared/advanced-form-router/advanced-form-router.component';
// tslint:disable-next-line:max-line-length
import { StudyProgramsTableConfigurationResolver, StudyProgramsTableSearchResolver } from './components/study-programs-table/study-programs-table-config.resolver';
// tslint:disable-next-line:max-line-length
import { StudyProgramsPreviewCoursesConfigurationResolver, StudyProgramsPreviewCoursesSearchResolver } from './components/preview/preview-courses/study-programs-preview-courses-config.resolver';
import {
  ActiveDepartmentIDResolver,
  ActiveDepartmentResolver
} from '../registrar-shared/services/activeDepartmentService.service';
import {ItemRulesModalComponent, RuleFormModalData} from '../rules';
import {SpecializationCourseProgramCourseResolver} from './services/program-course.resolver';
import { SpecialtiesComponent } from './components/preview/specialties/specialties.component';
import { DataServiceQueryParams } from '@themost/client';
import { CalculationRulesModalComponent} from '../rules/components/calculation-rules/calculation-rules.component';
// tslint:disable-next-line:import-spacing
import  {GroupsComponent} from './components/preview/groups/groups.component';
import {GroupCoursesComponent} from './components/preview/group-courses/group-courses.component';
import {CoursesSharedModule} from '../courses/courses.shared';
import {GroupAddCourseComponent} from './components/preview/group-courses/group-courses-details/group-add-course.component';
// tslint:disable-next-line:max-line-length
import {GroupCoursesDetailsPercentComponent} from './components/preview/group-courses/group-courses-details/group-courses-details-percent/group-courses-details-percent.component';
import * as PROGRAM_COURSES_LIST from './components/preview/group-courses/group-courses-details/group-courses-details.config.list.json';
import {CopyProgramComponent} from './components/preview/copy-program/copy-program.component';
import {PreviewSemesterRulesComponent} from './components/preview/preview-semester-rules/preview-semester-rules.component';
import {
  SemesterRulesConfigurationResolver,
  SemesterRulesSearchResolver
} from './components/preview/preview-semester-rules/semester-rules-config.resolver';

const routes: Routes = [
    {
        path: '',
        component: StudyProgramsHomeComponent,
        data: {
            title: 'Study Programs'
        },
        children: [
        {
          path: '',
          pathMatch: 'full',
          redirectTo: 'list/active'
        },
        {
          path: 'list',
          pathMatch: 'full',
          redirectTo: 'list/active'
        },
        {
          path: 'list/:list',
          component: StudyProgramsTableComponent,
          data: {
            title: 'Study Programs List'
          },
          resolve: {
            tableConfiguration: StudyProgramsTableConfigurationResolver,
            searchConfiguration: StudyProgramsTableSearchResolver
          }
        }
      ]
    },
  {
    path: 'create',
    component: StudyProgramsRootComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'new'
      },
      {
        path: 'new',
        component: AdvancedFormRouterComponent,
        data: {
          '$state': 1,
          'action': 'new',
          continue:
            ['/study-programs/${id}']
        },
      }
    ],
    resolve: {
      department: ActiveDepartmentIDResolver
    },
  },
    {
     path: ':id',
        component: StudyProgramsRootComponent,
        data: {
            title: 'Study Programs Home'
        },
        children: [
        {
          path: '',
          pathMatch: 'full',
          redirectTo: 'preview'
        },
        {
          path: 'preview',
          component: StudyProgramsPreviewComponent,
          data: {
            title: 'Study Programs Preview'
          },
          children: [
            {
                path: '',
                redirectTo: 'general'
            },
            {
                path: 'general',
                component: StudyProgramsPreviewGeneralComponent,
                data: {
                    title: 'Study Programs Preview General'
                }
            },
            {
              path: 'internshipRules',
              pathMatch: 'full',
              component: ItemRulesModalComponent,
              outlet: 'modal',
              data: <RuleFormModalData> {
                model: 'StudyPrograms',
                closeOnSubmit: true,
                navigationProperty: 'InternshipRules',
              },
              resolve: {
                data: AdvancedFormItemResolver,
                department: ActiveDepartmentIDResolver,
                studyProgram: AdvancedFormItemResolver
              }
            },
            {
              path: 'thesisRules',
              pathMatch: 'full',
              component: ItemRulesModalComponent,
              outlet: 'modal',
              data: <RuleFormModalData> {
                model: 'StudyPrograms',
                closeOnSubmit: true,
                navigationProperty: 'ThesisRules'
              },
              resolve: {
                data: AdvancedFormItemResolver,
                department: ActiveDepartmentIDResolver,
                studyProgram: AdvancedFormItemResolver
              }
            },
            {
              path: 'graduateCalculationMode',
              pathMatch: 'full',
              component: CalculationRulesModalComponent,
              outlet: 'modal',
              data: <RuleFormModalData> {
                model: 'StudyPrograms',
                closeOnSubmit: true,
                navigationProperty: 'graduateCalculationMode',
              },
              resolve: {
                data: AdvancedFormItemResolver,
                department: ActiveDepartmentIDResolver
              }
            },
            {
              path: 'copyProgram',
              pathMatch: 'full',
              component: CopyProgramComponent,
              outlet: 'modal',
              data: <AdvancedFormModalData> {
                model: 'StudyPrograms',
                closeOnSubmit: true,
                serviceQueryParams: {
                }
              },
              resolve: {
                data: AdvancedFormItemResolver
              }
            },
            {
              path: 'specialties',
              component: SpecialtiesComponent,
              data: {
                  title: 'Study Programs Specialties'
              },
              children: [
                {
                  path: 'new',
                  pathMatch: 'full',
                  component: AdvancedFormModalComponent,
                  outlet: 'modal',
                  data: <AdvancedFormModalData>{
                    action: 'new',
                    model: 'StudyProgramSpecialties',
                    closeOnSubmit: true
                  },
                  resolve: {
                    studyProgram: AdvancedFormParentItemResolver
                  }
                },
                {
                  path: ':id/edit',
                  pathMatch: 'full',
                  component: AdvancedFormModalComponent,
                  outlet: 'modal',
                  data: <AdvancedFormModalData>{
                    action: 'edit',
                    model: 'StudyProgramSpecialties',
                    serviceQueryParams: {
                      $expand: 'locales'
                    },
                    closeOnSubmit: true
                  },
                  resolve: {
                    data: AdvancedFormItemWithLocalesResolver
                  }
                },
                {
                  path: ':id/rules',
                  pathMatch: 'full',
                  component: ItemRulesModalComponent,
                  outlet: 'modal',
                  data: <RuleFormModalData> {
                    model: 'StudyProgramSpecialties',
                    closeOnSubmit: true,
                    navigationProperty: 'GraduationRules'
                  },
                  resolve: {
                    data: AdvancedFormItemResolver,
                    department: ActiveDepartmentIDResolver,
                    studyProgram: AdvancedFormRouteItemResolver
                  }
                },
                {
                  path: ':id/specialtyRules',
                  pathMatch: 'full',
                  component: ItemRulesModalComponent,
                  outlet: 'modal',
                  data: <RuleFormModalData> {
                    model: 'StudyProgramSpecialties',
                    closeOnSubmit: true,
                    navigationProperty: 'SpecialtyRules'
                  },
                  resolve: {
                    data: AdvancedFormItemResolver,
                    department: ActiveDepartmentIDResolver,
                    studyProgram: AdvancedFormRouteItemResolver
                  }
                }
              ]
            },
            {
              path: 'groups',
              component: GroupsComponent,
              data: {
                title: 'Study Programs Groups',
              },
              children: [
                {
                  path: 'new',
                  pathMatch: 'full',
                  component: AdvancedFormModalComponent,
                  outlet: 'modal',
                  data: <AdvancedFormModalData>{
                    action: 'new',
                    model: 'ProgramGroups',
                    closeOnSubmit: true,
                    groupType: {alternateName: 'simple'}
                  },
                  resolve: {
                    program: AdvancedFormParentItemResolver
                  }
                },
                {
                  path: ':id/edit',
                  pathMatch: 'full',
                  component: AdvancedFormModalComponent,
                  outlet: 'modal',
                  data: <AdvancedFormModalData>{
                    action: 'edit',
                    model: 'ProgramGroups',
                    serviceQueryParams: {
                        $expand: 'gradeScale, courseType, semester'
                    },
                    closeOnSubmit: true
                  },
                  resolve: {
                    data: AdvancedFormItemWithLocalesResolver
                  }
                },
                {
                  path: ':id/programGroupsRules',
                  pathMatch: 'full',
                  component: ItemRulesModalComponent,
                  outlet: 'modal',
                  data: <RuleFormModalData> {
                    model: 'ProgramGroups',
                    closeOnSubmit: true,
                    navigationProperty: 'RegistrationRules'
                  },
                  resolve: {
                    data: AdvancedFormItemResolver,
                    department: ActiveDepartmentIDResolver
                  }
                }
              ]
            },
            {
              path: 'groups/:id',
              component: GroupCoursesComponent,
              data: {
                title: 'Exams Preview Display Grades'
              },
              children: [
                {
                  path: 'add',
                  pathMatch: 'full',
                  component: GroupAddCourseComponent,
                  outlet: 'modal',
                  data: <AdvancedFormModalData> {
                    title: 'StudyPrograms.AddCourse',
                    config: PROGRAM_COURSES_LIST,
                    closeOnSubmit: true,
                  },
                resolve: {
                  group: AdvancedFormItemResolver
}
                },
                {
                  path: 'factors',
                  pathMatch: 'full',
                  component: GroupCoursesDetailsPercentComponent,
                  outlet: 'modal'
                }
              ]
            },
              {
                  path: 'specialization/:specialization/courses',
                  component: StudyProgramsPreviewCoursesComponent,
                  data: {
                      title: 'Study Programs Preview Courses'
                  },
                  resolve: {
                      tableConfiguration: StudyProgramsPreviewCoursesConfigurationResolver,
                      searchConfiguration: StudyProgramsPreviewCoursesSearchResolver,
                      department: ActiveDepartmentResolver
                  },
                  children: [
                    {
                      path: 'item/:id/rules',
                      pathMatch: 'full',
                      component: ItemRulesModalComponent,
                      outlet: 'modal',
                      data: <RuleFormModalData> {
                        model: 'ProgramCourses',
                        closeOnSubmit: true,
                        serviceQueryParams: {
                          $expand: 'course,studyProgram',
                        },
                        navigationProperty: 'RegistrationRules'
                      },
                      resolve: {
                        data: SpecializationCourseProgramCourseResolver,
                        department: ActiveDepartmentIDResolver,
                        studyProgram: AdvancedFormRouteItemResolver
                      }
                    },
                      {
                          path: 'item/:id/edit',
                          pathMatch: 'full',
                          component: AdvancedFormModalComponent,
                          outlet: 'modal',
                          data: <AdvancedFormModalData> {
                              model: 'SpecializationCourses',
                              action: 'edit',
                              closeOnSubmit: true,
                              serviceQueryParams: {
                                  $expand: 'studyProgramCourse($expand=course),specialization',
                              }
                          },
                          resolve: {
                              data: AdvancedFormItemResolver
                          }
                      }
                  ]
              },
            {
                path: 'courses',
                component: StudyProgramsPreviewCoursesComponent,
                data: {
                    title: 'Study Programs Preview Courses'
                },
                resolve: {
                  tableConfiguration: StudyProgramsPreviewCoursesConfigurationResolver,
                  searchConfiguration: StudyProgramsPreviewCoursesSearchResolver,
                  department: ActiveDepartmentResolver
                },
                children: [
                    {
                        path: 'programCourses/:id/edit',
                        pathMatch: 'full',
                        component: AdvancedFormModalComponent,
                        outlet: 'modal',
                        data: <AdvancedFormModalData> {
                            model: 'SpecializationCourses',
                            action: 'edit',
                            serviceQueryParams: {
                                $expand: 'studyProgramCourse($expand=course),specialization',
                            }
                        },
                        resolve: {
                            data: AdvancedFormItemResolver
                        }
                    }
                ]
            },
            {
              path: 'semesterRules',
              component: PreviewSemesterRulesComponent,
              resolve: {
                tableConfiguration: SemesterRulesConfigurationResolver,
                searchConfiguration: SemesterRulesSearchResolver
              },
              data: {
                title: 'StudyPrograms.SemesterRules'
              },
              children: [
                {
                  path: 'new',
                  pathMatch: 'full',
                  component: AdvancedFormModalComponent,
                  outlet: 'modal',
                  data: <AdvancedFormModalData>{
                    action: 'new',
                    model: 'StudyProgramSemesterRules',
                    closeOnSubmit: true
                  },
                  resolve: {
                    studyProgram: AdvancedFormParentItemResolver
                  }
                },
                {
                  path: ':id/edit',
                  pathMatch: 'full',
                  component: AdvancedFormModalComponent,
                  outlet: 'modal',
                  data: <AdvancedFormModalData>{
                    action: 'edit',
                    model: 'StudyProgramSemesterRules',
                    serviceQueryParams: {
                      $expand: 'courseTypes,semester'
                    },
                    closeOnSubmit: true
                  },
                  resolve: {
                    data: AdvancedFormItemResolver
                  }
                }
              ]
            },
        ]

        },
        {
            path: 'edit',
            component: AdvancedFormRouterComponent,
            data:
              {
                action: 'edit',
                serviceQueryParams: {
                  $expand: 'info($expand=studyType,studyTitleType,specializationTitleType,locales)'
                }
              },
            resolve: {
              data: AdvancedFormStudyProgramWithLocalesResolver,
              department: ActiveDepartmentIDResolver
            }
          },
          {
            path: ':action',
            component: AdvancedFormRouterComponent,
            data:
          {
            serviceParams: {
              $expand: 'info($expand=studyType,studyTitleType,specializationTitleType,locales)'
            }
          }
        }
      ]
    },
    {
      path: ':ProgramID/courses/:CourseID',
      component: ProgramCoursePreviewComponent,
      data: {
        title: 'Study Programs Preview'
      },
      children: [
        {
            path: '',
            redirectTo: 'general'
        },
        {
            path: 'general',
            component: ProgramCoursePreviewGeneralComponent,
            data: {
                title: 'Study Programs Preview General'
            }
        }
      ]
    }
];

@NgModule({
    imports: [
      RouterModule.forChild(routes)
    ],
    exports: [RouterModule],
    declarations: []
})
export class StudyProgramsRoutingModule {
}
