import {Component, OnInit, ViewChild, OnDestroy, Input, ElementRef, ViewEncapsulation} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import {Observable, Subscription} from 'rxjs';
import 'rxjs/add/observable/combineLatest';
import {ErrorService, ModalService} from '@universis/common';
import {ResponseError} from '@themost/client';
import {NgxExtendedPdfViewerComponent} from 'ngx-extended-pdf-viewer';
import {AppEventService} from '@universis/common';

declare var $: any;

@Component({
  selector: 'app-attachment-download',
  templateUrl: './attachment-download.component.html',
  styleUrls: ['./attachment-download.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AttachmentDownloadComponent implements OnInit, OnDestroy {

  @ViewChild('pdfViewer') pdfViewer: NgxExtendedPdfViewerComponent;
  @ViewChild('pdfViewerContainer') pdfViewerContainer: ElementRef<HTMLDivElement>;

  private subscription: Subscription;
  private changeSubscription: Subscription;
  private queryParamsSubscription: Subscription;
  public recordsTotal: number;
  public blob: any;
  public currentItem: any;
  @Input() showSignButton = false;
  public enableSignButton = false;

  constructor(private _activatedRoute: ActivatedRoute,
              private _translateService: TranslateService,
              private _context: AngularDataContext,
              private _errorService: ErrorService,
              private _router: Router,
              private _modalService: ModalService,
              private _appEvent: AppEventService) {
  }

  ngOnInit() {
    this.subscription = Observable.combineLatest(
      this._activatedRoute.params,
      this._activatedRoute.data
    ).subscribe((results) => {
      const params = Object.assign({}, results[0], results[1]);
      if (this.queryParamsSubscription) {
        this.queryParamsSubscription.unsubscribe();
      }
      this.queryParamsSubscription = this._activatedRoute.queryParams.subscribe((queryParams) => {
        if (queryParams.download) {
          this.download(queryParams.download).then((result) => {
            this.blob = result;
            this.showViewer();
            // hide body overflow
            $(document.body).css('overflow-y', 'hidden');
          }).catch((err) => {
            this._errorService.showError(err, {continueLink: '.'});
          });
        } else {
          this.closeViewer();
          $(document.body).css('overflow-y', 'auto');
          this.blob = null;
        }
      });
    }, (err) => {
      this._errorService.showError(err, {continueLink: '.'});
    });
    this.changeSubscription = this._appEvent.change.subscribe((event) => {
      // if change event refers to documents
      if (event && event.target && event.model === 'Attachments') {
        // check target id
        if (this.currentItem && event.target.documentCode === this.currentItem.documentCode) {
          return this._router.navigate([], {
            relativeTo: this._activatedRoute,
            queryParams: {
              download : event.target.documentCode
            }
          });
        }
      }
    });
  }

  /**
   * Downloads a file by using the specified document code
   */
  async download(attachmentURL: any) {
    const headers = new Headers({
      'Accept': 'application/pdf',
      'Content-Type': 'application/json'
    });
    this.currentItem = {
      url: attachmentURL
    };
    // enable of disable sign button
    this.enableSignButton = !this.currentItem.signed;
    const url = this.currentItem.url;
    // get service headers
    const serviceHeaders = this._context.getService().getHeaders();
    // manually assign service headers
    Object.keys(serviceHeaders).forEach((key) => {
      if (Object.prototype.hasOwnProperty.call(serviceHeaders, key)) {
        headers.set(key, serviceHeaders[key]);
      }
    });
    // get print url
    const documentURL = this._context.getService().resolve(url.replace(/\\/g, '/').replace(/^\/api\//, ''));
    // get report blob
    return fetch(documentURL, {
      method: 'GET',
      headers: headers,
      credentials: 'include'
    }).then((response) => {
      if (response.ok === false) {
        throw new ResponseError(response.statusText, response.status);
      }
      return response.blob();
    });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.queryParamsSubscription) {
      this.queryParamsSubscription.unsubscribe();
    }
    if (this.changeSubscription) {
      this.changeSubscription.unsubscribe();
    }
  }

  showViewer() {

    $(this.pdfViewerContainer.nativeElement).show();
    this.pdfViewer.onResize();
    // add close button
    const toolbarViewerRight = $(this.pdfViewerContainer.nativeElement)
      .find('#toolbarViewerRight');
    if (toolbarViewerRight.find('pdf-close-tool').length === 0) {
      const closeText = this._translateService.instant('Reports.Viewer.Close');
      const closeTool = $(`
        <pdf-close-tool>
            <button style="width:auto" class="toolbarButton px-2" type="button">
                ${closeText}
            </button>
        </pdf-close-tool>
      `);
      // prepare to close viewer
      closeTool.find('button').on('click', () => {
        $(this.pdfViewerContainer.nativeElement).hide();
      });
      // insert before first which is the pdf hand tool
      closeTool.insertBefore(toolbarViewerRight.find('pdf-hand-tool'));
    }
  }

  closeViewer() {
    $(this.pdfViewerContainer.nativeElement).hide();
  }

  pdfClose() {
    this._router.navigate([], {
      relativeTo: this._activatedRoute
    });
  }

  printAction() {
    $(this.pdfViewerContainer.nativeElement).find('pdf-print>button').trigger('click');
  }

  downloadAction() {
    this.pdfViewer.filenameForDownload = this.currentItem.name;
    this.pdfViewer.ngOnChanges({
      filenameForDownload: this.currentItem.name
    });
    $(this.pdfViewerContainer.nativeElement).find('pdf-download>button').trigger('click');
  }


}
