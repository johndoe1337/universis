import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdvancedFormModalComponent, AdvancedFormModalData, AdvancedFormItemResolver } from '@universis/forms';
import { ReportTemplateListComponent } from './components/report-template-list/list.component';
import { ReportCategoryListComponent } from './components/report-category-list/list.component';

import * as ReportTemplateNewForm from './components/report-template-list/forms/new.json';
import * as ReportCategoryNewForm from './components/report-category-list/forms/new.json';
import * as ReportTemplateEditForm from './components/report-template-list/forms/edit.json';
import * as ReportCategoryEditForm from './components/report-category-list/forms/edit.json';

const routes: Routes = [
    {
        path: 'configuration/templates',
        component: ReportTemplateListComponent,
        data: {
            model: 'ReportTemplates'
        },
        children: [
            {
                path: 'add',
                pathMatch: 'full',
                component: AdvancedFormModalComponent,
                outlet: 'modal',
                data: <AdvancedFormModalData>{
                    action: 'new',
                    closeOnSubmit: true,
                    formConfig: ReportTemplateNewForm
                }
            },
            {
                path: ':id/edit',
                pathMatch: 'full',
                component: AdvancedFormModalComponent,
                outlet: 'modal',
                data: <AdvancedFormModalData>{
                    action: 'edit',
                    closeOnSubmit: true,
                    formConfig: ReportTemplateEditForm
                },
                resolve: {
                    data: AdvancedFormItemResolver
                }
            }
        ]
    },
    {
        path: 'configuration/categories',
        component: ReportCategoryListComponent,
        data: {
            model: 'ReportCategories'
        },
        children: [
            {
                path: 'add',
                pathMatch: 'full',
                component: AdvancedFormModalComponent,
                outlet: 'modal',
                data: <AdvancedFormModalData>{
                    action: 'new',
                    closeOnSubmit: true,
                    formConfig: ReportCategoryNewForm
                }
            },
            {
                path: ':id/edit',
                pathMatch: 'full',
                component: AdvancedFormModalComponent,
                outlet: 'modal',
                data: <AdvancedFormModalData>{
                    action: 'edit',
                    closeOnSubmit: true,
                    formConfig: ReportCategoryEditForm
                },
                resolve: {
                    data: AdvancedFormItemResolver
                }
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    declarations: []
})
export class ReportsRoutingModule {
}
