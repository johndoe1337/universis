import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ClassesHomeComponent} from './components/classes-home/classes-home.component';
import {ClassesTableComponent} from './components/classes-table/classes-table.component';
import {ClassesRootComponent} from './components/classes-root/classes-root.component';
import {ClassesStudentsComponent} from './components/classes-dashboard/classes-students/classes-students.component';
import {ClassesInstructorsComponent} from './components/classes-dashboard/classes-instructors/classes-instructors.component';
import { AdvancedFormRouterComponent } from '../registrar-shared/advanced-form-router/advanced-form-router.component';
import {
  AdvancedFormItemResolver,
  AdvancedFormModalComponent,
  AdvancedFormModalData,
  AdvancedFormParentItemResolver
} from '@universis/forms';
import {InstructorsSharedModule} from '../instructors/instructors.shared';
import {ClassesAddInstructorComponent} from './components/classes-dashboard/classes-instructors/classes-add-instructor.component';
import {ClassesAddStudentComponent} from './components/classes-dashboard/classes-students/classes-add-student.component';
import { ClassTableConfigurationResolver, ClassTableSearchResolver } from './components/classes-table/classes-table-config.resolver';
import { ClassesDashboardComponent } from './components/classes-dashboard/classes-dashboard.component';
import { ClassesOverviewComponent } from './components/classes-dashboard/classes-overview/classes-overview.component';
import { StudentsSharedModule } from '../students/students.shared';
import { ClassesExamsComponent } from './components/classes-dashboard/classes-exams/classes-exams.component';
import { ClassStudentsSearchResolver } from './components/classes-dashboard/classes-students/class-students-config.resolver';
import { ClassInstructorsSearchResolver } from './components/classes-dashboard/classes-instructors/class-instructor-config.resolver';
import {ClassesSectionsComponent} from './components/classes-dashboard/classes-sections/classes-sections.component';
import {
  ActiveDepartmentIDResolver,
  ActiveDepartmentResolver, CurrentAcademicPeriodResolver,
  CurrentAcademicYearResolver, LastStudyProgramResolver
} from '../registrar-shared/services/activeDepartmentService.service';
import {CourseTitleResolver} from '../courses/components/dashboard/courses-preview-classes/courses-preview-classes.component';
import {SelectReportComponent} from '../reports-shared/components/select-report/select-report.component';
import {ItemRulesModalComponent, RuleFormModalData} from '../rules';

const routes: Routes = [
    {
        path: '',
        component: ClassesHomeComponent,
        data: {
            title: 'Classes'
        },
        children: [
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'list/current'
            },
          {
            path: 'list',
            pathMatch: 'full',
            redirectTo: 'list/index'
          },
            {
                path: 'list/:list',
                component: ClassesTableComponent,
                data: {
                    title: 'Classes List'
                },
                resolve: {
                  currentYear: CurrentAcademicYearResolver,
                  currentPeriod: CurrentAcademicPeriodResolver,
                  tableConfiguration: ClassTableConfigurationResolver,
                  searchConfiguration: ClassTableSearchResolver
                },
              children: [
                {
                  path: 'item/:id/rules',
                  pathMatch: 'full',
                  component: ItemRulesModalComponent,
                  outlet: 'modal',
                  data: <RuleFormModalData> {
                    model: 'CourseClasses',
                    closeOnSubmit: true,
                    serviceQueryParams: {
                      $expand: 'course',
                    },
                    navigationProperty: 'RegistrationRules'
                  },
                  resolve: {
                    department: ActiveDepartmentIDResolver,
                    data: AdvancedFormItemResolver
                  }
                },
              ]
            }
        ]
    },
  {
    path: 'create',
    component: ClassesRootComponent,
    children: [
      {
        path: 'new',
        pathMatch: 'full',
        component: AdvancedFormRouterComponent,
        resolve: {
          department: ActiveDepartmentResolver
        }
      }
    ]
  },
    {
        path: ':id',
        component: ClassesRootComponent,
        data: {
            title: 'Classes Home'
        },
        children: [
          {
            path: '',
            pathMatch: 'full',
            redirectTo: 'dashboard'
          },
          {
            path: 'dashboard',
            component: ClassesDashboardComponent,
            data: {
              title: 'Classes Dashboard'
            },
            children: [
              {
                path: '',
                pathMatch: 'full',
                redirectTo: 'overview'
              },
              {
                path: 'overview',
                component: ClassesOverviewComponent,
                data: {
                  title: 'Classes.Overview'
                },
                children: [
                  {
                    path: 'print',
                    pathMatch: 'full',
                    component: SelectReportComponent,
                    outlet: 'modal',
                    resolve: {
                      item: AdvancedFormItemResolver
                    }
                  }
              ]
              },
              {
                path: 'students',
                component: ClassesStudentsComponent,
                data: {
                  title: 'Classes.Students'
                },
                resolve: {
                  searchConfiguration: ClassStudentsSearchResolver
                },
                children: [
                  {
                    path: 'add',
                    pathMatch: 'full',
                    component: ClassesAddStudentComponent,
                    outlet: 'modal',
                    data: <AdvancedFormModalData> {
                      title: 'Classes.AddStudent',
                      config: StudentsSharedModule.StudentsList
                    },
                    resolve: {
                      courseClass: AdvancedFormItemResolver
                    }
                  }
                ]
              },
              {
                path: 'instructors',
                component: ClassesInstructorsComponent,
                data: {
                  title: 'Classes.Instructors'
                },
                resolve: {
                  searchConfiguration: ClassInstructorsSearchResolver
                },
                children: [
                  {
                    path: 'add',
                    pathMatch: 'full',
                    component: ClassesAddInstructorComponent,
                    outlet: 'modal',
                    data: <AdvancedFormModalData> {
                      title: 'Classes.AddInstructor',
                      config: InstructorsSharedModule.InstructorsList
                    },
                    resolve: {
                      courseClass: AdvancedFormItemResolver
                    }
                  },
                  {
                    path: ':id/edit',
                    pathMatch: 'full',
                    component: AdvancedFormModalComponent,
                    outlet: 'modal',
                    data: <AdvancedFormModalData> {
                      model: 'CourseClassInstructors',
                      action: 'edit',
                      closeOnSubmit: true,
                      serviceQueryParams: {
                        $expand: 'role,instructor($expand=department)',
                      }
                    },
                    resolve: {
                      data: AdvancedFormItemResolver,
                    }
                  }
                ]
              },
              {
                path: 'exams',
                component: ClassesExamsComponent,
                data: {
                  title: 'Classes.Exams'
                },
              },
              {
                path: 'sections',
                component: ClassesSectionsComponent,
                data: {
                  title: 'Classes.Sections'
                },
                children: [
                  {
                    path: 'add',
                    pathMatch: 'full',
                    component: AdvancedFormModalComponent,
                    outlet: 'modal',
                    data: <AdvancedFormModalData> {
                      model: 'CourseClassSections',
                      closeOnSubmit: true,
                      action: 'new',
                      serviceQueryParams: {
                        $expand: 'course',
                      },
                    },
                    resolve: {
                      courseClass: AdvancedFormParentItemResolver,
                      department: ActiveDepartmentResolver,
                      year: CurrentAcademicYearResolver,
                      period: CurrentAcademicPeriodResolver
                    }
                  },
                  {
                    path: ':id/edit',
                    pathMatch: 'full',
                    component: AdvancedFormModalComponent,
                    outlet: 'modal',
                    data: <AdvancedFormModalData> {
                      model: 'CourseClassSections',
                      action: 'edit',
                      closeOnSubmit: true,
                      serviceQueryParams: {
                        $expand: 'courseClass($expand=course)',
                      }
                    },
                    resolve: {
                      data: AdvancedFormItemResolver
                    }
                  },
                  {
                    path: 'item/:id/rules',
                    pathMatch: 'full',
                    component: ItemRulesModalComponent,
                    outlet: 'modal',
                    data: <RuleFormModalData> {
                      model: 'CourseClassSections',
                      closeOnSubmit: true,
                      serviceQueryParams: {
                      },
                      navigationProperty: 'CourseClassSectionRules'
                    },
                    resolve: {
                      department: ActiveDepartmentIDResolver,
                      data: AdvancedFormItemResolver
                    }
                  },
                ]
              }
            ]
          },
          {
            path: 'shared',
            component: ClassesStudentsComponent,
            data: {
              title: 'Classes.Students'
            },
            resolve: {
              searchConfiguration: ClassStudentsSearchResolver
            },
            children: [
              {
                path: 'add',
                pathMatch: 'full',
                component: ClassesAddStudentComponent,
                outlet: 'modal',
                data: <AdvancedFormModalData> {
                  title: 'Classes.AddStudent',
                  config: StudentsSharedModule.StudentsList
                },
                resolve: {
                  courseClass: AdvancedFormItemResolver
                }
              }
            ]
          },
          {
            path: ':action',
            component: AdvancedFormRouterComponent
          }
        ]
      }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    declarations: []
})
export class ClassesRoutingModule {
}
