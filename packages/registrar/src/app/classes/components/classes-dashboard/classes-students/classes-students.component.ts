import {Component, OnInit, ViewChild, Input, EventEmitter, OnDestroy, Output} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {
  AdvancedRowActionComponent,
  AdvancedTableComponent,
  AdvancedTableConfiguration,
  AdvancedTableDataResult
} from '@universis/ngx-tables';
import {AngularDataContext} from '@themost/angular';
import * as CLASSES_STUDENTS_LIST_CONFIG from './classes-students.config.list.json';
import {Observable, Subscription} from 'rxjs';
import {TranslateService} from '@ngx-translate/core';
import {ErrorService, ModalService, ToastService, DIALOG_BUTTONS, LoadingService} from '@universis/common';
// tslint:disable-next-line:max-line-length
import {ActivatedTableService} from '@universis/ngx-tables';
import {AdvancedSearchFormComponent} from '@universis/ngx-tables';
import { ClientDataQueryable } from '@themost/client';

@Component({
  selector: 'app-classes-preview-students',
  templateUrl: './classes-students.component.html'
})
export class ClassesStudentsComponent implements OnInit, OnDestroy {

  public readonly config: AdvancedTableConfiguration = <AdvancedTableConfiguration>CLASSES_STUDENTS_LIST_CONFIG;
  public recordsTotal: any;
  private dataSubscription: Subscription;
  @ViewChild('students') students: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  courseClassID: any;
  private fragmentSubscription: Subscription;
  @Input() reload: EventEmitter<any> = new EventEmitter<any>();
  @Input() tableConfiguration: any;
  @Input() searchConfiguration: any;
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();
  private subscription: Subscription;
  private selectedItems = [];

  constructor(private _activatedRoute: ActivatedRoute,
              private _translateService: TranslateService,
              private _activatedTable: ActivatedTableService,
              private _errorService: ErrorService,
              private _modalService: ModalService,
              private _toastService: ToastService,
              private _context: AngularDataContext,
              private _loadingService: LoadingService
  ) {
  }

  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.courseClassID = params.id;
      this._activatedTable.activeTable = this.students;

      this.students.query = this._context.model('StudentCourseClasses')
        .where('courseClass').equal(this.courseClassID)
        .expand('student($expand=department,person,studyProgram,semester,studentStatus,inscriptionYear,inscriptionPeriod)')
        .prepare();

      this.students.config = AdvancedTableConfiguration.cast(CLASSES_STUDENTS_LIST_CONFIG);
      this.students.fetch();

      this.fragmentSubscription = this._activatedRoute.fragment.subscribe(fragment => {
        if (fragment && fragment === 'reload') {
          this.students.fetch(true);
        }
      });

      this.dataSubscription = this._activatedRoute.data.subscribe(data => {
        if (data.tableConfiguration) {
          this.students.config = data.tableConfiguration;
          this.students.ngOnInit();
        }
        if (data.searchConfiguration) {
          this.search.form = data.searchConfiguration;
          Object.assign(this.search.form, { courseClass: this.courseClassID });
          this.search.ngOnInit();
        }
      });
    });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }

  remove() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.courseClassID = params.id;
      if (this.students && this.students.selected && this.students.selected.length) {
        const items = this.students.selected.map(item => {
          return {
            courseClass: this.courseClassID,
            student: item.studentId
          };
        });
        return this._modalService.showWarningDialog(
          this._translateService.instant('Classes.RemoveStudentTitle'),
          this._translateService.instant('Classes.RemoveStudentMessage'),
          DIALOG_BUTTONS.OkCancel).then(result => {
          if (result === 'ok') {
            this._context.model('StudentCourseClasses').remove(items).then(() => {
              this._toastService.show(
                this._translateService.instant('Classes.RemoveStudentsMessage.title'),
                this._translateService.instant((items.length === 1 ?
                  'Classes.RemoveStudentsMessage.one' : 'Classes.RemoveStudentsMessage.many')
                  , {value: items.length})
              );
              this.students.fetch(true);
            }).catch(err => {
              this._errorService.showError(err, {
                continueLink: '.'
              });
            });
          }
        });

      }
    });
  }

  async addToSectionManually() {
    try {
      this._loadingService.showLoading();
      this.selectedItems = await this.getSelectedItems();
      // validate sections and show message
      const sections = await this._context.model('CourseClassSections')
        .asQueryable()
        .where('courseClass').equal(this.courseClassID)
        .select('count(id) as total')
        .getItem();
      if (!(sections && sections.total)) {
        this.selectedItems = [];
      }
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          data: {
            'courseClass': this.courseClassID
          },
          items: this.selectedItems,
          formTemplate: 'Students/addToSection',
          modalTitle: 'Classes.AddToSectionManually.Title',
          description: 'Classes.AddToSectionManually.Description',
          errorMessage: 'Classes.AddToSectionManually.CompletedWithErrors',
          additionalMessage: this._translateService.instant('Classes.AddToSectionManually.NoSections'),
          refresh: this.refreshAction,
          execute: this.executeAddToSectionManually()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  async addToSectionAutomated() {
    // pass
  }

  async removeFromSection() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      this.selectedItems = items.filter(item => item.section);
      this._loadingService.hideLoading();
      // open modal
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Classes.RemoveFromSection.Title',
          description: 'Classes.RemoveFromSection.Description',
          errorMessage: 'Classes.RemoveFromSection.CompletedWithErrors',
          refresh: this.refreshAction,
          execute: this.executeRemoveFromSection()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  executeRemoveFromSection() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      this.refreshAction.emit({
        progress: 1
      });
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            const removeFromSection = {
              id: item.id,
              student: item.studentId,
              courseClass: this.courseClassID,
              course: item.courseId,
              section: null,
              $state: 2
            }
            await this._context.model('StudentCourseClasses').save(removeFromSection);
            result.success += 1;
            try {
              await this.students.fetchOne({
                  id: item.id
              });
            } catch (err) {
              console.log(err);
            }
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  executeAddToSectionManually() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      // get values from modal component
      const component = this._modalService.modalRef.content as AdvancedRowActionComponent;
      const data = component.formComponent.form.formio.data;
      if (data.courseClassSection == null) {
        this.selectedItems = [];
        result.errors = result.total;
        return observer.next(result);
      }
      const section = data.courseClassSection;
      this.refreshAction.emit({
        progress: 1
      });
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            // assign data
            data.id = item.id;
            data.section = parseInt(section, 10);
            data.course = item.courseId;
            data.student = item.studentId;
            delete data.courseClassSection;
            // and save
            await this._context.model('StudentCourseClasses').save(data);
            result.success += 1;
            try {
              await this.students.fetchOne({
                  id: item.id
              });
            } catch (err) {
              console.log(err);
            }
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  async getSelectedItems() {
    let items = [];
    if (this.students && this.students.lastQuery) {
      const lastQuery: ClientDataQueryable = this.students.lastQuery;
      if (lastQuery != null) {
        if (this.students.smartSelect) {
          // get items
          const selectArguments = ['student/id as studentId', 'id as id', 'courseClass/course/id as courseId', 'section'];
          // query items
          const queryItems = await lastQuery.select.apply(lastQuery, selectArguments)
            .take(-1)
            .skip(0)
            .getItems();
          if (this.students.unselected && this.students.unselected.length) {
            // remove items that have been unselected by the user
            items = queryItems.filter( item => {
              return this.students.unselected.findIndex( (x) => {
                return x.id === item.id;
              }) < 0;
            });
          } else {
            items = queryItems;
          }
        } else {
          // get selected items only
          items = this.students.selected.map( (item) => {
            return {
              studentId: item.studentId,
              id: item.id,
              courseId: item.courseId,
              section: item.section
            };
          });
        }
      }
    }
    return items;
  }
}
