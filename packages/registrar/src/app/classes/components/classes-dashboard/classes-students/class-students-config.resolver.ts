import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {TableConfiguration} from '@universis/ngx-tables';

export class ClassStudentsConfigurationResolver implements Resolve<TableConfiguration> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
        return import(`./classes-students.config.${route.params.list}.json`)
            .catch( err => {
           return  import(`./classes-students.config.list.json`);
        });
    }
}

export class ClassStudentsSearchResolver implements Resolve<TableConfiguration> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
        return import(`./classes-students.search.${route.params.list}.json`)
            .catch( err => {
                return  import(`./classes-students.search.list.json`);
            });
    }
}

export class ClassDefaultStudentsConfigurationResolver implements Resolve<any> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return import(`./classes-students.config.list.json`);
    }
}
