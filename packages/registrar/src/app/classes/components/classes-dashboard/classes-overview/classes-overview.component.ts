import {Component, OnDestroy, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-classes-overview',
  templateUrl: './classes-overview.component.html',
  styleUrls: ['./classes-overview.component.scss']
})
export class ClassesOverviewComponent implements OnInit, OnDestroy  {

  public class;
  private subscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute, private _context: AngularDataContext) { }

  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.class = await this._context.model('CourseClasses')
        .where('id').equal(params.id)
        .expand('status,period,course($expand=courseStructureType, instructor, department, gradeScale)')
        .getItem();
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
