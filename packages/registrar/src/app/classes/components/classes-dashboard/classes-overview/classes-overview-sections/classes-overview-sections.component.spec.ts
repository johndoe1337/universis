import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassesOverviewSectionsComponent } from './classes-overview-sections.component';

describe('ClassesOverviewSectionsComponent', () => {
  let component: ClassesOverviewSectionsComponent;
  let fixture: ComponentFixture<ClassesOverviewSectionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassesOverviewSectionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassesOverviewSectionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
