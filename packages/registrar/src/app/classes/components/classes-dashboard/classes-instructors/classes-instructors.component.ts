import {Component, EventEmitter, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
// tslint:disable-next-line:max-line-length
import {AdvancedTableComponent, AdvancedTableConfiguration, AdvancedTableDataResult} from '@universis/ngx-tables';
import {AngularDataContext} from '@themost/angular';
import * as CLASSES_INSTRUCTORS_LIST_CONFIG from './classes-instructors.config.list.json';
import {DIALOG_BUTTONS, ErrorService, ModalService, ToastService} from '@universis/common';
import {TranslateService} from '@ngx-translate/core';
import {Subscription} from 'rxjs';
import {ActivatedTableService} from '@universis/ngx-tables';
import {AdvancedSearchFormComponent} from '@universis/ngx-tables';
// tslint:disable-next-line:max-line-length


@Component({
  selector: 'app-classes-preview-instructors',
  templateUrl: './classes-instructors.component.html'
})
export class ClassesInstructorsComponent implements OnInit, OnDestroy {

  public readonly config: AdvancedTableConfiguration = <AdvancedTableConfiguration>CLASSES_INSTRUCTORS_LIST_CONFIG;
  public recordsTotal: any;
  private dataSubscription: Subscription;
  @ViewChild('instructors') instructors: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  courseClassID: any;
  private fragmentSubscription: Subscription;
  @Input() reload: EventEmitter<any> = new EventEmitter<any>();
  @Input() tableConfiguration: any;
  @Input() searchConfiguration: any;
  private subscription: Subscription;


  constructor(private _activatedRoute: ActivatedRoute,
              private _translateService: TranslateService,
              private _activatedTable: ActivatedTableService,
              private _errorService: ErrorService,
              private _modalService: ModalService,
              private _toastService: ToastService,
              private _context: AngularDataContext

  ) { }

  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.courseClassID = params.id;
      this._activatedTable.activeTable = this.instructors;

      this.instructors.query = this._context.model('CourseClassInstructors')
        .where('courseClass').equal(this.courseClassID)
        .expand('instructor($expand=department)')
        .prepare();

      this.instructors.config = AdvancedTableConfiguration.cast(CLASSES_INSTRUCTORS_LIST_CONFIG);
      this.instructors.fetch();

      this.fragmentSubscription = this._activatedRoute.fragment.subscribe(fragment => {
        if (fragment && fragment === 'reload') {
          this.instructors.fetch(true);
        }
      });

      this.dataSubscription = this._activatedRoute.data.subscribe(data => {
        if (data.tableConfiguration) {
          this.instructors.config = data.tableConfiguration;
          this.instructors.ngOnInit();
        }
        /*      if (data.searchConfiguration) {
                this.search.form = data.searchConfiguration;
                this.search.ngOnInit();
              }*/
      });
    });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;

  }

  ngOnDestroy(): void {
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }

    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }
  remove() {
    if (this.instructors && this.instructors.selected && this.instructors.selected.length) {
      const items = this.instructors.selected.map( item => {
        return {
          courseClass: this.courseClassID,
          instructor: item.instructorId,
        };
      });
      return this._modalService.showWarningDialog(
        this._translateService.instant('Classes.RemoveInstructorTitle'),
        this._translateService.instant('Classes.RemoveInstructorMessage'),
        DIALOG_BUTTONS.OkCancel).then( result => {
        if (result === 'ok') {
          this._context.model('courseClassInstructors').remove(items).then( () => {
            this._toastService.show(
              this._translateService.instant('Classes.RemoveInstructorsMessage.title'),
              this._translateService.instant((items.length === 1 ?
                'Classes.RemoveInstructorsMessage.one' : 'Classes.RemoveInstructorsMessage.many')
                , { value: items.length })
            );
            this.instructors.fetch(true);
          }).catch( err => {
            this._errorService.showError(err, {
              continueLink: '.'
            });
          });
        }
      });

    }
  }


}
