import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { ResponseError } from '@themost/client';
import { AppEventService, ErrorService, LoadingService } from '@universis/common';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'app-document-download-routed',
  templateUrl: './document-download-routed.component.html',
  styleUrls: ['../select-report/select-report.component.scss']
})
export class DocumentDownloadRoutedComponent implements OnInit, OnDestroy {

  public currentItem;
  public documents: any[];
  @Input() showSignButton = true;

  private subscription: Subscription;
  private changeSubscription: Subscription;
  private queryParamsSubscription: Subscription;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _errorService: ErrorService,
    private _context: AngularDataContext,
    private _appEvent: AppEventService,
    private _router: Router,
    private _loadingService: LoadingService
  ) { }

  ngOnInit() {
    this.subscription = Observable.combineLatest(
      this._activatedRoute.params,
      this._activatedRoute.data
    ).subscribe((results) => {
      const params = Object.assign({}, results[0], results[1]);
      if (this.queryParamsSubscription) {
        this.queryParamsSubscription.unsubscribe();
      }
      this.queryParamsSubscription = this._activatedRoute.queryParams.subscribe((queryParams) => {
        if (queryParams.download) {
          this._loadingService.showLoading();
          this.download(queryParams.download).then((result) => {
            this.documents = [{
              blob: result,
              documentCode: this.currentItem.documentCode,
              signed: this.currentItem.signed
            }];
            // hide body overflow
            $(document.body).css('overflow-y', 'hidden');
            this._loadingService.hideLoading();
          }).catch((err) => {
            this._loadingService.hideLoading();
            this._errorService.showError(err, { continueLink: '.' });
          });
        } else {
          $(document.body).css('overflow-y', 'auto');
          this.documents = [];
        }
      });
    }, (err) => {
      this._errorService.showError(err, { continueLink: '.' });
    });
    this.changeSubscription = this._appEvent.change.subscribe((event) => {
      // if change event refers to documents
      if (event && event.target && event.model === 'DocumentNumberSeriesItems') {
        // check target id
        if (this.currentItem && event.target.documentCode === this.currentItem.documentCode) {
          return this._router.navigate([], {
            relativeTo: this._activatedRoute,
            queryParams: {
              download: event.target.documentCode
            }
          });
        }
      }
    });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.queryParamsSubscription) {
      this.queryParamsSubscription.unsubscribe();
    }
    if (this.changeSubscription) {
      this.changeSubscription.unsubscribe();
    }
  }

  navigateBack() {
    return this._router.navigate([], {
      relativeTo: this._activatedRoute,
      queryParams: {
        download: null
      },
      queryParamsHandling: 'merge'
    });
  }

  /**
   * Downloads a file by using the specified document code
   */
  async download(documentCode: any) {
    const headers = new Headers({
      'Accept': 'application/pdf',
      'Content-Type': 'application/json'
    });
    this.currentItem = await this._context.model('DocumentNumberSeriesItems')
      .where('documentCode').equal(documentCode)
      .getItem();
    if (this.currentItem == null) {
      throw new ResponseError('Not Found', 404);
    }
    // enable of disable sign button
    const url = this.currentItem.url;
    // get service headers
    const serviceHeaders = this._context.getService().getHeaders();
    // manually assign service headers
    Object.keys(serviceHeaders).forEach((key) => {
      if (Object.prototype.hasOwnProperty.call(serviceHeaders, key)) {
        headers.set(key, serviceHeaders[key]);
      }
    });
    // get print url
    const documentURL = this._context.getService().resolve(url.replace(/\\/g, '/').replace(/^\/api\//, ''));
    // get report blob
    return fetch(documentURL, {
      method: 'GET',
      headers: headers,
      credentials: 'include'
    }).then((response) => {
      if (response.ok === false) {
        throw new ResponseError(response.statusText, response.status);
      }
      return response.blob();
    });
  }
}
