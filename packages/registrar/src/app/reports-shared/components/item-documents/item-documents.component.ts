import { Component, OnInit, ViewChild, OnDestroy, Input, ElementRef, ViewEncapsulation } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import {AdvancedTableComponent,
  AdvancedTableConfiguration,
  AdvancedTableDataResult} from '@universis/ngx-tables';
import { Observable, Subscription } from 'rxjs';
import 'rxjs/add/observable/combineLatest';
import { ErrorService } from '@universis/common';
import { Args, ResponseError } from '@themost/client';
import * as DOCUMENTS_LIST_CONFIG from './item-documents.config.json';
import {AppEventService} from '@universis/common';
import pick = require('lodash/pick');
declare var $: any;

@Component({
  selector: 'app-item-documents',
  templateUrl: './item-documents.component.html',
  styleUrls: ['../select-report/select-report.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ItemDocumentsComponent implements OnInit, OnDestroy {

  @Input('item') item: any;
  @Input('model') model: any;
  @ViewChild('documents') documents: AdvancedTableComponent;

  private subscription: Subscription;
  private changeSubscription: Subscription;
  private queryParamsSubscription: Subscription;
  public recordsTotal: number;
  public blob: any;
  public currentItem: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _translateService: TranslateService,
              private _context: AngularDataContext,
              private _errorService: ErrorService,
              private _router: Router,
              private _appEvent: AppEventService) {
  }

  ngOnInit() {
    this.subscription = Observable.combineLatest(
      this._activatedRoute.params,
      this._activatedRoute.data
    ).subscribe( (results) => {
      const params = Object.assign({ }, results[0], results[1]);
      // validate parameters
      Args.notEmpty(params.model, 'Model');
      Args.notEmpty(params.id, 'ID');
      this.model = params.model;
      this.item = params.id;
      this.documents.query = this._context.model(`${this.model}/${this.item}/documents`).asQueryable().prepare();
      this.documents.config = AdvancedTableConfiguration.cast(DOCUMENTS_LIST_CONFIG);
    }, (err) => {
      this._errorService.showError(err);
    });
    this.changeSubscription = this._appEvent.change.subscribe((event) => {
      if  (this.documents == null) {
        return;
      }
      if  (this.documents.dataTable == null) {
        return;
      }
      if (event && event.target && event.model === this.documents.config.model) {
        // get table rows
        const rows = Array.from(this.documents.dataTable.rows().data());
        if (rows && rows.length) {
          // find row index by id
          const rowIndex = rows.findIndex( (row: any) => {
            return row.id === event.target.id;
          });
          // if item found
          if (rowIndex >= 0) {
            // get data
            const row = rows[rowIndex];
            // pick only existing properties
            const newData = pick(event.target, Object.keys(row));
            // set data
            this.documents.dataTable.row(rowIndex).data(newData).draw();
          }
        }
      }
    });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.queryParamsSubscription) {
      this.queryParamsSubscription.unsubscribe();
    }
    if (this.changeSubscription) {
      this.changeSubscription.unsubscribe();
    }
  }

}
