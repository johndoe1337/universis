import {TableConfiguration} from '@universis/ngx-tables';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';

export class  InstructorDashboardThesesConfigurationResolver implements Resolve<TableConfiguration> {
  resolve(route: ActivatedRouteSnapshot,
          state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
    return import(`./instructors-dashboard-theses.config.${route.params.list}.json`)
      .catch( err => {
        return  import(`./instructors-dashboard-theses.config.list.json`);
      });
  }
}


export class  InstructorDashboardThesesRequestsConfigurationResolver implements Resolve<any> {
  resolve(route: ActivatedRouteSnapshot,
          state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return import(`./instructors-dashboard-theses.config.list.json`);
  }
}
