import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {InstructorsHomeComponent} from './components/instructors-home/instructors-home.component';
import {InstructorsTableComponent} from './components/instructors-table/instructors-table.component';
import {InstructorsDashboardComponent} from './components/instructors-dashboard/instructors-dashboard.component';
import {InstructorsRootComponent} from './components/instructors-root/instructors-root.component';
// tslint:disable-next-line:max-line-length
import {InstructorsDashboardOverviewComponent} from './components/instructors-dashboard/instructors-dashboard-overview/instructors-dashboard-overview.component';
// tslint:disable-next-line:max-line-length
import {InstructorsDashboardGeneralComponent} from './components/instructors-dashboard/instructors-dashboard-general/instructors-dashboard-general.component';
// tslint:disable-next-line:max-line-length
import {InstructorsDashboardClassesComponent} from './components/instructors-dashboard/instructors-dashboard-classes/instructors-dashboard-classes.component';
// tslint:disable-next-line:max-line-length
import {InstructorsDashboardExamsComponent} from './components/instructors-dashboard/instructors-dashboard-exams/instructors-dashboard-exams.component';
// tslint:disable-next-line:max-line-length
import {InstructorsDashboardThesesComponent} from './components/instructors-dashboard/instructors-dashboard-theses/instructors-dashboard-theses.component';
import {AdvancedFormItemWithLocalesResolver, AdvancedFormRouterComponent} from '../registrar-shared/advanced-form-router/advanced-form-router.component';
import {AdvancedFormItemResolver, AdvancedFormModalComponent, AdvancedFormModalData, AdvancedFormResolver} from '@universis/forms';
import {InstructorAddClassComponent} from './components/instructors-dashboard/instructors-dashboard-classes/instructor-add-class-component';
import {ClassesSharedModule} from '../classes/classes.shared';
import {InstructorAddExamsComponent} from './components/instructors-dashboard/instructors-dashboard-exams/instructor-add-exams-component';
import {ExamsSharedModule} from '../exams/exams.shared';
import { InstructorTableConfigurationResolver, InstructorTableSearchResolver } from './components/instructors-table/instructor-table-config.resolver';
import { InstructorDashboardExamsSearchResolver
} from './components/instructors-dashboard/instructors-dashboard-exams/instructor-dashboard-exams-config.resolver';
import { InstructorDashboardClassesSearchResolver } from './components/instructors-dashboard/instructors-dashboard-classes/instructor-dashboard-classes-config.resolver';
import { ActiveDepartmentResolver } from '../registrar-shared/services/activeDepartmentService.service';
import {InstructorsDashboardStudentsComponent} from './components/instructors-dashboard-students/instructors-dashboard-students.component';
import {InstructorDashboardStudentsSearchResolver} from './components/instructors-dashboard-students/instructors-dashboard-students-config.resolver';

const routes: Routes = [
  {
    path: '',
    component: InstructorsHomeComponent,
    data: {
      title: 'Instructors'
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list/currentDepartment'
      },
      {
        path: 'list',
        pathMatch: 'full',
        redirectTo: 'list/currentDepartment'
      },
      {
        path: 'list/:list',
        component: InstructorsTableComponent,
        data: {
          title: 'Instructors List'
        },
        resolve: {
          tableConfiguration: InstructorTableConfigurationResolver,
          searchConfiguration: InstructorTableSearchResolver
        }
      }
    ]
  },
  {
    path: 'create',
    component: InstructorsRootComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'new'
      },
      {
        path: 'new',
        component: AdvancedFormRouterComponent
      }
    ],
    resolve: {
      department: ActiveDepartmentResolver
    },
    data: {
      title: ''
    }
  },
  {
    path: ':id',
    component: InstructorsRootComponent,
    data: {
      title: 'Instructor Home'
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'dashboard'
      },
      {
        path: 'dashboard',
        component: InstructorsDashboardComponent,
        data: {
          title: 'Instructor Dashboard'
        },
        children: [
          {
            path: '',
            pathMatch: 'full',
            redirectTo: 'overview'
          },
          {
            path: 'overview',
            component: InstructorsDashboardOverviewComponent,
            data: {
              title: 'Instructors.Overview'
            },
            children: [
              {
                path: 'add/:action',
                pathMatch: 'full',
                component: AdvancedFormModalComponent,
                outlet: 'modal',
                data: <AdvancedFormModalData> {
                  model: 'Instructors',
                  action: 'addDepartment',
                },
                resolve: {
                  data: AdvancedFormItemResolver
                }
              }
            ]

          },
          {
            path: 'general',
            component: InstructorsDashboardGeneralComponent,
            data: {
              title: 'Instructors.General'
            }
          },
          {
            path: 'classes',
            component: InstructorsDashboardClassesComponent,
            data: {
              title: 'Instructors.Classes'
            },
            resolve: {
              searchConfiguration: InstructorDashboardClassesSearchResolver
            },
            children: [
              {
                path: 'add',
                pathMatch: 'full',
                component: InstructorAddClassComponent,
                outlet: 'modal',
                data: <AdvancedFormModalData> {
                  title: 'Instructors.AddClass',
                  config: ClassesSharedModule.DefaultClassList
                },
                resolve: {
                  instructor: AdvancedFormItemResolver
                }
              }
            ]
          },
          {
            path: 'exams',
            component: InstructorsDashboardExamsComponent,
            data: {
              title: 'Instructors.Exams'
            },
            resolve: {
              searchConfiguration: InstructorDashboardExamsSearchResolver
            },
            children: [
              {
                path: 'add',
                pathMatch: 'full',
                component: InstructorAddExamsComponent,
                outlet: 'modal',
                data: <AdvancedFormModalData> {
                  title: 'Instructors.AddExam',
                  config: ExamsSharedModule.DefaultExamList
                },
                resolve: {
                  instructor: AdvancedFormItemResolver
                }
              }
            ]
          },
          {
            path: 'theses',
            component: InstructorsDashboardThesesComponent,
            data: {
              title: 'Sidebar.Theses'
            }
          },
          {
            path: 'students',
            component: InstructorsDashboardStudentsComponent,
            data: {
              title: 'Instructors.SupervisedStudents'
            },
            resolve: {
              searchConfiguration: InstructorDashboardStudentsSearchResolver
            },
            children: [
              {
                path: ':id/edit',
                pathMatch: 'full',
                component: AdvancedFormModalComponent,
                outlet: 'modal',
                data: <AdvancedFormModalData>{
                  model: 'StudentCounselors',
                  action: 'edit',
                  closeOnSubmit: true,
                  serviceQueryParams: {
                    $expand: 'instructor,toYear,toPeriod,student($expand=person)',
                    $levels: 3
                  }
                },
                resolve: {
                  formConfig: AdvancedFormResolver,
                  data: AdvancedFormItemResolver
                }
              },
              {
                path: ':action',
                component: AdvancedFormRouterComponent
              }
            ]
          }
        ]
      },

      {
        path: 'edit',
        component: AdvancedFormRouterComponent,
        data: {
          action: 'edit',
          serviceQueryParams: {
            $expand: 'department,status,locales,person($expand=identityCountry)',
          }
        },
        resolve: {
          data: AdvancedFormItemWithLocalesResolver
        }
      },
      {
        path: ':action',
        component: AdvancedFormRouterComponent
      }
    ]
  }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    declarations: [
    ]
})
export class InstructorsRoutingModule {
}
