import { Component, OnInit } from '@angular/core';
import * as REGISTRATIONS_LIST_CONFIG from '../registrations-table/registrations-table.config.list.json';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { AppEventService } from '@universis/common';

declare interface TablePathConfiguration {
  name: string;
  alternateName: string;
  show?: boolean;
}

@Component({
  selector: 'app-registrations-home',
  templateUrl: './registrations-home.component.html',
  styleUrls: ['./registrations-home.component.scss']
})
export class RegistrationsHomeComponent implements OnInit {

  public paths: Array<TablePathConfiguration> = [];
  public activePaths: Array<TablePathConfiguration> = [];
  public maxActiveTabs = 3;
  public tabs: Array<any> = [];
  private paramSubscription: Subscription;
  private toggleShowExportSubscription: Subscription;
  public showExport = true;

  constructor(private _activatedRoute: ActivatedRoute,
              private _appEvent: AppEventService) { }

  ngOnInit() {

    this.toggleShowExportSubscription = this._appEvent.change.subscribe(change => {
      if (change && change.model === 'ToggleShowExport') {
        this.showExport = change.target;
      }
    });

    this.paths = (<any>REGISTRATIONS_LIST_CONFIG).paths;
    this.activePaths = this.paths.filter( x => {
      return x.show === true;
    }).slice(0);
    this.paramSubscription = this._activatedRoute.firstChild.params.subscribe( params => {
      const matchPath = new RegExp('^list/' + params.list  + '$', 'ig');
      const findItem = this.paths.find( x => {
        return matchPath.test(x.alternateName);
      });
      if (findItem) {
        this.showTab(findItem);
      }
    });

  }

  showTab(item: any) {
    // find if path exists in active paths
    const findIndex = this.activePaths.findIndex( x => {
      return x.alternateName === item.alternateName;
    });
    if (findIndex < 0) {
      if (this.activePaths.length >= this.maxActiveTabs) {
        // remove item at index 0
        this.activePaths.splice(0, 1);
      }
      // add active path
      this.activePaths.push(Object.assign({}, item));
    }
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnDestroy(): void {
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
    if (this.toggleShowExportSubscription) {
      this.toggleShowExportSubscription.unsubscribe();
    }
  }

}
