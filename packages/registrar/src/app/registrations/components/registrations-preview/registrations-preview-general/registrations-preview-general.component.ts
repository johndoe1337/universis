import {Component, OnInit, Input, ViewChild, OnDestroy} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ActivatedRoute, Router} from '@angular/router';
import {
  AdvancedTableComponent,
  AdvancedTableConfiguration,
  AdvancedTableDataResult
} from '@universis/ngx-tables';
import {Observable, Subscription} from 'rxjs';
import {EditCoursesComponent} from '../../../../study-programs/components/preview/edit-courses/edit-courses.component';
import {
  AppEventService,
  DIALOG_BUTTONS,
  ErrorService,
  LoadingService,
  ModalService,
  ToastService,
  UserActivityService
} from '@universis/common';
import * as STUDENTPERIODREGISTRATIONS_LIST_CONFIG from './registrations-preview-general.config.list.json';
import {AdvancedSearchFormComponent} from '@universis/ngx-tables';
import {TranslateService} from '@ngx-translate/core';
import {AddItemsComponent} from '@universis/ngx-tables';
import {ClassesSharedModule} from '../../../../classes/classes.shared';
import {AdvancedTableEditorDirective} from '@universis/ngx-tables';
import {GroupByPipe} from 'ngx-pipes';


@Component({
  selector: 'app-registrations-preview-general',
  templateUrl: './registrations-preview-general.component.html'
})
export class RegistrationsPreviewGeneralComponent implements OnInit, OnDestroy {

  public readonly config = STUDENTPERIODREGISTRATIONS_LIST_CONFIG;
  @ViewChild('classes') classes: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  @ViewChild(AdvancedTableEditorDirective) tableEditor: AdvancedTableEditorDirective;
  private dataSubscription: Subscription;
  private paramSubscription: Subscription;
  private fragmentSubscription: Subscription;
  private reloadSubscription: Subscription;

  public isEditable = false;
  public loading = true;
  public recordsTotal: any;
  private selectedItems = [];
  public registration: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _translateService: TranslateService,
              private _userActivityService: UserActivityService,
              private _router: Router,
              private _errorService: ErrorService,
              private _modalService: ModalService,
              private _toastService: ToastService,
              private _loadingService: LoadingService,
              private _appEvent: AppEventService,
              private groupByPipe: GroupByPipe) {
  }

  async ngOnInit() {
    this.isEditable = false;
    this.loading = true;

    this.dataSubscription = this._activatedRoute.data.subscribe(data => {
      // get query params
      if (this.paramSubscription) {
        this.paramSubscription.unsubscribe();
      }
      this.fragmentSubscription = this._activatedRoute.fragment.subscribe((fragment) => {
        if (fragment === 'reload') {
          this.fetch();
        }
      });

      if (data.tableConfiguration) {
        this.classes.config = data.tableConfiguration;
      }
      if (data.searchConfiguration) {
        this.search.form = data.searchConfiguration;
        this.search.formComponent.formLoad.subscribe((res: any) => {
          Object.assign(res, {registration: this._activatedRoute.snapshot.params.id, activeDepartment: data.department.id});
        });
      }
    });

    // get registration
    this.reloadSubscription = this._appEvent.change.subscribe(async event => {
      if (event && event.target && event.model === 'RegistrationsPreviewChange') {
        // reload registration
        if (event.target.id == this._activatedRoute.snapshot.params.id) {
          this.loading = true;
          if (!this.registration) {
            await this._userActivityService.setItem({
              category: this._translateService.instant('Registrations.TitleMany'),
              description: `${event.target.student.person.familyName} ${event.target.student.person.givenName}`,
              url: window.location.hash.substring(1), // get the path after the hash
              dateCreated: new Date
            });
          }
          this.registration = event.target;
          this.fetch();
        }
      }
    });

  }

  fetch() {
    if(!this.registration) {
      return;
    }
    this.loading = true;
    this.isEditable = this.registration.status.alternateName !== 'closed' &&
      this.registration.student.studentStatus.alternateName === 'active';

    this.classes.selectable = this.isEditable;
    this.classes.ngOnInit();

    const select = this.classes.config.columns.filter( column => {
      return column.virtual !== true;

    }).map(column => {
      if (column.property) {
        return column.name + ' as ' + column.property;
      }
      return column.name;
    });
    this._context.model('StudentCourseClasses')
      .select(...select)
      .where('registration').equal(this.registration.id)
      .take(-1)
      .getItems().then((items) => {

      if( items && items.length > 0 ) {
        this.tableEditor.set(items);
      } else {
        this.classes.dataTable.context[0].oLanguage.sEmptyTable = this.classes.emptyTable.nativeElement.innerHTML;
      }
      this.tableEditor.undo();
      this.loading = false;
    });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
    if (this.reloadSubscription) {
      this.reloadSubscription.unsubscribe();
    }
  }

  executeEditAction() {
    return new Observable((observer) => {
      this._context.model('StudentCourseClasses').save(this.selectedItems).then(() => {
        this.classes.fetch(true);
        observer.next();
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  editMany() {
    if (this.classes.selected.length === 1) {
      // open edit modal window
      const urlTree = this._router.createUrlTree([{
        outlets: {
          modal: ['item', this.classes.selected[0].id, 'edit']
        }
      }], {
        relativeTo: this._activatedRoute
      });
      return this._router.navigateByUrl(urlTree).catch((err) => {
        this._errorService.showError(err, {
          continueLink: '.'
        });
      });
    } else {
      this.selectedItems = this.classes.selected.map((x) => {
        return {
          id: x.id,
          units: x.units,
          coefficient: x.coefficient,
          semester: x.semester,
          ects: x.ects,
          courseType: x.courseType
        };
      });
      this._modalService.openModalComponent(EditCoursesComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Registrations.EditCourses',
          execute: this.executeEditAction()
        }
      });
    }
  }

  removeMany() {
    this.tableEditor.remove(...this.classes.selected);
  }

  async add() {
    try {
      const tableConfiguration =  AdvancedTableConfiguration.cast(ClassesSharedModule.AvailableClassList, true);
      tableConfiguration.model = `students/${this.registration.student.id}/periodAvailableClasses?registrationYear=${this.registration.registrationYear.id}&registrationPeriod=${this.registration.registrationPeriod.id}`;

      const courseTypes = await this._context.model(tableConfiguration.model)
        .select('courseType')
        .groupBy('courseType')
        .getItems();
      tableConfiguration.searches = courseTypes.map((item) => {
        return {
          'name': item.courseType.name,
          'value': item.courseType.id,
          'filter': {
            'courseTypeId': item.courseType.id
          },
          'emit': false
        };
      });
      this._modalService.openModalComponent(AddItemsComponent, {
        class: 'modal-xl modal-table',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          tableConfiguration: tableConfiguration,
          target: this.registration,
          modalTitle: 'Registrations.EditCourses',
          execute: (() => {
            return new Observable((observer) => {
              try {
                // get add courses component
                const component = <AddItemsComponent>this._modalService.modalRef.content;
                let courseClasses = [];
                const courseClassesFromTable = this.tableEditor.rows();

                component.items.forEach((item, index) => {
                  // @ts-ignore
                  let classExist = courseClassesFromTable.find(e => e.courseClass === item.courseClass)
                  if(!classExist){
                    item.isPassed = 0;
                    item.formattedGrade = 0;
                    courseClasses.push(item)
                  } else {
                    this._toastService.show(
                      this._translateService.instant('Registrations.AddExistCourse.Title'),
                      this._translateService.instant('Registrations.AddExistCourse.Message',
                        {displayCode: item.displayCode, name: item.name})
                    );
                  }
                })

                this.tableEditor.add(...courseClasses);
                observer.next();
              } catch (e) {
                observer.error(e);
              }
            });
          })()
        }
      });
    } catch (err) {
      if (this._modalService.modalRef) {
        this._modalService.modalRef.hide();
      }
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  async commit() {
    const dialogResult = await this._modalService.showDialog(
      this._translateService.instant('Registrations.CommitModal.Title'),
      this._translateService.instant('Registrations.CommitModal.Message'),
      DIALOG_BUTTONS.YesNo
    );
    if (dialogResult === 'no') {
      return;
    }

    const courseClasses = this.tableEditor.rows();
    this._loadingService.showLoading();
    const registration = await this._context.model(`StudentPeriodRegistrations`)
      .asQueryable()
      .where('id').equal(this.registration.id)
      .expand('student,classes')
      .getItem();

    registration.classes = courseClasses;

    this._context.model(`Students/${registration.student.id}/registrations`)
      .save(registration).then((registrationResult) => {

      if (!registrationResult) {
        this._modalService.showErrorDialog( this._translateService.instant('Registrations.NoSuccessModal.Title'),
          this._translateService.instant('Registrations.NoSuccessModal.MessageRegistrationError'));

      }  else if (!registrationResult.validationResult) {
        this._modalService.showErrorDialog( this._translateService.instant('Registrations.NoSuccessModal.Title'),
          this._translateService.instant('Registrations.NoSuccessModal.MessageValidationError'));

      } else if (registrationResult.validationResult.success === false &&
        registrationResult.validationResult.code !== 'FAIL' &&
        registrationResult.validationResult.code !== 'EFAIL'
      ) {
        this._modalService.showErrorDialog( this._translateService.instant('Registrations.NoSuccessModal.Title'),
          this._translateService.instant('Registrations.NoSuccessModal.Message'));

      } else if (registrationResult.validationResult.success === false ||
        registrationResult.validationResult.code === 'PSUCC' ||
        registrationResult.validationResult.code === 'FAIL' ||
        registrationResult.validationResult.code === 'EFAIL') {
        let message = '';

        message += registrationResult.validationResult.message;
        // tslint:disable-next-line:max-line-length
        message +=  registrationResult.validationResult.innerMessage ? ' : <br> ' + registrationResult.validationResult.innerMessage : '' ;
        message += '<br><br>';

        const classes = registrationResult.classes.filter(x => {
          if (x.validationResult) {
            if (!x.validationResult.success) {
              return x;
            }
          }
        });
        classes.forEach(x => {
          if (x.validationResult.code === 'DEL') {
            message += this._translateService.instant('Registrations.PartialSuccessModal.DeleteMessage',
              {name: x.name || x.displayCode, message: x.validationResult.message}) + '<br><br>';
          } else {
            let classMessage = x.validationResult.message;

            if (x.validationResult.validationResults) {
              const types = this.groupByPipe.transform(x.validationResult.validationResults, 'type');
              if (Object.values(types).length > 1) {
                classMessage = '';
                x.validationResult.validationResults.forEach(c => {
                  classMessage += classMessage.length > 0 ? ', ' : '';
                  classMessage += c.message;
                });
              }
            }
            message += this._translateService.instant('Registrations.PartialSuccessModal.Message',
              {name: x.name || x.displayCode, message: classMessage}) + '<br><br>';
          }
        });

        this._modalService.showErrorDialog(this._translateService.instant('Registrations.PartialSuccessModal.Title'),
          message);

      }  else if (registrationResult.validationResult.success === false) {
        this._modalService.showErrorDialog( this._translateService.instant('Registrations.NoSuccessModal.Title'),
          this._translateService.instant('Registrations.NoSuccessModal.MessageValidationError'));

      } else if (registrationResult.validationResult.success) {
        this._toastService.show(
          this._translateService.instant('Registrations.SuccessToast.Title'),
          this._translateService.instant('Registrations.SuccessToast.Message')
        );

      } else {
        this._modalService.showErrorDialog( this._translateService.instant('Registrations.NoSuccessModal.Title'),
          this._translateService.instant('Registrations.NoSuccessModal.Message'));

      }

      this.fetch();
      this._loadingService.hideLoading();
    }).catch((err) => {
      this._loadingService.hideLoading();
      this._modalService.showErrorDialog('errorTitle', err.message);

    });
  }
}
