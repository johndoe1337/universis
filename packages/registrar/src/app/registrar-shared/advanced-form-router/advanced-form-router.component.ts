import {Component, Injectable, Input, OnDestroy, OnInit} from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import {combineLatest, Observable, Subscription} from 'rxjs';
import { AdvancedFormContainerComponent } from '../advanced-form-container/advanced-form-container.component';
import { AngularDataContext } from '@themost/angular';
import {ConfigurationService, ErrorService, TemplatePipe, ToastService} from '@universis/common';
import { ActiveDepartmentService } from '../services/activeDepartmentService.service';
import {EdmSchema} from '@themost/client';
import { AdvancedFormItemResolver } from '@universis/forms';
import { fadeAnimation } from '../../animations';

@Component({
  selector: 'app-advanced-form-router',
  templateUrl: '../advanced-form-container/advanced-form-container.component.html',
  styles: [],
  providers: [TemplatePipe],
  animations: [
    fadeAnimation
  ]
})
export class AdvancedFormRouterComponent extends AdvancedFormContainerComponent implements OnInit, OnDestroy {

  private subscription: Subscription;
  private schema: EdmSchema;

  @Input() continueLink: any;

  constructor(private _router: Router, private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext, private _activeDepartment: ActiveDepartmentService,
              private _errorService: ErrorService, private _toastService: ToastService,
              private _template: TemplatePipe) {
    super();
  }

  ngOnInit() {
    this.subscription = combineLatest(
        this._activatedRoute.params,
        this._activatedRoute.data,
        new Observable(subscriber => {
          this._context.getMetadata().then( schema => {
            return subscriber.next(schema);
          }).catch( err => {
            subscriber.error(err);
          });
        })
    ).subscribe((results: any) => {
      // get data
      const routeData = results[1];
      // assign continueLink if any
      if (routeData.continue) {
        this.continueLink = routeData.continue;
      }
      // get params
      const params = Object.assign({}, results[0], results[1]);
      // get schema
      this.schema = results[2];
      // if params has a model defined e.g. LocalDepartments
      // and an action e.g. edit
      if (params.model && params.action) {
        // set form src
        this.src = `${params.model}/${params.action}`;
        if (routeData.data) {
          this.data = routeData.data;
          //  Is there any continue link in query params?
          if ( this._activatedRoute && this._activatedRoute.snapshot && this._activatedRoute.snapshot.queryParams
            && this._activatedRoute.snapshot.queryParams.continue ) {
            this.continueLink = this._activatedRoute.snapshot.queryParams.continue;
          }

          return;
        }
        if (params.id) {
          // try to get model data
          this._context.model(params.model).asQueryable(params.serviceParams).prepare()
            .where('id').equal(params.id).getItem().then(item => {
              this.data = item;
            }).catch(err => {
              this._errorService.showError(err);
            });
        } else {
          // assign route data to model
          if (routeData) {
            const data = {};
            // pass only data that are properties or navigation properties of the specified model
            this._assign(params.model, data, routeData);
            // check if routeData contains $state
            if (routeData['$state']) {
              data['$state'] = routeData['$state'];
            }
            // set data
            this.data = data;
            if (this.data.id == null) {
              delete this.data.id;
            }
          }
        }

        // validate new form state by using routeConfig.path === 'new'
        // todo:: change the way that identifies whether a form refers to a new object or not
      } else if (params.model && this._activatedRoute && this._activatedRoute.snapshot
        && this._activatedRoute.snapshot.routeConfig && this._activatedRoute.snapshot.routeConfig.path
        && this._activatedRoute.snapshot.routeConfig.path === 'new') {
        // assign route data to model
        if (routeData) {
          const data = {};
          // pass only data that are properties or navigation properties of the specified model
          this._assign(params.model, data, routeData);
          // check if routeData contains $state
          if (routeData['$state']) {
            data['$state'] = routeData['$state'];
          }
          // set form src
          // todo:: change the assignment of form source with something more configurable
          this.src = `${params.model}/new`;
          // set data
          this.data = data;
        }
      }
      //  Is there any continue link in query params?
      if ( this._activatedRoute && this._activatedRoute.snapshot && this._activatedRoute.snapshot.queryParams
                                && this._activatedRoute.snapshot.queryParams.continue ) {
        this.continueLink = this._activatedRoute.snapshot.queryParams.continue;
      }
    });
  }

  _assign(entitySet, dest, source) {
    const add = { };
    // get metadata
    const schema =  this.schema;
    // find entity set based on model name
    const findEntitySet = schema.EntityContainer.EntitySet.find( x => {
      return x.Name === entitySet;
    });
    if (findEntitySet) {
      // find entity type
      const findEntityType = schema.EntityType.find( x => {
        return x.Name === findEntitySet.EntityType;
      });
      if (findEntityType) {
        const properties = findEntityType.Property.slice(0);
        const navigationProperties = findEntityType.NavigationProperty.slice(0);
        if (findEntityType.BaseType) {
          let baseType = schema.EntityType.find( x => {
            return x.Name === findEntityType.BaseType;
          });
          while (baseType) {
            properties.push.apply(properties, baseType.Property);
            navigationProperties.push.apply(navigationProperties, baseType.NavigationProperty);
            baseType = baseType.BaseType && schema.EntityType.find(x => {
              return x.Name === baseType.BaseType;
            });
          }
        }
        // enumerate params
        const  keys = Object.keys(source);
        for (let i = 0; i < keys.length; i++) {
          const key = keys[i];
          if (Object.prototype.hasOwnProperty.call(source, key)) {
            const property = key.split('/');
            if (property.length === 1) {
              const testProperty = new RegExp(`^${property[0]}$`);
              // check if query param is a property of the specified model
              const findProperty = properties.find( x => {
                return testProperty.test(x.Name);
              });
              // and assign value
              if (findProperty) {
                Object.defineProperty(add, findProperty.Name, {
                  enumerable: true,
                  configurable: true,
                  writable: true,
                  value: source[key]
                });
              } else {
                // search navigation property
                const findNavigationProperty = navigationProperties.find( x => {
                  return testProperty.test(x.Name);
                });
                // if query param is a navigation property
                if (findNavigationProperty) {
                  // and set property
                  Object.defineProperty(add, findNavigationProperty.Name, {
                    enumerable: true,
                    configurable: true,
                    writable: true,
                    value: source[key]
                  });
                }
              }
            }
          }
        }
      }
      // finally assign values
      Object.assign(dest, add);
      return dest;
    }
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  async onCompletedSubmission($event: any) {
    let continueLink;
    if (this.continueLink && Array.isArray(this.continueLink)) {
      continueLink = this.continueLink.map(x => {
        if (typeof x === 'string') {
          return this._template.transform(x, this.form.data)
        }
        return x;
      });
    } else if (typeof this.continueLink === 'string' && this.continueLink.trim().length > 0) {
      continueLink = [this._template.transform(this.continueLink, this.form.data)]
    }
    if (continueLink) {
      await this._router.navigate(continueLink);
      this._toastService.show( $event.toastMessage.title, $event.toastMessage.body );
    } else {
      await this._router.navigate(['../'], {relativeTo: this._activatedRoute});
      this._toastService.show( $event.toastMessage.title, $event.toastMessage.body );
    }
  }
}

@Injectable({ providedIn: 'root' })
export class AdvancedFormItemWithLocalesResolver implements Resolve<any> {
  constructor(private _context: AngularDataContext,
              private _itemResolver: AdvancedFormItemResolver,
              private _configurationService: ConfigurationService) { }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<any> | any {
    return this._itemResolver.resolve(route, state).then((item) => {
      if (item.locales) {
        // get index of object with inLanguage: defaultLocale
        const removeIndex = item.locales.findIndex(x => {
          return x.inLanguage === this._configurationService.settings.i18n.defaultLocale;
        });
        // remove object
        if (removeIndex >= 0) {
          item.locales.splice(removeIndex, 1);
        }

        // map application locales
        for (const locale of this._configurationService.settings.i18n.locales) {
          if(locale !== this._configurationService.settings.i18n.defaultLocale) {
            // try to find  if localization data exist
            const findLocale = item.locales.find((itemLocale) => {
              return itemLocale.inLanguage === locale;
            });
            if (findLocale == null) {
              // if item does not exist add it
              item.locales.push({
                inLanguage: locale
              });
            }
          }
        }
      }
      return Promise.resolve(item);
    });
  }
}

@Injectable({ providedIn: 'root' })
export class AdvancedFormStudentWithLocalesResolver implements Resolve<any> {
  constructor(private _context: AngularDataContext,
              private _itemResolver: AdvancedFormItemResolver,
              private _configurationService: ConfigurationService) { }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<any> | any {
    return this._itemResolver.resolve(route, state).then((item) => {
      if (item.person.locales) {
        // get index of object with inLanguage: defaultLocale
        const removeIndex = item.person.locales.findIndex(x => {
          return x.inLanguage === this._configurationService.settings.i18n.defaultLocale;
        });
        // remove object
        if (removeIndex >= 0) {
          item.person.locales.splice(removeIndex, 1);
        }

        // map application locales
        for (const locale of this._configurationService.settings.i18n.locales) {
          if (locale !== this._configurationService.settings.i18n.defaultLocale) {
            // try to find  if localization data exist
            const findLocale = item.person.locales.find((itemLocale) => {
              return itemLocale.inLanguage === locale;
            });
            if (findLocale == null) {
              // if item does not exist add it
              item.person.locales.push({
                inLanguage: locale
              });
            }
          }
        }
      }
      return Promise.resolve(item);
    });
  }
}

@Injectable({ providedIn: 'root' })
export class AdvancedFormStudyProgramWithLocalesResolver implements Resolve<any> {
  constructor(private _context: AngularDataContext,
              private _itemResolver: AdvancedFormItemResolver,
              private _configurationService: ConfigurationService) { }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<any> | any {
    return this._itemResolver.resolve(route, state).then((item) => {
      if (item.info.locales) {
        // get index of object with inLanguage: defaultLocale
        const removeIndex = item.info.locales.findIndex(x => {
          return x.inLanguage === this._configurationService.settings.i18n.defaultLocale;
        });
        // remove object
        if (removeIndex >= 0) {
          item.info.locales.splice(removeIndex, 1);
        }

        // map application locales
        for (const locale of this._configurationService.settings.i18n.locales) {
          if (locale !== this._configurationService.settings.i18n.defaultLocale) {
            // try to find  if localization data exist
            const findLocale = item.info.locales.find((itemLocale) => {
              return itemLocale.inLanguage === locale;
            });
            if (findLocale == null) {
              // if item does not exist add it
              item.info.locales.push({
                inLanguage: locale
              });
            }
          }
        }
      }
      return Promise.resolve(item);
    });
  }
}