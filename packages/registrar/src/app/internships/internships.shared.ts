import { NgModule, OnInit, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { environment } from '../../environments/environment';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { InternshipsPreviewGeneralComponent } from './components/internships-preview-general/internships-preview-general.component';
import { SharedModule } from '@universis/common';
import { FormsModule } from '@angular/forms';
import { InternshipsPreviewFormComponent } from './components/internships-preview-general/internships-preview-form.component';
import {
  InternshipsDefaultTableConfigurationResolver,
  InternshipsTableConfigurationResolver,
  InternshipsTableSearchResolver
} from './components/internships-table/internships-table-config.resolver';
import { AdvancedFormsModule } from '@universis/forms';
import { TablesModule } from '@universis/ngx-tables';
import { MostModule } from '@themost/angular';
import { SettingsSharedModule } from '../settings-shared/settings-shared.module';
import { SettingsService } from '../settings-shared/services/settings.service';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    SharedModule,
    FormsModule,
    MostModule,
    AdvancedFormsModule,
    SettingsSharedModule,
    TablesModule
  ],
  declarations: [
    InternshipsPreviewFormComponent
  ],
  providers: [
    InternshipsDefaultTableConfigurationResolver,
    InternshipsTableConfigurationResolver,
    InternshipsTableSearchResolver
  ],
  exports: [
    InternshipsPreviewFormComponent
  ]
})
export class InternshipsSharedModule implements OnInit {

  constructor(private _translateService: TranslateService,
    private _settings: SettingsService) {
    this.ngOnInit().catch(err => {
      console.error('An error occurred while loading internships shared module');
      console.error(err);
    });
  }

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: InternshipsSharedModule,
      providers: [
      ],
    };
  }

  async ngOnInit() {
    const sources = environment.languages.map((language) => {
      return import(`./i18n/internships.${language}.json`)
        .then((translations) => {
          this._translateService.setTranslation(language, translations, true);
        })
        .catch((err) => {
          console.log(err);
        });
    });
    // await for translations
    await Promise.all(sources);
    // wait for language change
    this._translateService.onLangChange.subscribe(() => {
      const Settings = this._translateService.instant('Settings');
      this._settings.addSection({
        name: 'Company',
        description: Settings.Lists.Company.Description,
        longDescription: Settings.Lists.Company.LongDescription,
        category: 'Companies',
        url: '/internships/configuration/companies',
      });
    });
  }

}
