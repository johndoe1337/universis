import { Component, EventEmitter, Input, OnDestroy, OnInit, ViewChild, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
// tslint:disable-next-line:max-line-length
import { AdvancedTableComponent, AdvancedTableConfiguration, AdvancedTableDataResult } from '@universis/ngx-tables';
import { AngularDataContext } from '@themost/angular';
import * as TABLE_CONFIG from './InternshipRegisterActionTable.json';
import * as SEARCH_CONFIG from './InternshipRegisterActionSearch.json';
import { DIALOG_BUTTONS, ErrorService, ModalService, ToastService, UserActivityService, AppEventService } from '@universis/common';
import { TranslateService } from '@ngx-translate/core';
import { Subscription, Observable } from 'rxjs';
import { LoadingService } from '@universis/common';
import { ClientDataQueryable } from '@themost/client';
import { RequestActionComponent } from 'packages/registrar/src/app/requests/components/request-action/request-action.component';
import { AdvancedSearchFormComponent } from '@universis/ngx-tables';
import { ActivatedTableService } from '@universis/ngx-tables';
// tslint:disable-next-line:max-line-length

@Component({
  selector: 'app-internship-candidates',
  templateUrl: './internship-candidates.component.html'
})
export class InternshipCandidatesComponent implements OnInit, OnDestroy {

  public readonly config: AdvancedTableConfiguration = <AdvancedTableConfiguration>TABLE_CONFIG;
  public recordsTotal: any;
  private dataSubscription: Subscription;
  @ViewChild('table') table: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  internshipID: any;
  private fragmentSubscription: Subscription;
  @Input() reload: EventEmitter<any> = new EventEmitter<any>();
  @Input() tableConfiguration: any;
  @Input() searchConfiguration: any;
  private subscription: Subscription;
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();
  public selectedItems: any[];
  public title: string;
  public hasEntityType: any;
  private appEventSubscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
    private _translateService: TranslateService,
    private _activatedTable: ActivatedTableService,
    private _errorService: ErrorService,
    private _modalService: ModalService,
    private _toastService: ToastService,
    private _context: AngularDataContext,
    private _loadingService: LoadingService,
    private _userActivityService: UserActivityService,
    private _appEvent: AppEventService
  ) { }


  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      
      this._context.getMetadata().then((schema) => {
        this.hasEntityType = schema.EntityType.find((item) => {
          return item.Name === 'InternshipRegisterActions'
        }) != null;
        if (this.hasEntityType === false) {
          return;
        }

        this.internshipID = params.id;
      this._activatedTable.activeTable = this.table;

      this.table.query = this._context.model('InternshipRegisterActions')
        .where('internship').equal(this.internshipID).prepare();

      this.table.config = AdvancedTableConfiguration.cast(TABLE_CONFIG);
      this.table.fetch();

      this.searchConfiguration = SEARCH_CONFIG;
      this.search.form = this.searchConfiguration;
      this.search.formComponent.formLoad.subscribe((res: any) => {
        Object.assign(res, { courseClass: this.internshipID });
      });
      this.search.ngOnInit();

      this.fragmentSubscription = this._activatedRoute.fragment.subscribe(fragment => {
        if (fragment && fragment === 'reload') {
          this.table.fetch(true);
        }
      });

      this.appEventSubscription = this._appEvent.change.subscribe((event: { model: string; target: any }) => {
        if (event && event.model === 'InternshipRegisterActions') {
          // find row
          if (event.target != null) {
            this.table.fetchOne({
              id: event.target.id
            });
          }
        }
      });

      this.dataSubscription = this._activatedRoute.data.subscribe(data => {
        if (data.tableConfiguration) {
          this.table.config = data.tableConfiguration;
          this.table.ngOnInit();
        }
      });

      this._context.model('Internships')
        .where('id').equal(params.id)
        .select('id', 'name', 'internshipYear/name as academicYear', 'internshipPeriod/name as academicPeriod')
        .getItem().then((item) => {
          if (item) {
            const activityItem = {
              category: this._translateService.instant('Internships.Candidates.Many'),
              description: this._translateService.instant(`${item.name} ${item.academicYear} ${item.academicPeriod}`),
              url: window.location.hash.substring(1), // get the path after the hash
              dateCreated: new Date
            };
            this._userActivityService.setItem(activityItem);
            this.title = `${activityItem.category}-${activityItem.description}`;
          }
        });
      }).catch((err) => {
        this._errorService.showError(err, {
          continueLink: '.'
        })
      });      
    });
  }
  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }
  ngOnDestroy(): void {
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.appEventSubscription) {
      this.appEventSubscription.unsubscribe();
    }
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }

  export() {
    //
  }

  async getSelectedItems() {
    let items = [];
    if (this.table && this.table.lastQuery) {
      const lastQuery: ClientDataQueryable = this.table.lastQuery;
      if (lastQuery != null) {
        if (this.table.smartSelect) {
          // get items
          const selectArguments = ['id', 'actionStatus/alternateName as actionStatus'];
          // query items
          const queryItems = await lastQuery.select.apply(lastQuery, selectArguments)
            .take(-1)
            .skip(0)
            .getItems();
          if (this.table.unselected && this.table.unselected.length) {
            // remove items that have been unselected by the user
            items = queryItems.filter(item => {
              return this.table.unselected.findIndex((x) => {
                return x.id === item.id;
              }) < 0;
            });
          } else {
            items = queryItems;
          }
        } else {
          // get selected items only
          items = this.table.selected.map((item) => {
            return {
              id: item.id,
              actionStatus: item.actionStatus
            };
          });
        }
      }
    }
    return items;
  }

  async acceptAction() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      // get only active items
      this.selectedItems = items.filter((item) => {
        return item.actionStatus === 'ActiveActionStatus';
      });
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(RequestActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Requests.Edit.AcceptAction.Title',
          description: 'Requests.Edit.AcceptAction.Description',
          refresh: this.refreshAction,
          execute: this.executeChangeActionStatus('CompletedActionStatus')
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  /**
   * Rejects the selected requests
   */
  async rejectAction() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      // get only active items
      this.selectedItems = items.filter((item) => {
        return item.actionStatus === 'ActiveActionStatus';
      });
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(RequestActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Requests.Edit.RejectAction.Title',
          description: 'Requests.Edit.RejectAction.Description',
          refresh: this.refreshAction,
          execute: this.executeChangeActionStatus('CancelledActionStatus')
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  executeChangeActionStatus(statusText: string) {
    return new Observable((observer) => {
      this.refreshAction.emit({
        progress: 1
      });
      const total = this.selectedItems.length;
      const result = {
        total: total,
        success: 0,
        errors: 0
      };
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            // set active item
            const updated = {
              id: item.id,
              actionStatus: {
                alternateName: statusText
              }
            };
            await this._context.model(this.table.config.model).save(updated);
            result.success += 1;
            // do not throw error while updating row
            // (user may refresh view)
            try {
              await this.table.fetchOne({
                id: updated.id
              });
            } catch (err) {
              //
            }

          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }

        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

}
