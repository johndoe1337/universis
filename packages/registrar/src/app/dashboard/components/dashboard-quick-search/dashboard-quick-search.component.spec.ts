import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardQuickSearchComponent } from './dashboard-quick-search.component';

describe('DashboardQuickSearchComponent', () => {
  let component: DashboardQuickSearchComponent;
  let fixture: ComponentFixture<DashboardQuickSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardQuickSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardQuickSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
