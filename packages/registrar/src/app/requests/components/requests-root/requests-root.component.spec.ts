import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestsRootComponent } from './requests-root.component';

describe('RequestsRootComponent', () => {
  let component: RequestsRootComponent;
  let fixture: ComponentFixture<RequestsRootComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestsRootComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestsRootComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
