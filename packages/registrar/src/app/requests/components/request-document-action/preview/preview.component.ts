import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {DIALOG_BUTTONS, ErrorService, LoadingService, ModalService, ToastService} from '@universis/common';
import {RequestsService} from '../../../services/requests.service';
import {TranslateService} from '@ngx-translate/core';
import {AppEventService} from '@universis/common';
import {Subscription} from 'rxjs';
import {ReportService} from '../../../../reports-shared/services/report.service';
import {ActivatedRoute} from '@angular/router';
import { SignerService } from '@universis/ngx-signer';

@Component({
  selector: 'app-request-document-action',
  templateUrl: './preview.component.html'
})
export class RequestDocumentActionPreviewComponent implements OnInit, OnDestroy {
  constructor(private _context: AngularDataContext,
              private _requestService: RequestsService,
              private _errorService: ErrorService,
              private _appEvent: AppEventService,
              private _translateService: TranslateService,
              private _modalService: ModalService,
              private _loadingService: LoadingService,
              private _reports: ReportService,
              private _signer: SignerService,
              private _toastService: ToastService) {
  }
  @Input() request: any;
  private changeSubscription: Subscription;

  async  ngOnInit() {
    this.changeSubscription = this._appEvent.change.subscribe((event) => {
      if (this.request == null) {
        // do nothing
        return;
      }
      if (event && event.model) {
        this._context.getMetadata().then( (schema) => {
          // validate this request state
          // get entity type for the given model
          const thisEntitySet = schema.EntityContainer.EntitySet.find( (item) => {
            return item.Name === event.model;
          });
          if (thisEntitySet && thisEntitySet.EntityType === this.request.additionalType) {
            if (event.target && this.request.id === event.target.id) {
              // reload request
              return this._requestService.getRequestDocumentById(event.target.id).then((item) => {
                this.request = item;
              }).catch((err) => {
                return this._errorService.showError(err, {
                  continueLink: '.'
                });
              });
            }
          }
          // validate result state
          if (this.request.result == null) {
            // do nothing
            return;
          }
          // get entity type for the given model
          const entitySet = schema.EntityContainer.EntitySet.find( (item) => {
            return item.Name === event.model;
          });
          // if current result has the same type
          if (entitySet && entitySet.EntityType === this.request.result.additionalType) {
            if (event.target && event.target.id === this.request.result.id) {
              // fetch request result
              this._context.model(event.model)
                  .where('id').equal(event.target.id)
                  .getItem().then( result => {
                 if (result != null) {
                   // set item
                   this.request.result = result;
                 }
              });
            }
          }
        });
      }
    });
  }

  ngOnDestroy(): void {
    if (this.changeSubscription) {
      this.changeSubscription.unsubscribe();
    }
  }

  async reIssueReport() {
    try {
      if (this.request.result) {
        const title = this._translateService.instant('Requests.ReIssue');
        const message = this._translateService.instant('Requests.ReIssueMessage');
        const dialogResult = await this._modalService.showDialog(title, message, DIALOG_BUTTONS.YesNo);
        if (dialogResult === 'no') {
          return;
        }
        this._loadingService.showLoading();
        // get blob
        // print report by passing report parameters
        const blob = await this._reports.printReport(this.request.object.reportTemplate.id, {
          ID: this.request.student.id,
          REPORT_USE_DOCUMENT_NUMBER: false,
          REPORT_DOCUMENT_SERIES: this.request.result.parentDocumentSeries,
          REPORT_DOCUMENT_NUMBER: this.request.result.documentNumber
        });
        // replace document
        const updateItem = {
          url: this.request.result.url,
          published: false,
          datePublished: null,
          signed: false
        };
        await this._signer.replaceDocument(updateItem, blob);
        // emit app change event
        // close
        if (this._modalService.modalRef) {
          this._modalService.modalRef.hide();
        }
        this._appEvent.change.next({
          model: 'DocumentNumberSeriesItems',
          target: Object.assign(this.request.result, updateItem)
        });
        this._loadingService.hideLoading();
        this._toastService.show(
          this._translateService.instant('Requests.ReIssue'),
          this._translateService.instant('Requests.ReIssueSuccessMessage')
        );
      }
    } catch (err) {
      this._loadingService.hideLoading();
      return this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }


  publishReport(state) {
    // publish
    if (this.request.result) {
      // if student user is null, notify and return
      if (state && this.request.student && this.request.student.user == null) {
        return this._modalService.showWarningDialog(this._translateService.instant('Requests.PublishDocument')
          , this._translateService.instant('Requests.PublishDocumentNoUser'));
      }
      const title = state ? this._translateService.instant('Requests.Publish') :
          this._translateService.instant('Requests.CancelPublish');
      const message = state ? this._translateService.instant('Requests.PublishMessage') :
          this._translateService.instant('Requests.CancelPublishMessage');
      this._modalService.showDialog(title, message, DIALOG_BUTTONS.YesNo).then( dialogResult => {
        if (dialogResult === 'no') {
          return;
        }
        this._loadingService.showLoading();
        const result = {'id': this.request.result.id, 'published': state, 'datePublished': state ? new Date() : null };
        this._requestService.changePublishedStatus(result).then( (res) => {
          this.request.result.published = res.published;
          this.request.result.datePublished = res.datePublished;
          this._loadingService.hideLoading();
        }).catch(err => {
          this._loadingService.hideLoading();
          this._errorService.showError(err, {
            continueLink: '.'
          });
        });
      });
    }
  }
}

@Component({
  selector: 'app-document-action-container',
  template: `
            <div *ngIf="request">
                <app-request-document-action [request]="request"></app-request-document-action>
            </div>
    `
})
// tslint:disable-next-line:component-class-suffix
export class DocumentActionContainer implements OnInit, OnDestroy {
    public request;
    private dataSubscription: Subscription;
    constructor(private _requestService: RequestsService,
                private _activatedRoute: ActivatedRoute,
                private _errorService: ErrorService) {
      //
    }

  ngOnDestroy(): void {
      if (this.dataSubscription) {
        this.dataSubscription.unsubscribe();
      }
  }

  ngOnInit(): void {
      this.dataSubscription = this._activatedRoute.params.subscribe((params) => {
        this._requestService.getRequestDocumentById(params.id).then((item) => {
          this.request = item;
        }).catch((err) => {
          return this._errorService.navigateToError(err);
        });
      }, (err) => {
        return this._errorService.navigateToError(err);
      });
  }

}
