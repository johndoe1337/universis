import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import {ElementsModule} from '../elements/elements.module';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { TablesModule } from '@universis/ngx-tables';
import { SharedModule } from '@universis/common';
import { DepartmentsSharedModule } from './departments.shared';
import { DepartmentsRoutingModule } from './departments.routing';
import { DepartmentsTableComponent } from './components/departments-table/departments-table.component';
import { DepartmentsHomeComponent } from './components/departments-home/departments-home.component';
import { DepartmentsPreviewComponent } from './components/departments-preview/departments-preview.component';
// tslint:disable-next-line:max-line-length
import { DepartmentsPreviewGeneralComponent } from './components/departments-preview/departments-preview-general/departments-preview-general.component';
import { DepartmentsRootComponent } from './components/departments-root/departments-root.component';
import { MostModule } from '@themost/angular';
import { DepartmentsAdvancedTableSearchComponent } from './components/departments-table/departments-advanced-table-search.component';
import { AdvancedFormsModule } from '@universis/forms';
import { RegistrarSharedModule } from '../registrar-shared/registrar-shared.module';
import { ReportsSharedModule } from '../reports-shared/reports-shared.module';
import {DepartmentsDashboardComponent} from './components/departements-dashboard/departments-dashboard.component';
// tslint:disable-next-line:import-spacing max-line-length
import  {DepartmentsDashboardOverviewComponent} from './components/departements-dashboard/departments-dashboard-overview/departments-dashboard-overview.component';
// tslint:disable-next-line:max-line-length
import {DepartmentsDashboardOverviewGeneralComponent} from './components/departements-dashboard/departments-dashboard-overview/departments-dashboard-overview-general/departments-dashboard-overview-general.component';
// tslint:disable-next-line:max-line-length
import {DepartmentsDashboardOverviewRegistrationsComponent} from './components/departements-dashboard/departments-dashboard-overview/departments-dashboard-overview-registrations/departments-dashboard-overview-registrations.component';
// tslint:disable-next-line:max-line-length
import {DepartmentsDashboardRegistrationsComponent} from './components/departements-dashboard/departments-dashboard-registrations/departments-dashboard-registrations.component';
import {StudyProgramsSharedModule} from '../study-programs/study-programs.shared';
// tslint:disable-next-line:max-line-length
import {DepartmentsDashboardOverviewStudyProgramsComponent} from './components/departements-dashboard/departments-dashboard-overview/departments-dashboard-overview-study-programs/departments-dashboard-overview-study-programms.component';
import {CoursesSharedModule} from '../courses/courses.shared';
import {DashboardSharedModule} from '../dashboard/dashboard.shared';
import { ArchivedDocumentsHomeComponent } from './components/archived-documents-home/archived-documents-home.component';
import { ArchivedDocumentsTableComponent } from './components/archived-documents-table/archived-documents-table.component';
import {DepartmentSnapshotsComponent} from './components/departments-snapshots/department-snapshots.component';
import {RouterModalModule} from '@universis/common/routing';
// tslint:disable-next-line:max-line-length
import { DepartmentsPreviewUsersComponent } from './components/departments-preview/departments-preview-users/departments-preview-users.component';
import { DepartmentsAddUserComponent } from './components/departments-preview/departments-preview-users/departments-add-user.component';
import { DepartmentSnapshotVariablesComponent } from './components/departments-snapshots/department-snapshot-variables.component';


@NgModule({
  imports: [
    ElementsModule,
    CommonModule,
    FormsModule,
    TranslateModule,
    TablesModule,
    SharedModule,
    DepartmentsSharedModule,
    DepartmentsRoutingModule,
    MostModule,
    AdvancedFormsModule,
    RegistrarSharedModule,
    ReportsSharedModule,
    CoursesSharedModule,
    StudyProgramsSharedModule,
    DashboardSharedModule,
    RouterModalModule,
  ],
  declarations: [DepartmentsTableComponent,
    DepartmentsHomeComponent,
    DepartmentsPreviewComponent,
    DepartmentsPreviewGeneralComponent,
    DepartmentsRootComponent,
    DepartmentsAdvancedTableSearchComponent,
    DepartmentsDashboardComponent,
    DepartmentsDashboardOverviewComponent,
    DepartmentsDashboardOverviewGeneralComponent,
    DepartmentsDashboardOverviewRegistrationsComponent,
    DepartmentsDashboardRegistrationsComponent,
    DepartmentsDashboardOverviewStudyProgramsComponent,
    DepartmentSnapshotsComponent,
    ArchivedDocumentsHomeComponent,
    ArchivedDocumentsTableComponent,
    DepartmentsPreviewUsersComponent,
    DepartmentsAddUserComponent,
    DepartmentSnapshotVariablesComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DepartmentsModule { }
