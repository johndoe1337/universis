import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { ErrorService, LoadingService, ModalService, DIALOG_BUTTONS, ToastService,  } from '@universis/common';
import { TranslateService } from '@ngx-translate/core';
import { MessagesService } from '../../services/messages.service';

@Component({
  selector: 'app-send-message-to-instructor',
  templateUrl: './send-message-to-instructor.component.html'
})
export class SendMessageToInstructorComponent implements OnInit {

  @Input() id: number;
  @Input() showMessageForm = false;
  public messageModel = {
    body: null,
    attachment: null,
    subject: null
  };
  @Output() succesfulSend = new EventEmitter<boolean>();
  @ViewChild('fileinput') fileinput;

  constructor(private _errorService: ErrorService,
              private _loadingService: LoadingService,
              private _messagesService: MessagesService,
              private _modalService: ModalService,
              private _toastService: ToastService,
              private _translate: TranslateService) { }

  ngOnInit() {
  }
  initData() {
    this.messageModel.attachment = null;
    this.messageModel.body = null;
    this.messageModel.subject = null;
  }
  save() {
    this._modalService.showDialog( this._translate.instant('Modals.ModalSendMessageTitle'),
      this._translate.instant('Modals.ModalSendMessageMessage'),
      DIALOG_BUTTONS.OkCancel)
      .then(res => {
        if (res === 'ok') {
          this._loadingService.showLoading();
          this._messagesService.sendMessageToInstructor(this.id, this.messageModel)
            .subscribe(() => {
              this.initData();

              this._loadingService.hideLoading();
              this._toastService.show( this._translate.instant('Modals.SuccessfulSendMessageTitle'),
                this._translate.instant('Modals.SuccessfulSendMessageMessage'));
              const container = document.body.getElementsByClassName('universis-toast-container')[0];
              if (container != null) {
                container.classList.add('toast-success');
              }
              this.messageSent(true);
            }, (err) => {
              this._loadingService.hideLoading();
              this._toastService.show( this._translate.instant('Modals.FailedSendMessageTitle'),
                this._translate.instant('Modals.FailedSendMessageMessage'));
              const container = document.body.getElementsByClassName('universis-toast-container')[0];
              if (container != null) {
                container.classList.add('toast-error');
              }
              this.messageSent(false);
            });
        }
      });
  }
  onFileChanged(event) {
    this.messageModel.attachment = this.fileinput.nativeElement.files[0];
  }
  reset() {
    this.fileinput.nativeElement.value = '';
    this.messageModel.attachment = null;
  }
  messageSent(succesful: boolean) {
    this.succesfulSend.emit(succesful);
  }
  close() {
    this.showMessageForm = !this.showMessageForm;
  }
}
