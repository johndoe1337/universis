import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';
import { DIALOG_BUTTONS, ErrorService, LoadingService, ModalService, ToastService } from '@universis/common';
import { DocumentSignActionComponent, SignerService } from '@universis/ngx-signer';
import { Observable } from 'rxjs';
import { ActiveDepartmentService } from '../../../registrar-shared/services/activeDepartmentService.service';
// tslint:disable-next-line:max-line-length
import { AdvancedTableComponent, AdvancedTableConfiguration, AdvancedTableDataResult } from '@universis/ngx-tables';
import { AdvancedTableEditorDirective } from '@universis/ngx-tables';
import * as VALIDATED_GRADES_LIST_CONFIG from './exams-new-grade-submission.config.list.json';

@Component({
  selector: 'app-exams-new-grade-submission',
  templateUrl: './exams-new-grade-submission.component.html'
})

export class ExamsNewGradeSubmissionComponent implements OnInit, OnDestroy {

  public model: any;
  public courseClasses: any;
  public courseExamId: any;
  public gradeSheets: File[] = [];
  public toBeInitiallySubmitted = false;
  public toBeFinallySubmitted = false;
  public sheetUploadResults: any;
  public useDigitalSignature: boolean;
  public lastError: any;
  public readonly config = VALIDATED_GRADES_LIST_CONFIG;
  public recordsTotal: number;
  public lastInfo: any;
  @ViewChild('grades') grades: AdvancedTableComponent;
  @ViewChild(AdvancedTableEditorDirective) tableEditor: AdvancedTableEditorDirective;
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();

  constructor(private _context: AngularDataContext,
              private _activatedRoute: ActivatedRoute,
              private _modalService: ModalService,
              private _translateService: TranslateService,
              private _http: HttpClient,
              private _loadingService: LoadingService,
              private _signer: SignerService,
              private _errorService: ErrorService,
              private _router: Router,
              private _activeDepartmentService: ActiveDepartmentService,
              private _toastService: ToastService) { }

  async ngOnInit() {
    this.courseExamId = this._activatedRoute.snapshot.params.id;

    // get courseExam.
    this.model = await this._context.model('CourseExams')
      .where('id').equal(this.courseExamId)
      .expand('course($expand=department), year, examPeriod')
      .getItem();

    // get courseClasses.
    this.courseClasses = await this._context.model('CourseExamClasses')
      .where('courseExam').equal(this.courseExamId)
      .expand('courseClass($expand=period, department, year)')
      .select('courseClass as courseClass')
      .getItems();

    // get activeDepartment.
    const activeDepartment = await this._activeDepartmentService.getActiveDepartment();
    // get instituteId.
    const instituteId = activeDepartment.organization;
    // get institute.
    const institute = await this._context.model('Institutes')
      .where('id').equal(instituteId)
      .getItem();
    // get useDigitalSignature from the institute's configuration.
    this.useDigitalSignature = institute
      && institute.instituteConfiguration
      && institute.instituteConfiguration.useDigitalSignature;
  }

  ngOnDestroy() {

  }

  onFileSelect(event) {
    // remove previously added file (there can be only one, since multiple files upload is not allowed).
    if (this.gradeSheets.length !== 0) {
      this.gradeSheets.splice(0, 1);
    }
    // get new file.
    const addedFile = event.addedFiles[0];
    // add to sheets list.
    this.gradeSheets.push(addedFile);
    // enable initial submission.
    this.toBeInitiallySubmitted = true;
  }

  onFileRemove(event) {
    // remove sheet.
    this.gradeSheets.splice(this.gradeSheets.indexOf(event), 1);
    // disable submissions.
    this.toBeInitiallySubmitted = false;
    this.toBeFinallySubmitted = false;
  }

  /**
   * Downloads the grading sheet.
   */
  doDownloadGradingSheet(): void {
    const headers = new Headers();
    // get service headers.
    const serviceHeaders = this._context.getService().getHeaders();
    Object.keys(serviceHeaders).forEach((key) => {
      if (serviceHeaders.hasOwnProperty(key)) {
        headers.set(key, serviceHeaders[key]);
      }
    });
    // set accept header.
    headers.set('Accept', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    const fileURL = this._context.getService().resolve(`/CourseExams/${this.courseExamId}/exportStudents`);
    // fetch exam grading sheet.
    fetch(fileURL, {
      headers: headers,
      credentials: 'include'
    }).then((response) => {
      return response.blob();
    }).then(blob => {
        const objectUrl = window.URL.createObjectURL(blob);
        const a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.href = objectUrl;
        const name = this.model.name.replace(/[/\\?%*:|"<>]/g, '-');
        const extension = 'xlsx';
        const downloadName = `${this.model.year.name}_${this.model.examPeriod.name}_${this.model.course.displayCode}_${name}.${extension}`;
        a.download = downloadName;
        a.click();
        window.URL.revokeObjectURL(objectUrl);
        a.remove();
      });
  }

  /**
   * Gets confirmation, via a modal dialogue, about the initial submission/grades-check procedure.
   * If digital signing is supported by the institute it starts the signing process, otherwise it starts the grades import procedure.
   */
  initialSubmissionConfirmation() {
    // if digital signature is supported by the institute, begin the signing procedure.
    if (this.useDigitalSignature) {
      this.signAction();
    } else /* digital signature not supported */ {
      // get confirmation regarding the initial submission procedure.
      this._modalService.showDialog(
        this._translateService.instant('Exams.InitialSubmission'),
        this._translateService.instant('Exams.InitialSubmissionMessage'),
        DIALOG_BUTTONS.OkCancel).then(result => {
        // if the user confirms the action, proceed.
        if (result === 'ok') {
          // get file.
          const file = this.gradeSheets[0];
          this._loadingService.showLoading();
          // submit file and get validationResults.
          this.submitDocument(file, file.name).then(validatedResult => {
            this.sheetUploadResults = validatedResult;
            this.createOfflineTable();
            this.handleSubmissionsBasedOnGradesValidation();
            this._loadingService.hideLoading();
          }).catch(err /*invalid grading sheet */ => {
            this._loadingService.hideLoading();
            if (err.error && err.error.object && err.error.object.validationResult && err.error.object.validationResult.message) {
              // assign validationResult message to error
              err.error = {...err.error, message: err.error.object.validationResult.message};
            }
            this._errorService.showError(err, {
              continueLink: '.'
            });
          });
        }
      });
    }
  }

  /**
   * Enables or disables submission buttons based on the action's validation result.
   */
  handleSubmissionsBasedOnGradesValidation(): void {
    const validGrades = this.sheetUploadResults['object'].validationResult.code === 'SUCC';
    // if all the grades are valid.
    if (validGrades) {
      // enable final submission.
      this.toBeFinallySubmitted = true;
      // disable initial submission.
      this.toBeInitiallySubmitted = false;
    } else /*PSUCC*/ {
      // inform user.
      this.lastError = this._translateService.instant('Exams.SuccessErrorMessage');
    }
  }

  /**
   * Performs the grading sheet submission ('import').
   *
   * @param file the file to be submitted.
   * @param fileName the file name (optional). If none is passed, then the default file.name is used.
   * @returns a promise carrying the import results of the grading sheet.
   */
  async submitDocument(file: File, fileName?: string) {
    // clear error.
    this.lastError = null;
    // clear info.
    this.lastInfo = null;
    // create formData to be posted.
    const formData: FormData = new FormData();
    formData.append('file', file, fileName || file.name);
    // get headers.
    const serviceHeaders = this._context.getService().getHeaders();
    // set postUrl.
    const postUrl = this._context.getService().resolve(`CourseExams/${this.courseExamId}/import`);
    // import sheet.
    return this._http.post(postUrl, formData, {
      headers: serviceHeaders
    }).toPromise();
  }

  /**
   * Opens a modal for the digital signing procedure.
   */
  signAction() {
    try {
      const items = [{}];
      const extras = {
        documentCanBePublished: false
      };
      this._modalService.openModalComponent(DocumentSignActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          item: {
          },
          items: items,
          modalTitle: 'Exams.SignActionTitle',
          description: 'Exams.SignActionDescription',
          refresh: this.refreshAction,
          execute: this.executeSignAction(),
          extras: extras
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  /**
   * Executes the digital signing action of the grading sheet.
   *
   * @returns any errors that may occur while digitally signing the sheet.
   */
  executeSignAction() {
    return new Observable((observer) => {
      this.refreshAction.emit({
        progress: 33
      });
      // get file.
      const file = this.gradeSheets[0];
      // get component.
      const component = <DocumentSignActionComponent>this._modalService.modalRef.content;
      // get data.
      const data = component.form.formio.data;
      let progressInterval;
      (async () => {
        // set extras.
        const extras = {
          fileName: 'unsigned.xlsx'
        };
        // check if signature requires verification code (e.g. Remote Signature)
        const requiresVerificationCode = await this._signer.requiresVerificationCode();
        if (requiresVerificationCode) {
          // validate verification code.
          const verificationCode = await this._signer.confirmVerificationCode();
          if (verificationCode == null) {
            return;
          }
          Object.assign(extras, {
            otp: verificationCode
          });
        }
        // handle fake progress with interval.
        let progressValue = 33;
        progressInterval = setInterval(() => {
          progressValue = progressValue + 5 < 100 ? progressValue + 5 : 5;
          this.refreshAction.emit({
            progress: progressValue
          });
        }, 1000);
        // sign sheet via signerService.
        const signedDocument = await this._signer.signDocument(file, data.signCertificate, null, null, extras);
        signedDocument.name = file.name;
        // verify signature.
        await this.verifySignature(signedDocument);
        // submit signed sheet for validation.
        this.sheetUploadResults = await this.submitDocument(signedDocument);
        this.createOfflineTable();
        this.handleSubmissionsBasedOnGradesValidation();
        if (progressInterval) {
          clearInterval(progressInterval);
        }
        this.refreshAction.emit({
          progress: 100
        });
       })().then(() => {
          setTimeout(() => {
            return observer.next();
          }, 750);
       }).catch(err => {
        if (progressInterval) {
          clearInterval(progressInterval);
        }
        if (err.error && err.error.object && err.error.object.validationResult && err.error.object.validationResult.message) {
          // return the message of the validationResult as the error
          return observer.error(Object.assign({}, {message: err.error.object.validationResult.message}));
        }
        if (err.error && err.error.message) {
          // show the error message
          return observer.error(Object.assign({}, {message: err.error.message}));
        }
        return observer.error(err);
       });
    });
  }

  /**
   * Creates an offline advanced table containing the modified grades of the grading sheet.
   */
  createOfflineTable(): void {
    // set table configuration
    this.grades.config = AdvancedTableConfiguration.cast(this.config);
    this.grades.config.model = `CourseExams/${this.courseExamId}/import`;

    // get grades.
    let items = this.sheetUploadResults['object'].grades;

    if (items.length !== 0) {
      // remove UNMOD/NOACTION status grades.
      items = items.filter(someGrade => {
        return !(someGrade.validationResult.code === 'UNMOD' || someGrade.validationResult.code === 'NOACTION');
      });
      if (items.length !== 0) {
        // sort grades by success, so that invalid grades come first at the *offline* table.
        items.sort((gradeOne, gradeNext) => (gradeOne.validationResult.success > gradeNext.validationResult.success) ? 1 : -1);
      } else {
        this.lastInfo = this._translateService.instant('Exams.NoModifiedGrades');
      }
    }

    this.grades.ngOnInit();
    // fill table via tableEditor.
    this.tableEditor.set(items);
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  /**
   * Gets confirmation, via a modal dialogue, about the final submission procedure and begins it on positive response.
   */
  finalSubmissionConfirmation() {
    // get confirmation regarding the final submission procedure.
    this._modalService.showDialog(
      this._translateService.instant('Exams.FinalSubmission'),
      this._translateService.instant('Exams.FinalSubmissionMessage'),
      DIALOG_BUTTONS.OkCancel).then(result => {
      // if the user confirms the action, proceed.
      if (result === 'ok') {
        this._loadingService.showLoading();
        this.doFinalSubmit().then(() => {
          // disable final submission.
          this.toBeFinallySubmitted = false;
          this._loadingService.hideLoading();
          // inform user.
          this._toastService.show(this._translateService.instant('Exams.SheetSubmissionFeedback.title'),
            this._translateService.instant('Exams.SheetSubmissionFeedback.message'));
          setTimeout(() => {
            // navigate to sheet's approval interface.
            this._router.navigate(['/exams', this.courseExamId, 'dashboard', 'grading', this.sheetUploadResults.id]);
          }, 750);
        }).catch(err => {
          this._loadingService.hideLoading();
          this._errorService.showError(err, {
            continueLink: '.'
          });
        });
      }
    });
  }
  /**
   * Permorms the final submission procedure (action completion).
   * @returns
   */
  async doFinalSubmit(): Promise<any> {
    // get actionId.
    const actionId = this.sheetUploadResults.id;
    // complete action.
    return this._context.model(`ExamDocumentUploadActions/${actionId}/complete`).save(null);
  }

  /**
   * Verifies all digital signatures contained in a file, via signer service.
   * @param file the file to be examined.
   * @returns an error if any signatures are invalid.
   */
  async verifySignature(file: any): Promise<any> {
    // get signature verification results.
    let signatureVerificationResults = await this._signer.verifyDocument(file);
    // check if all signatures are valid.
    signatureVerificationResults = signatureVerificationResults.filter(signature => {
      return signature.valid === false;
    });
    if (signatureVerificationResults.length === 0) {
      return Promise.resolve();
    }
    return Promise.reject(new Error(this._translateService.instant('Exams.InvalidSignature')));
  }
}
