import {Component, OnDestroy, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import {cloneDeep} from 'lodash';
import * as EXAMS_LIST_CONFIG from '../exams-table/exams-table.config.list.json';
import {AppEventService, DIALOG_BUTTONS, TemplatePipe, ModalService, ErrorService} from '@universis/common';
import {combineLatest, Subscription} from 'rxjs';
import {ActionButtonItem} from '../../../registrar-shared/actions-button/actions-button.component';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-exams-root',
  templateUrl: './exams-root.component.html'
})
export class ExamsRootComponent implements OnInit, OnDestroy  {

  public model: any;
  public actions: any[];
  public config: any;
  public allowedActions: any[];
  public edit: any;
  private gradeSubmissions: Array<any>;
  private subscription: Subscription;


  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _template: TemplatePipe,
              private _translateService: TranslateService,
              private _modalService: ModalService,
              private _errorService: ErrorService,
              private _appEventService: AppEventService) { }

  async ngOnInit() {
    this.subscription =  combineLatest([this._activatedRoute.params, this._appEventService.change]).subscribe(async (params) => {
      const routeParams = params[0];
      const eventChanges = params[1];
      this.model = eventChanges && eventChanges.model === 'CourseExams' ? eventChanges.target : await this._context.model('CourseExams')
        .where('id').equal(routeParams.id)
        .expand('course,examPeriod,status,completedByUser,year')
        .getItem();

      // @ts-ignore
      this.config = cloneDeep(EXAMS_LIST_CONFIG as TableConfiguration);

      if (this.config.columns && this.model) {
        // get actions from config file
        this.actions = this.config.columns.filter(x => {
          return x.actions;
        })
          // map actions
          .map(x => x.actions)
          // get list items
          .reduce((a, b) => b, 0);

        // filter actions with student permissions
        this.allowedActions = this.actions.filter(x => {
          if (x.role) {
            if (x.role === 'action') {
              return x;
            }
          }
        });

        this.edit = this.actions.find(x => {
          if (x.role === 'edit') {
            x.href = this._template.transform(x.href, this.model);
            return x;
          }
        });

        this.actions = this.allowedActions;
        this.actions.forEach(action => {
          action.href = this._template.transform(action.href, this.model);
        });
      }
      await this.extendActions();
    });
  }

  async extendActions() {
    const hasActiveSubmissions: boolean = await this.validateExam();
    const extendedActions = <Array<ActionButtonItem>> [
      {
        title: 'Exams.OpenExam',
        click: () => {
          return this.changeStatus('open');
        },
        disabled: hasActiveSubmissions || this.model.status.alternateName !== 'closed'
      },
      {
        title: 'Exams.CloseExam',
        click: () => {
          return this.changeStatus('closed');
        },
        disabled: hasActiveSubmissions || !['open', 'completed'].includes(this.model.status.alternateName)
      }
    ];
    this.actions = [...this.actions, ...extendedActions];
  }

  async changeStatus(status: string) {
    const updatedModel = {
      id: this.model.id,
      status: {
        alternateName: status
      }
    }
    return this._modalService.showWarningDialog(
      this._translateService.instant(status === 'open' ? 'Exams.OpenAction.Title' : 'Exams.CloseAction.Title'),
      this._translateService.instant(status === 'open' ? 'Exams.OpenAction.DescriptionOne' : 'Exams.CloseAction.DescriptionOne'),
      DIALOG_BUTTONS.OkCancel).then( async(result) => {
      if (result === 'ok') {
        try {
          const exam = await this._context.model('CourseExams').save(updatedModel);
          this._appEventService.change.next({
            model: 'CourseExams',
            target: {...this.model, ...exam }
          });
        } catch (error) {
          this._errorService.showError(error, {
            continueLink: '.'
          });
        }
      }
    });
  }


  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  private async validateExam(): Promise<boolean> {
    this.gradeSubmissions = await this._context.model('CourseExams/' + this.model.id + '/actions')
      .asQueryable()
      .where('actionStatus/alternateName').equal('ActiveActionStatus')
      .prepare().and('additionalResult').notEqual(null)
      .select('count(id) as totalActive')
      .getItem();
    return !!this.gradeSubmissions['totalActive'];
  }
}
