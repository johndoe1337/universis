import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import {ApplicationSettings} from '../../../registrar-shared/registrar-shared.module';
import {ConfigurationService} from '@universis/common';

@Component({
  selector: 'app-exams-uploads-preview-form',
  templateUrl: './uploads-preview-form.component.html'
})
export class UploadsPreviewFormComponent implements OnInit {

  @Input() exams: any;
  public useDigitalSignature: boolean;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _config: ConfigurationService) {
  }
  async ngOnInit() {
    this.useDigitalSignature = !! (<ApplicationSettings>this._config.settings.app).useDigitalSignature;
  }
}
