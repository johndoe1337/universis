import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import {
  AppEventService,
  DIALOG_BUTTONS,
  ErrorService,
  LoadingService,
  ModalService, SignatureInfoComponent, ToastService
} from '@universis/common';
import {Subscription} from 'rxjs';
import {TranslateService} from '@ngx-translate/core';
// tslint:disable-next-line:max-line-length
import {AdvancedSearchFormComponent, AdvancedTableComponent, AdvancedTableConfiguration, AdvancedTableSearchComponent} from '@universis/ngx-tables';
import * as EXAM_GRADES_LIST_CONFIG from './action-grades-table.config.json';
import * as EXAM_GRADES_SEARCH_CONFIG from './action-grades.search.json';
import {ActivatedTableService} from '@universis/ngx-tables';
import { SignatureVerificationComponent, VerifySignatureResult } from '@universis/ngx-signer';
import { SignerService } from '@universis/ngx-signer';


@Component({
  selector: 'app-exams-preview-grades-check',
  templateUrl: './exams-preview-grades-check.component.html',
  styleUrls: ['./exams-preview-grades-check.component.scss']
})
export class ExamsPreviewGradesCheckComponent implements OnInit, OnDestroy  {

  public courseExamId;
  public courseExamAction: any;
  public instructor: any;
  public useDigitalSignature = false;
  private subscription: Subscription;
  public actionId;
  public showCancelAction = false;
  private changeSubscription: Subscription;
  public failedGrades = [];
  public showFailed = false;
  @ViewChild('grades') grades: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  @ViewChild('advancedSearch') advancedSearch: AdvancedTableSearchComponent;

  /**
   * @property {boolean} signatureVerified Indicates whether the signature was verified
   */
  public signatureVerified: boolean;
  public attachmentBlob: Blob;
  public digitalSignatureControl = {
    'valid': 'Exams.DigitalSignatureControl.Valid',
    'invalid': 'Exams.DigitalSignatureControl.Invalid',
    'noSignature': 'Exams.DigitalSignatureControl.NoSignature',
    'cannotBeDetermined': 'Exams.DigitalSignatureControl.NotDetermined'
  };
  public digitalSignatureStatus: string;
  public verifyError: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _loadingService: LoadingService,
              private errorService: ErrorService,
              private _translateService: TranslateService,
              private _modalService: ModalService,
              private _appEvent: AppEventService,
              private _toastService: ToastService,
              private _activatedTable: ActivatedTableService,
              private _signer: SignerService
  ) { }

  async ngOnInit() {
    try {
      this._loadingService.showLoading();
      this.subscription = this._activatedRoute.params.subscribe(async (params) => {
        this.courseExamId = this._activatedRoute.snapshot.parent.params.id;
        this.actionId = params.id;
        await this.load();
      });
      this.changeSubscription = this._appEvent.change.subscribe(async (event) => {
        if (event && (event.model === 'ExamDocumentUploadActions')
          && event.target && this.courseExamAction && event.target.id === this.courseExamAction.id) {
          // reload
          await this.load();
        }
      });
      this._loadingService.hideLoading();
    } catch (err) {
      this._loadingService.hideLoading();
      return this.errorService.navigateToError(err);
    }
  }
private async load() {
  this.showCancelAction = false;
  const courseExamAction = await this._context.model(`CourseExams/${this.courseExamId}/actions`)
    .asQueryable()
    .expand('additionalResult,attachments,owner,grades($expand=status),object($expand=examPeriod,' +
      'course($expand=department($expand=currentPeriod,currentYear))' +
      ',classes($expand=courseClass($expand=period,year)),examPeriod,year),actionResult($expand=createdBy,actionStatus)')
    .where('id').equal(this.actionId)
    .getItem();
  // a workaround for getting actual action result
  if (courseExamAction && courseExamAction.actionResult == null) {
    // use ActionResult navigation property
    const actionResult = await this._context.model(`ExamDocumentUploadActions/${courseExamAction.id}/ActionResult`)
      .asQueryable().getItem();
    if (actionResult) {
      Object.assign(courseExamAction, {
        actionResult
      });
    }
  }
  // set action
  this.courseExamAction = courseExamAction;

  const instituteId = this.courseExamAction
                    && this.courseExamAction.object
                    && this.courseExamAction.object.course
                    && this.courseExamAction.object.course.department
                    && this.courseExamAction.object.course.department.organization;
  const institute = await this._context.model('Institutes')
      .where('id').equal(instituteId)
      .select('instituteConfiguration/useDigitalSignature as useDigitalSignature')
      .getItem();
  this.useDigitalSignature = institute && institute.useDigitalSignature;
  if (this.useDigitalSignature) {
    // verify on load, do not use await, let this be performed async
    this.verifyDocument().then((digitalSignatureStatus: string) => {
      this.digitalSignatureStatus = digitalSignatureStatus;
    });
  }
  if (this.courseExamAction.owner) {
    this.instructor = await this._context.model('Instructors')
      .where('user').equal(this.courseExamAction.owner.id)
      .getItem();
  }
  // get related requestDocumentActions
  if (this.courseExamAction.grades && this.courseExamAction.grades.length > 0) {
    this.getActionGrades();
  }
  this.signatureVerified = false;
  if (this.courseExamAction
      && this.courseExamAction.actionResult
      && this.courseExamAction.actionResult.actionStatus
      && this.courseExamAction.actionResult.actionStatus.alternateName === 'ActiveActionStatus') {
      const now = new Date();
      const startTime = new Date(this.courseExamAction.actionResult.startTime || this.courseExamAction.actionResult.dateCreated);
      const diff = (now.valueOf() - startTime.valueOf()) / 60000;
      if (diff > 1) {
        // show cancel button after a minute has passed
        this.showCancelAction = true;
      }
  }
  if (this.courseExamAction && this.courseExamAction.grades && this.courseExamAction.grades.length) {
    this.failedGrades = this.courseExamAction.grades.filter(grade => grade.status.alternateName === 'failed');
    this.showFailed = !!((this.courseExamAction && this.courseExamAction.actionResult
        && (this.courseExamAction.actionResult.additionalType === 'AcceptAction'
        || this.courseExamAction.actionResult.additionalType === 'ExamDocumentUploadAction')
        && this.courseExamAction.actionResult.actionStatus.alternateName === 'CompletedActionStatus')
        && (this.failedGrades && this.failedGrades.length));
  }
}
  attachedGrades(attachments) {
    const headers = new Headers();
    const serviceHeaders = this._context.getService().getHeaders();
    Object.keys(serviceHeaders).forEach((key) => {
      if (serviceHeaders.hasOwnProperty(key)) {
        headers.set(key, serviceHeaders[key]);
      }
    });
    const attachURL = attachments[0].url.replace(/\\/g, '/').replace('/api', '');
    const fileURL = this._context.getService().resolve(attachURL);
    fetch(fileURL, {
      headers: headers,
      credentials: 'include'
    }).then((response) => {

      return response.blob();
    })
      .then(blob => {
        const objectUrl = window.URL.createObjectURL(blob);
        const a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.href = objectUrl;
        a.download = `${attachments[0].name}`;
        a.click();
        window.URL.revokeObjectURL(objectUrl);
        a.remove();
      });
  }

  getActionGrades() {
    this._activatedTable.activeTable = this.grades;
    this.grades.query = this._context.model(`ExamDocumentUploadActions/${this.courseExamAction.id}/grades`)
      .asQueryable()
      .expand('status')
      .prepare();
    this.grades.config = AdvancedTableConfiguration.cast(EXAM_GRADES_LIST_CONFIG);
    if (this.search) {
      this.search.form = AdvancedTableConfiguration.cast(EXAM_GRADES_SEARCH_CONFIG);
      this.search.ngOnInit();
    }
    this.grades.ngOnInit();
  }

  async changeStatus(status) {
    try {
      const title = status === 'approve' ?
        this._translateService.instant('Exams.AcceptGrades.title') :
        this._translateService.instant('Exams.RejectGrades.title');
      const message = status === 'approve' ?
        this._translateService.instant('Exams.AcceptGrades.message') :
        this._translateService.instant('Exams.RejectGrades.message');

      const dialogResult = await this._modalService.showDialog(title, message, DIALOG_BUTTONS.YesNo);
      if (dialogResult === 'no') {
        return;
      }
      this._loadingService.showLoading();

      // and finally save action
      await this._context.model(`ExamDocumentUploadActions/${this.actionId}/${status}`).save(null);
      this._appEvent.change.next({
        model: 'ExamDocumentUploadActions',
        target: this.courseExamAction
      });
      this._toastService.show(
        this._translateService.instant(status === 'approve' ? `Exams.AcceptGrades.title` : `Exams.RejectGrades.title`),
        this._translateService.instant(status === 'approve' ? `Exams.AcceptAction.ToastInfo` : `Exams.RejectAction.ToastInfo`),
      );
      this._loadingService.hideLoading();
    } catch (err) {
      this._loadingService.hideLoading();
      console.log(err);
      return this.errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  /**
   *
   * Opens the certification modal
   *
   */
  showCertificateModal(): void {
    this._modalService.openModalComponent(
      SignatureInfoComponent,
      {
        animated: true,
        class: 'modal-dialog-centered modal-xl border-0',
        ignoreBackdropClick: true,
        initialState: {
          isVerified: this.signatureVerified,
          user: this.instructor,
          certificate: this.courseExamAction.additionalResult.userCertificate,
          signatureBlock: this.courseExamAction.additionalResult.signatureBlock,
          checkHashKey: this.courseExamAction.additionalResult.checkHashKey,
          examinationCode: this.courseExamAction.code,
          dateCreated: this.courseExamAction.dateCreated
        }
      }
    );
  }

  verifySignatures(): void {
    try {
      this._modalService.openModalComponent(SignatureVerificationComponent, {
        class: 'modal-xl',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          file: this.attachmentBlob
        }
      });
    } catch (err) {
      console.error(err);
      this.errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  verifyDocument(): Promise<string> {
    return new Promise((resolve, reject) => {
      // get attachment url
      const attachmentUrl = this.courseExamAction
      && this.courseExamAction.attachments
      && this.courseExamAction.attachments[0]
      && this.courseExamAction.attachments[0].url;
      if (attachmentUrl) {
            // fetch blob
            this.prepareBlob(attachmentUrl).then(blob => {
                this.attachmentBlob = blob;
                Object.assign(this.attachmentBlob, {name: 'file.xlsx'});
                // verify
                this._signer.verifyDocument(this.attachmentBlob).then((results: VerifySignatureResult[]) => {
                  if (results.length === 0) {
                    return resolve('noSignature');
                  }
                  for (const result of results) {
                    if (result.certificates.length === 0) {
                      return resolve('noSignature');
                    }
                    if (!result.valid) {
                      return resolve('invalid');
                    }
                  }
                  return resolve('valid');
                }).catch(err => {
                  this.verifyError = err;
                  console.error(err);
                  return resolve('cannotBeDetermined');
                });
              }).catch(err => {
                console.error(err);
                return resolve('cannotBeDetermined');
              });
        } else {
          return resolve('cannotBeDetermined');
        }
      });
  }

  prepareBlob(url: string): Promise<Blob> {
    return new Promise((resolve, reject) => {
      const headers = new Headers();
      const serviceHeaders = this._context.getService().getHeaders();
      Object.keys(serviceHeaders).forEach((key) => {
        if (serviceHeaders.hasOwnProperty(key)) {
          headers.set(key, serviceHeaders[key]);
        }
      });
      const attachURL = url.replace(/\\/g, '/').replace('/api', '');
      const fileURL = this._context.getService().resolve(attachURL);
      fetch(fileURL, {
        headers: headers,
        credentials: 'include'
      }).then((response) => {
        return resolve(response.blob());
      }).catch(err => {
        return reject(err);
      });
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.changeSubscription) {
      this.changeSubscription.unsubscribe();
    }
  }

  async cancelChangeStatusAction(entitySet: string) {
    // validate that action has not already been completed
    const changeStatusAction = await this._context.model('ExamDocumentUploadActions')
      .where('id').equal(this.courseExamAction && this.courseExamAction.id)
      .select('actionResult/actionStatus/alternateName as status')
      .getItem();
    if (changeStatusAction.status !== 'ActiveActionStatus') {
      return this._modalService.showWarningDialog(this._translateService.instant('Exams.ActionAlreadyCompleted.Title'),
        this._translateService.instant('Exams.ActionAlreadyCompleted.Description'));
    }
    return this._modalService.showWarningDialog(
      this._translateService.instant(entitySet === 'AcceptActions'
                                      ? 'Exams.AcceptAction.CancelAction.ModalTitle'
                                      : 'Exams.RejectAction.CancelAction.ModalTitle'),
      this._translateService.instant(entitySet === 'AcceptActions'
                                      ? 'Exams.AcceptAction.CancelAction.ModalDescription'
                                      : 'Exams.RejectAction.CancelAction.ModalDescription'),
      DIALOG_BUTTONS.OkCancel).then((result: string) => {
      // if the user confirms the action, proceed
      if (result === 'ok') {
        // show loading
        this._loadingService.showLoading();
        // prepare minimal action result object
        const actionResult = {
          id: this.courseExamAction.actionResult && this.courseExamAction.actionResult.id,
          actionStatus: {
            alternateName: 'CancelledActionStatus'
          }
        };
        return this._context.model(entitySet).save(actionResult).then(() => {
          this._appEvent.change.next({
            model: 'ExamDocumentUploadActions',
            target: this.courseExamAction
          });
          this._loadingService.hideLoading();
        }).catch(err => {
          this._loadingService.hideLoading();
          this.errorService.showError(err, {
            continueLink: '.'
          });
        });
      }
    });
  }

}
