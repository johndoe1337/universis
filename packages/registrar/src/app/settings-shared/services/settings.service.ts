import { Injectable } from '@angular/core';
import { Args } from '@themost/client';
import { BehaviorSubject } from 'rxjs';

export declare interface SettingsSection {
  category?: string;
  name: string;
  description: string;
  longDescription: string;
  url?: string;
  dependencies?: string[];
  disabled?: boolean;
  hidden?: boolean;
}

@Injectable()
export class SettingsService {

  private _sections: SettingsSection[] = [];

  public sections: BehaviorSubject<Array<SettingsSection>> = new BehaviorSubject(this._sections);

  constructor() {
  }

  addSection(...section: SettingsSection[]): SettingsService {
    if (section.length === 0) {
      return this;
    }
    section.forEach( item => {
      Args.check( !!(item && item.name), 'Settings section is invalid. It\'s either null or invalid');
      const findSection = this._sections.find( x => {
        return x.name === item.name;
      });
      if (findSection == null) {
        this._sections.push(item);
      }
    });
    this.sections.next(this._sections);
    return this;
  }

  removeSection(name: string): SettingsSection {
    const findSectionIndex = this._sections.findIndex( x => {
      return x.name === name;
    });
    let item: SettingsSection;
    if (findSectionIndex >= 0) {
      item = Object.assign({}, this._sections[findSectionIndex]);
      this._sections.splice(findSectionIndex, 1);
    }
    this.sections.next(this._sections);
    return item;
  }

}



