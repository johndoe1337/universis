import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import * as GRADUATIONS_GRADUATES_LIST_CONFIG from './graduations-graduates.config.json';
import * as GRADUATIONS_GRADUATES_SEARCH_CONFIG from './graduations-graduates.search.json';
import {
  AdvancedRowActionComponent,
  AdvancedTableComponent,
  AdvancedTableConfiguration,
  AdvancedTableDataResult
} from '@universis/ngx-tables';
import {AdvancedSearchFormComponent} from '@universis/ngx-tables';
import { ErrorService, LoadingService, ModalService } from '@universis/common';
import { ClientDataQueryable } from '@themost/client';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-graduations-graduates',
  templateUrl: './graduations-graduates.component.html'
})
export class GraduationsGraduatesComponent implements OnInit {

  public recordsTotal: any;
  @ViewChild('graduates') graduates: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  graduationEventId: any = this._activatedRoute.snapshot.params.id;
  private selectedItems: any[] = [];
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();


  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _loadingService: LoadingService,
              private _modalService: ModalService,
              private _errorService: ErrorService

  ) { }

  async ngOnInit() {
    this.graduates.query = this._context.model('GraduationRequestActions')
      .where('graduationEvent').equal(this.graduationEventId)
      .expand('student($expand=studyProgram,department,person,semester,requeststatus,inscriptionYear,inscriptionPeriod,studentStatus)')
      .prepare();

    this.graduates.config = AdvancedTableConfiguration.cast(GRADUATIONS_GRADUATES_LIST_CONFIG);
    this.search.form = AdvancedTableConfiguration.cast(GRADUATIONS_GRADUATES_SEARCH_CONFIG);
    this.graduates.ngOnInit();
    this.search.ngOnInit();

    }

    onDataLoad(data: AdvancedTableDataResult) {
      this.recordsTotal = data.recordsTotal;
    }

    async cancelGraduation() {
      try {
        this._loadingService.showLoading();
        const items = await this.getSelectedItems();
        const validationPromises = items.map(async (item) => {
          return {
            id: item.id,
            studentId: item.studentId,
            graduateAction: await this.getCompletedGraduateAction(item.id),
          };
        });
        // filter items with completed graduate actions
        this.selectedItems = (await Promise.all(validationPromises)).filter(
          (item) => !!item.graduateAction
        );
        this._loadingService.hideLoading();
        this._modalService.openModalComponent(AdvancedRowActionComponent, {
          class: 'modal-lg',
          keyboard: false,
          ignoreBackdropClick: true,
          initialState: {
            items: this.selectedItems,
            modalTitle: 'Graduations.ResetToDeclaredAction.Title',
            description: 'Graduations.ResetToDeclaredAction.Description',
            errorMessage:
              'Graduations.ResetToDeclaredAction.CompletedWithErrors',
            refresh: this.refreshAction,
            execute: this.executeCancelGraduation(),
          },
        });
      } catch (err) {
        this._loadingService.hideLoading();
        this._errorService.showError(err, {
          continueLink: '.',
        });
      }
    }

    executeCancelGraduation() {
      return new Observable((observer) => {
        const total = this.selectedItems.length;
        const result = {
          total: this.selectedItems.length,
          success: 0,
          errors: 0,
        };
        this.refreshAction.emit({
          progress: 1,
        });
        // execute promises in series within an async method
        (async () => {
          for (let index = 0; index < this.selectedItems.length; index++) {
            try {
              const item = this.selectedItems[index];
              // set progress
              this.refreshAction.emit({
                progress: Math.floor(((index + 1) / total) * 100),
              });
              // get student graduate action
              const graduateAction = item.graduateAction;
              // cancel it
              graduateAction.actionStatus = {
                alternateName: 'CancelledActionStatus'
              };
              // and save
              await this._context.model('StudentGraduateActions').save(graduateAction);
              result.success += 1;
            } catch (err) {
              // log error
              console.error(err);
              result.errors += 1;
            }
          }
        })()
          .then(() => {
            this.graduates.fetch();
            observer.next(result);
          })
          .catch((err) => {
            observer.error(err);
          });
      });
    }

    async getSelectedItems() {
      let items = [];
      if (this.graduates && this.graduates.lastQuery) {
        const lastQuery: ClientDataQueryable = this.graduates.lastQuery;
        if (lastQuery != null) {
          if (this.graduates.smartSelect) {
            // get items
            const selectArguments = [
              'id',
              'student/id as studentId'
            ];
            // query items
            const queryItems = await lastQuery.select
              .apply(lastQuery, selectArguments)
              .take(-1)
              .skip(0)
              .getItems();
            if (this.graduates.unselected && this.graduates.unselected.length) {
              // remove items that have been unselected by the user
              items = queryItems.filter((item) => {
                return (
                  this.graduates.unselected.findIndex((x) => {
                    return x.id === item.id;
                  }) < 0
                );
              });
            } else {
              items = queryItems;
            }
          } else {
            // get selected items only
            items = this.graduates.selected.map((item) => {
              return {
                id: item.id,
                studentId: item.studentId
              };
            });
          }
        }
      }
      return items;
    }

    getCompletedGraduateAction(initiator: number | string): Promise<any> {
      if (initiator == null) {
        return Promise.resolve();
      }
      return this._context.model('StudentGraduateActions')
        .where('initiator').equal(initiator)
        .and('actionStatus/alternateName').equal('CompletedActionStatus')
        .orderByDescending('id')
        .getItem();
    }
}
