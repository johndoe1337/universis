{
  "Graduations": {
    "Title": "Graduations",
    "TitleSingular": "Graduation",
    "All": "All",
    "Opened": "Opened",
    "Any": "Any",
    "New": "New",
    "Export": "Export",
    "Filter": "Search",
    "CurrentYear": "Current academic year",
    "SearchPlaceholder": "Graduation title, academic year, etc.",
    "GraduationTitle": "Title",
    "GraduationDate": "Date",
    "Id": "Id",
    "Year": "Year",
    "Period": "Period",
    "Status": "Status",
    "DateCompleted": "Date completed",
    "CompletedByUser": "Completed by",
    "Preview": "Preview",
    "Students": "Students",
    "EditDate": "Edit Date",
    "Time": "Time",
    "Check": "Check",
    "Show": "Show",
    "SuccessfulCheck": "Successful check",
    "FailedCheck": "Failed check",
    "Reject": "Reject",
    "Approve": "Approve",
    "BackToList": "Back to list",
    "Choose": "Choose",
    "Statistics": "Statistics",
    "Totals": "Sums",
    "Username": "Username",
    "ChooseDate": "Choose date",
    "Info": "Graduation information",
    "NoGeneralInfo": "No information",
    "Candidates": "Candidates",
    "Admitted": "Admitted",
    "Graduated": "Graduated",
    "RequestPeriodStart": "Request period start",
    "RequestPeriodEnd": "Request period end",
    "NewGraduation": "New graduation",
    "EditGraduation": "Edit graduation",
    "StepOf": "Step {{step}} of {{steps}}",
    "EnterTitle": "Enter a title for this graduation event",
    "GraduationTime": "Start time",
    "MaximumAttendeeCapacity": "Capacity",
    "SendNotificationOnMax": "Send notification when exceed maximum number of requests",
    "Place": "Location",
    "RequestPeriod": "Request period",
    "ValidFrom": "From",
    "ValidThrough": "To",
    "RequestAttachments": "The following list contains the available documents which may be attached in an application form. Select the documents that should be attached in a graduation application form before submission.",
    "RequestAttachmentsHelp": "Students should upload the selected document before final submission.",
    "AutoRequestDocuments": "The following list contains the available report templates that are associated to certificate requests. Select one or more of them which are going to be generated automatically after completion.",
    "AutoRequestDocumentsHelp": "Students will be informed for those documents and will be able to download them after request completion.",
    "TitleHelp": "Set a title for this graduation event which will be visible to students during applying for this event.",
    "AcademicYearPeriodHelp": "Select academic year and period for this graduation event.",
    "LocationHelp": "Set the location where this graduation event will take place.",
    "MaximumAttendeeCapacityHelp": "Set the maximum number of students that are going to be in this event. System will be inform if the number of students exceeds this limit.",
    "Overview": "Overview",
    "Requests": "Requests",
    "General": "General",
    "GeneralUpper": "GENERAL",
    "Event": "EVENT",
    "AttachmentTypes": "ATTACHMENTS",
    "ReportTemplates": "REPORTS",
    "Date": "Date",
    "GraduationLocation": "Graduation location",
    "Location": "Location",
    "Grade": "Grade",
    "Contact": "Contact",
    "PreviewStudent": "Preview student",
    "PreviewRequest": "Preview request",
    "Details": "Details",
    "StudentDetails": "Student Details",
    "StudentTranscript": "Student Transcript",
    "AverageGrade": "Average Degree Grade",
    "ECTS": "ECTS",
    "Units": "Units",
    "Request": "Request",
    "Graduates": "Graduates",
    "EventDetails": "Event Details",
    "SubmissionDocuments": "Documents to submit",
    "NoDocuments": "There are no documents to submit",
    "CertificateDocuments": "Certificate documents",
    "Certificates": "Certificates",
    "NoCertificates": "There are no certificates",
    "StudentsSingular": "Student",
    "StudentsPlural": "Students",
    "GraduateCapacity": "Graduate capacity",
    "Capacity": "Capacity",
    "Available": "Available",
    "Occupied": "Occupied",
    "Start": "Start",
    "End": "End",
    "DocumentSubmissionPeriod": "Document Submission Period",
    "DocumentSubmissionPeriodStart": "Document submission period start",
    "DocumentSubmissionPeriodEnd": "Document submission period end",
    "RelatedGraduations": "Related Graduations",
    "NoRelatedGraduations": "There are no related graduations",
    "Relation": "Relation",
    "Parent": "Parent",
    "Child": "Child",
    "Url": "Url location",
    "UrlHelp": "Set the url location where this graduation event will take place.",
    "Error": "An error occurred while trying to save graduation event",
    "GraduationDateRequired": "Graduation date is required",
    "CheckDates": "Check the graduation date and the request period dates.",
    "AttachmentValid": "Required Attachments",
    "GraduationValid": "Student Graduation Rules",
    "Edit": {
      "Label": "Edit",
      "Title": "Graduations Requests",
      "accept": "Accept",
      "reject": "Reject",
      "AcceptAction": {
        "Title": "Accept requests",
        "Description": "The operation will try to complete the selected requests. The process is valid for pending requests only."
      },
      "RejectAction": {
        "Title": "Reject requests",
        "Description": "The operation will try to reject the selected requests. Request owners will be informed for this operation by the system interfaces. The process is valid for pending requests only."
      },
      "ValidateAction": {
        "Title": "Validation",
        "Description":"The validation process will recalculate student graduation rules and the verification of the required documents for all requests. The process is valid for pending requests only."
      },
      "CommunicateWithAttendees": {
        "Title": "Communicate with students",
        "Description": "Please complete the subject and body of the message you want to send to the students and press send."
      }
    },
    "Virtual": "Online event",
    "VirtualHelp": "Specify if the event will take place online",
    "ChangeStatus": "Change status",
    "AddStudents": {
      "Remove": "Remove",
      "Title": "Create Graduation Request",
      "many": "{{value}} graduation requests were successfully created.",
      "one": "A graduation request was successfully created.",
      "manyFail": "{{value}} graduation requests could not be created.",
      "oneFail": "A graduation request could not be created."
    },
    "Alerts": {
      "WrongPeriodYear": "The event period does not coincide with the current academic term, so requests cannot be added.",
      "WrongDates": "Requests can be added from {{startDate}} to {{endDate}}.",
      "CannotCreateRequestWithAttachments": "It is not possible to add a request to an event that requires attachment documents.",
      "CanCreateRequest": "You can add new requests to the graduation event."
    },
    "RequestTitle": "Student graduation request from registrar",
    "RequestDescription": "Student graduation request from registrar",
    "DeclaredStudents": "Declared students",
    "LastObligationDate": "Last obligation date",
    "SetLastObligationDate": "Set last obligation date",
    "CompleteGraduation": "Complete graduation",
    "CancelGraduation": "Cancel graduation",
    "CompleteDeclarationAction": {
      "Title": "Complete the students' declarations",
      "Description": "With this action you can complete the selected students' declarations, given that an active student declaration action exists for them. This process is identical to the one found inside the graduation request's preview.",
      "CompletedWithErrors": {
        "Title": "The operation was completed with errors",
        "Description": {
          "One": "A declaration of a student did not get completed due to an error",
          "Many": "The declarations of {{errors}} students did not get completed due to an error"
        }
      }
    },
    "CancelDeclarationAction": {
      "Title": "Cancel the students' declarations",
      "Description": "With this action you can cancel the selected students' declarations, given that an active student declaration action exists for them. This process is identical to the one found inside the graduation request's preview.",
      "CompletedWithErrors": {
        "Title": "The operation was completed with errors",
        "Description": {
          "One": "A declaration of a student did not get cancelled due to an error",
          "Many": "The declarations of {{errors}} students did not get cancelled due to an error"
        }
      }
    },
    "CompleteGraduationAction": {
      "Title": "Complete the students' graduations",
      "Description": "With this action you can completed the selected students' graduations (and graduate them), given that an active student graduation action exists for them. This process is identical to the one found inside the graduation request's preview.",
      "CompletedWithErrors": {
        "Title": "The operation was completed with errors",
        "Description": {
          "One": "A graduation of a student did not get completed due to an error",
          "Many": "The graduations of {{errors}} students did not get completed due to an error"
        }
      }
    },
    "CancelGraduationAction": {
      "Title": "Cancel the students' graduations",
      "Description": "With this action you can cancel the selected students' graduations, given that an active student graduation action exists for them. This process is identical to the one found inside the graduation request's preview.",
      "CompletedWithErrors": {
        "Title": "The operation was completed with errors",
        "Description": {
          "One": "A graduation of a student did not get cancelled due to an error",
          "Many": "The graduations of {{errors}} students did not get cancelled due to an error"
        }
      }
    },
    "SetLastObligationDateAction": {
      "Title": "Set the students' last obligation date",
      "Description": "With this action you can set the selected students' last obligation date.",
      "CompletedWithErrors": {
        "Title": "The operation was completed with errors",
        "Description": {
          "One": "A student's last obligation date did not get updated due to an error",
          "Many": "{{errors}} students' last obligation dates did not get updated due to an error"
        }
      }
    },
    "InvalidLastObligationDate": "The last obligation date is invalid",
    "ResetToDeclaredAction": {
      "Title": "Cancel the students' graduation",
      "Description": "With this action you can cancel the selected students' graduations and set their status to declared. This action can only be performed for students that have a completed graduation action for this event. If you wish, at a later stage, to make them graduated again, you can use the \"Complete declaration\" action which is located inside the student's preview page.",
      "CompletedWithErrors": {
        "Title": "The operation was completed with errors",
        "Description": {
          "One": "A graduation of a student did not get cancelled due to an error",
          "Many": "The graduations of {{errors}} students did not get cancelled due to an error"
        }
      }
    }
  }
}
