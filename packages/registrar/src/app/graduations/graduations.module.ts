import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GraduationsRootComponent } from './components/graduations-root/graduations-root.component';
import { GraduationsHomeComponent } from './components/graduations-home/graduations-home.component';
import { GraduationsTableComponent } from './components/graduations-table/graduations-table.component';
import { GraduationsSharedModule } from './graduations.shared';
import { TranslateModule } from '@ngx-translate/core';
import { TablesModule } from '@universis/ngx-tables';
import { SharedModule } from '@universis/common';
import { FormsModule } from '@angular/forms';
import { MostModule } from '@themost/angular';
import { RegistrarSharedModule } from '../registrar-shared/registrar-shared.module';
import { RouterModalModule } from '@universis/common/routing';
import { GraduationsRoutingModule } from './graduations.routing';
import {RouterModule} from '@angular/router';
import {GraduationEditComponent} from './components/graduation-edit/graduation-edit.component';
import {StudyProgramsSharedModule} from '../study-programs/study-programs.shared';
import {StudentsSharedModule} from '../students/students.shared';
import {BsDatepickerModule, TabsModule, TimepickerModule} from 'ngx-bootstrap';
import {ElementsModule} from '../elements/elements.module';
import { GraduationsDashboardComponent } from './components/graduations-dashboard/graduations-dashboard.component';
import { GraduationsOverviewComponent } from './components/graduations-dashboard/graduations-overview/graduations-overview.component';
import { GraduationsStudentRequestsComponent } from './components/graduations-dashboard/graduations-student-requests/graduations-student-requests.component';
import { GraduationsGeneralComponent } from './components/graduations-dashboard/graduations-general/graduations-general.component';
import { GraduationsOverviewCapacityComponent } from './components/graduations-dashboard/graduations-overview/graduations-overview-capacity/graduations-overview-capacity.component';
import { GraduationsOverviewCertificatesComponent } from './components/graduations-dashboard/graduations-overview/graduations-overview-certificates/graduations-overview-certificates.component';
import { GraduationsOverviewDocumentsComponent } from './components/graduations-dashboard/graduations-overview/graduations-overview-documents/graduations-overview-documents.component';
import { GraduationsOverviewGeneralComponent } from './components/graduations-dashboard/graduations-overview/graduations-overview-general/graduations-overview-general.component';
import {ChartsModule} from 'ng2-charts';
import {GraduationsGraduatesComponent} from './components/graduations-dashboard/graduations-graduates/graduations-graduates.component';
import {GraduationsRequestsRootComponent} from './components/graduations-dashboard/graduations-student-requests/graduations-requests-root/graduations-requests-root.component';
import {MessagesSharedModule} from '../messages/messages.shared';
import {SendMessageToStudentComponent} from '../messages/components/send-message-to-student/send-message-to-student.component';
import { GraduationsDeclaredStudentsComponent } from './components/graduations-dashboard/graduations-declared-students/graduations-declared-students/graduations-declared-students.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule,
    GraduationsSharedModule,
    GraduationsRoutingModule,
    TablesModule,
    SharedModule,
    FormsModule,
    MostModule,
    RegistrarSharedModule,
    RouterModalModule,
    StudyProgramsSharedModule,
    StudentsSharedModule,
    TabsModule.forRoot(),
    BsDatepickerModule.forRoot(),
    TimepickerModule.forRoot(),
    ElementsModule,
    ChartsModule,
    MessagesSharedModule
  ],
  declarations: [
    GraduationsRootComponent,
    GraduationsHomeComponent,
    GraduationsTableComponent,
    GraduationEditComponent,
    GraduationsDashboardComponent,
    GraduationsOverviewComponent,
    GraduationsStudentRequestsComponent,
    GraduationsGeneralComponent,
    GraduationsOverviewCapacityComponent,
    GraduationsOverviewCertificatesComponent,
    GraduationsOverviewDocumentsComponent,
    GraduationsOverviewGeneralComponent,
    GraduationsGraduatesComponent,
    GraduationsRequestsRootComponent,
    GraduationsDeclaredStudentsComponent
  ],
  entryComponents: [
    SendMessageToStudentComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GraduationsModule { }
