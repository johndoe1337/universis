import {Injectable} from '@angular/core';
import {TableConfiguration} from '@universis/ngx-tables';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
export class ThesesTableConfigurationResolver implements Resolve<TableConfiguration> {
  resolve(route: ActivatedRouteSnapshot,
          state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
    return import(`./theses-table.config.${route.params.list}.json`)
      .catch( err => {
        return  import(`./theses-table.config.json`);
      });
  }
}

export class ThesesTableSearchResolver implements Resolve<TableConfiguration> {
  resolve(route: ActivatedRouteSnapshot,
          state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
    return import(`./theses-table.search.${route.params.list}.json`)
      .catch( err => {
        return  import(`./theses-table.search.json`);
      });
  }
}

export class ThesesDefaultTableConfigurationResolver implements Resolve<any> {
  resolve(route: ActivatedRouteSnapshot,
          state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return import(`./theses-table.config.json`);
  }
}
