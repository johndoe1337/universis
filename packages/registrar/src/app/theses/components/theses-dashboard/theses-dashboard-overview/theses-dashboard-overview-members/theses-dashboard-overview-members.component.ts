import {Component, EventEmitter, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import {TranslateService} from '@ngx-translate/core';
// tslint:disable-next-line:max-line-length
import {AdvancedTableComponent, AdvancedTableConfiguration, AdvancedTableDataResult} from '@universis/ngx-tables';
import {DIALOG_BUTTONS, ErrorService, ModalService, ToastService} from '@universis/common';
import {ActivatedTableService} from '@universis/ngx-tables';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-theses-dashboard-overview-members',
  templateUrl: './theses-dashboard-overview-members.component.html',
})
export class ThesesDashboardOverviewMembersComponent implements OnInit, OnDestroy {

  private subscription: Subscription;
  public model: any;

  @ViewChild('members') members: AdvancedTableComponent;
  thesesID: any = this._activatedRoute.snapshot.params.id;

  constructor(private _activatedRoute: ActivatedRoute,
              private _activatedTable: ActivatedTableService,
              private _context: AngularDataContext) {}

  async ngOnInit() {

    this._activatedTable.activeTable = this.members;

    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.model = await this._context.model('ThesisRoles')
        .where('thesis').equal(params.id)
        .expand('member($expand=department)')
        .getItems();
      this.thesesID = params.id;
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

}
