import { Component, OnDestroy, OnInit, } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AngularDataContext } from '@themost/angular';
import { TranslateService } from '@ngx-translate/core';
import { RouterModalOkCancel } from '@universis/common/routing';
import { LoadingService } from '@universis/common';
import { ErrorService } from '@universis/common';

@Component({
  selector: 'app-theses-dashboard-members-factors',
  templateUrl: './theses-dashboard-members-factors.component.html',
})

export class ThesesDashboardMembersFactorsComponent
  extends RouterModalOkCancel
  implements OnInit, OnDestroy {

  private subscription: Subscription;
  public model: any;
  public thesisId: any;
  public lastError: any;

  constructor(protected _router: Router,
              protected _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _translateService: TranslateService,
              private _loadingService: LoadingService,
              private _errorService: ErrorService) {
    // call super constructor.
    super(_router, _activatedRoute);
  }

  ngOnInit() {
    this._loadingService.showLoading();
    // set up modal.
    this.modalTitle = this._translateService.instant('Theses.EditFactors');
    this.okButtonText = this._translateService.instant('Theses.Submit');
    this.cancelButtonText = this._translateService.instant('Theses.Cancel');
    this.modalClass = 'modal-xl';
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      // fetch thesis roles.
      this.model = await this._context.model('ThesisRoles')
        .where('thesis').equal(params.id)
        .expand('member')
        .getItems();
        this.thesisId = params.id;
        this._loadingService.hideLoading();
    });
  }

  async ok() {
    // clear last error.
    this.lastError = null;
    // gather factors to be saved.
    const factorsToBeSaved = this.model.map(member => {
      return {
        thesis: this.thesisId,
        member: member.member.id,
        factor: parseFloat(member.factor)
      };
    });
    // save factors.
    try {
      await this._context.model(`Theses/${this.thesisId}/roles`).save(factorsToBeSaved);
    } catch (err) {
      this.lastError = err;
      return false;
    }
    // close
    return this.close({
      fragment: 'reload',
      skipLocationChange: true
    });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  async cancel() {
    // close.
    return this.close();
  }
}
