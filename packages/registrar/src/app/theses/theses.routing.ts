import {NgModule} from '@angular/core';
import {NavigationExtras, RouterModule, Routes} from '@angular/router';
import {ThesesHomeComponent} from './components/theses-home/theses-home.component';
import {ThesesTableComponent} from './components/theses-table/theses-table.component';
import {ThesesRootComponent} from './components/theses-root/theses-root.component';
import {ThesesPreviewComponent} from './components/theses-preview/theses-preview.component';
// tslint:disable-next-line:max-line-length
import {AdvancedFormItemWithLocalesResolver, AdvancedFormRouterComponent} from '../registrar-shared/advanced-form-router/advanced-form-router.component';
import { AdvancedFormItemResolver, AdvancedFormModalComponent, AdvancedFormModalData, AdvancedFormModalOptions } from '@universis/forms';
import {StudentsSharedModule} from '../students/students.shared';
import {ThesesAddStudentComponent} from './components/theses-dashboard/theses-dashboard-students/theses-add-student.component';
import {
  ThesesTableConfigurationResolver,
  ThesesTableSearchResolver
} from './components/theses-table/theses-table-config.resolver';
import {ThesesSharedModule} from './theses.shared';
import {ThesesDashboardComponent} from './components/theses-dashboard/theses-dashboard.component';
// tslint:disable-next-line:max-line-length
import {ThesesDashboardOverviewComponent} from './components/theses-dashboard/theses-dashboard-overview/theses-dashboard-overview.component';
// tslint:disable-next-line:max-line-length
import {ThesesDashboardStudentsComponent} from './components/theses-dashboard/theses-dashboard-students/theses-dashboard-students.component';
import {ThesesDashboardGradesComponent} from './components/theses-dashboard/theses-dashboard-grades/theses-dashboard-grades.component';
import {
  ActiveDepartmentResolver,
  CurrentAcademicPeriodResolver,
  CurrentAcademicYearResolver
} from '../registrar-shared/services/activeDepartmentService.service';
import {ThesesDashboardMembersComponent} from './components/theses-dashboard/theses-dashboard-members/theses-dashboard-members.component';
import {ThesesAddMemberComponent} from './components/theses-dashboard/theses-dashboard-members/theses-add-member.component';
import {InstructorsSharedModule} from '../instructors/instructors.shared';
// tslint:disable-next-line:max-line-length
import { ThesesDashboardMembersFactorsComponent } from './components/theses-dashboard/theses-dashboard-members/theses-dashboard-members-factors/theses-dashboard-members-factors.component';
import { ModalOptions } from 'ngx-bootstrap';


const routes: Routes = [
    {
        path: '',
        component: ThesesHomeComponent,
        data: {
            title: 'Theses'
        },
      children: [
        {
          path: '',
          pathMatch: 'full',
          redirectTo: 'list/active'
        },
        {
          path: 'list',
          pathMatch: 'full',
          redirectTo: 'list/index'
        },
        {
          path: 'list/:list',
          component: ThesesTableComponent,
          data: {
            title: 'StudentTheses'
          },
          resolve: {
            tableConfiguration: ThesesTableConfigurationResolver,
            searchConfiguration: ThesesTableSearchResolver,
            department: ActiveDepartmentResolver
          },

        }
      ]
    },
  {
    path: 'create',
    component: ThesesRootComponent,
    children: [
      {
        path: 'new',
        pathMatch: 'full',
        component: AdvancedFormRouterComponent,
        data: {
          status: {
            alternateName: 'active'
          },
          startDate: new Date(),
          continue:
            ['/theses/${id}/dashboard/students',
              {outlets: {modal: 'add'}}
            ]
        }
      }
    ],
    resolve: {
      department: ActiveDepartmentResolver,
      startYear : CurrentAcademicYearResolver,
      startPeriod : CurrentAcademicPeriodResolver
    }
  },
  {
      path: ':id',
      component: ThesesRootComponent,
      data: {
        title: 'Theses Home'
      },
      children: [
        {
          path: '',
          pathMatch: 'full',
          redirectTo: 'dashboard'
        },
        {
          path: 'dashboard',
          component: ThesesDashboardComponent,
          data: {
            title: 'Theses Dashboard'
          },
          children: [
            {
              path: '',
              redirectTo: 'general'
            },
            {
              path: 'general',
              component: ThesesDashboardOverviewComponent,
              data: {
                title: 'Theses Preview General'
              }
            },
            {
              path: 'students',
              component: ThesesDashboardStudentsComponent,
              data: {
                title: 'Theses Students'
              },
              children: [
                {
                  path: 'add',
                  pathMatch: 'full',
                  component: ThesesAddStudentComponent,
                  outlet: 'modal',
                  data: <AdvancedFormModalData> {
                    title: 'Theses.AddStudent',
                    config: StudentsSharedModule.ActiveStudentsList,
                  },
                  resolve: {
                    thesis: AdvancedFormItemResolver
                  }
                },
                {
                  path: ':id/edit',
                  pathMatch: 'full',
                  component: AdvancedFormModalComponent,
                  outlet: 'modal',
                  data: <AdvancedFormModalData> {
                    model: 'StudentTheses',
                    action: 'editDateCompleted',
                    closeOnSubmit: true,
                    modalOptions: <AdvancedFormModalOptions> {
                      modalClass: 'modal-md',
                    },
                    serviceQueryParams: {
                      $expand: 'thesis',
                    }
                  },
                  resolve: {
                    data: AdvancedFormItemResolver,
                  }
                }
              ]
            },
            {
              path: 'members',
              component: ThesesDashboardMembersComponent,
              data: {
                title: 'Theses Students'
              },
              children: [
                {
                  path: 'add',
                  pathMatch: 'full',
                  component: ThesesAddMemberComponent,
                  outlet: 'modal',
                  data: <AdvancedFormModalData> {
                    title: 'Theses.AddMember',
                    config: InstructorsSharedModule.InstructorsList,
                    closeOnSubmit: true,
                  },
                  resolve: {
                    thesis: AdvancedFormItemResolver
                  }
                },
                {
                  path: 'factors',
                  pathMatch: 'full',
                  component: ThesesDashboardMembersFactorsComponent,
                  outlet: 'modal'
                }
              ]
            },
            {
              path: 'grades',
              component: ThesesDashboardGradesComponent,
              data: {
                title: 'Theses Preview Grades'
              }
            }
           ]
        },
        {
          path: 'edit',
          component: AdvancedFormRouterComponent,
          data:
            {
              action: 'edit',
              serviceQueryParams: {
                $expand: 'instructor,locales,status'
              }
            },
          resolve: {
            data: AdvancedFormItemWithLocalesResolver
          }
        },
        {
          path: ':action',
          component: AdvancedFormRouterComponent,
          data:
        {
          serviceParams: {
            $expand: 'instructor,locales'
          }
        }
        }
      ]
  }
];

@NgModule({
    imports: [RouterModule.forChild(routes),
    ThesesSharedModule],
    exports: [RouterModule],
    declarations: []
})
export class ThesesRoutingModule {
}
