import {BrowserModule, Title} from '@angular/platform-browser';
import {APP_INITIALIZER, CUSTOM_ELEMENTS_SCHEMA, LOCALE_ID, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';

import {AppComponent} from './app.component';
import {SharedModule, ConfigurationService, LocalUserStorageService, UserStorageService} from '@universis/common';
import {AngularDataContext, DATA_CONTEXT_CONFIG} from '@themost/angular';
import {FullLayoutComponent} from './layouts/full-layout.component';
import {BreadcrumbsComponent} from './layouts/breadcrumb.component';
import {RouterModule} from '@angular/router';
import {AppRoutingModule} from './app.routing';
import {TranslateModule} from '@ngx-translate/core';
import {ErrorModule, AuthModule} from '@universis/common';
import {HashLocationStrategy, LocationStrategy, registerLocaleData} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {APP_LOCATIONS} from '@universis/common';
import {AppSidebarModule} from '@coreui/angular';

import * as locations from './app.locations';
import {RegistrarSharedModule} from './registrar-shared/registrar-shared.module';
import {BsDropdownModule} from 'ngx-bootstrap/dropdown';
import {CollapseModule} from 'ngx-bootstrap/collapse';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
import {ModalModule} from 'ngx-bootstrap/modal';
import {TablesModule} from '@universis/ngx-tables';
import {FormsModule} from '@angular/forms';
import {FilterPipeModule} from 'ngx-filter-pipe';
import {NgPipesModule} from 'ngx-pipes';
import {AdvancedFilterValueProvider} from '@universis/ngx-tables';
import {AppFilterValueProvider} from './registrar-shared/services/app-filter-value-provider.service';
import {FormioAppConfig, FormioModule} from 'angular-formio';
import {AdvancedFormsModule} from '@universis/forms';
import { GuestLayoutComponent } from './layouts/guest-layout.component';
import { ChartsModule } from 'ng2-charts';
import {AppConfig} from './formio-app-config';
import { PersistentUserActivityService, UserActivityService } from '@universis/common';
import { UserActivityComponent } from './layouts/user-activity/user-activity.component';
import { ReportsSharedModule } from './reports-shared/reports-shared.module';
import { SettingsSharedModule } from './settings-shared/settings-shared.module';
import { DepartmentsSharedModule } from './departments/departments.shared';
import {RequestsSharedModule} from './requests/requests.shared';

import {defineLocale} from 'ngx-bootstrap/chronos';
import {elLocale} from './graduations/locales/el-locale';
import {StudyProgramsSharedModule} from './study-programs/study-programs.shared';
import {CoursesSharedModule} from './courses/courses.shared';
import {RulesModule} from './rules';
import { InternshipsSharedModule } from './internships/internships.shared';
import {RegisterSharedModule} from './register/register.shared';
import { NgxSignerModule } from '@universis/ngx-signer';
defineLocale('el', elLocale);
import { StoreModule } from '@ngrx/store';
import { reducers, metaReducers } from './reducers';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import * as fromTables from '@universis/ngx-tables';
// locales
import en from '@angular/common/locales/en';
import el from '@angular/common/locales/el';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { MessageSubscriber } from './registrar-shared/services/MessageSubscriber';
import { ServerEventModule } from '@universis/common';
import { ExamsSharedModule } from './exams/exams.shared';
import { ExamMessageSubscriber } from './exams/services/ExamMessageSubscriber';

import { DiningSharedModule } from '@universis/ngx-dining/shared';
import { DegreeTemplatesSharedModule } from './degree-templates';

// noinspection AngularInvalidImportedOrDeclaredSymbol
@NgModule({
    declarations: [
        AppComponent,
        BreadcrumbsComponent,
        FullLayoutComponent,
        GuestLayoutComponent,
        UserActivityComponent
    ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ChartsModule,
    HttpClientModule,
    TranslateModule.forRoot(),
    SharedModule.forRoot(),
    RegistrarSharedModule.forRoot(),
    RouterModule,
    AuthModule,
    FormsModule,
    AppRoutingModule,
    ErrorModule.forRoot(),
    AppSidebarModule,
    ModalModule.forRoot(),
    AdvancedFormsModule.forRoot(),
    TablesModule,
    FilterPipeModule,
    NgPipesModule,
    CollapseModule,
    FormioModule,
    SettingsSharedModule.forRoot(),
    ReportsSharedModule.forRoot(),
    DepartmentsSharedModule.forRoot(),
    RequestsSharedModule.forRoot(),
    StudyProgramsSharedModule.forRoot(),
    CoursesSharedModule.forRoot(),
    RulesModule.forRoot(),
    InternshipsSharedModule.forRoot(),
    RegisterSharedModule.forRoot(),
    NgxSignerModule.forRoot(),
    ProgressbarModule.forRoot(),
    BsDropdownModule.forRoot(),
    DiningSharedModule.forRoot(),
    ExamsSharedModule.forRoot(),
    ServerEventModule.forRoot([
        MessageSubscriber,
        ExamMessageSubscriber
    ]),
    StoreModule.forRoot(reducers, { metaReducers }),
    StoreModule.forFeature(fromTables.featureName, fromTables.tableReducers),
    DegreeTemplatesSharedModule.forRoot()
  ],
    providers: [
        Title,
        {
            provide: DATA_CONTEXT_CONFIG, useValue: {
                base: '/',
                options: {
                    useMediaTypeExtensions: false,
                    useResponseConversion: true
                }
            }
        },
        {
            provide: APP_LOCATIONS, useValue: locations.REGISTRAR_APP_LOCATIONS
        },
        AngularDataContext,
        {
            provide: APP_INITIALIZER,
            // use APP_INITIALIZER to load application configuration
            useFactory: (configurationService: ConfigurationService, context: AngularDataContext) =>
                () => {
                // load application configuration
                    return configurationService.load().then( loaded => {
                        // register locales
                        registerLocaleData(en);
                        registerLocaleData(el);
                        context.setBase(configurationService.settings.remote.server);
                    });
                },
            deps: [ConfigurationService, AngularDataContext],
            multi: true
        },
        {
            provide: LOCALE_ID,
            useFactory: (configurationService: ConfigurationService) => {
                return configurationService.currentLocale;
            },
            deps: [ConfigurationService]
        },
        {
            provide: LocationStrategy,
            useClass: HashLocationStrategy
        },
        {
            provide: AdvancedFilterValueProvider, useClass: AppFilterValueProvider
        },
        {
          provide: UserStorageService,
          useClass: LocalUserStorageService
        },
        {
          provide: FormioAppConfig,
          useValue: AppConfig
        },
        {
          provide: UserActivityService,
          useClass: PersistentUserActivityService
        }
    ],
    bootstrap: [ AppComponent ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA ]
})
export class AppModule {

}
