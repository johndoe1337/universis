import { Component, OnInit, ViewChild, OnDestroy, Injectable } from '@angular/core';
import { AngularDataContext, AngularDataService } from '@themost/angular';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
// tslint:disable-next-line: max-line-length
import { AdvancedTableComponent, AdvancedTableDataResult, AdvancedTableConfiguration } from '@universis/ngx-tables';
import { ErrorService, DIALOG_BUTTONS, ModalService, UserActivityService } from '@universis/common';
import {ResponseError} from '@themost/client';
import { TableConfiguration } from '@universis/ngx-tables';
import { SettingsService, SettingsSection } from '../../../settings-shared/services/settings.service';
import * as ENUMERATION_LIST_CONFIG from './enumeration.config.list.json';
import cloneDeep = require('lodash/cloneDeep');
import { ToastService } from '@universis/common';
import {AdvancedFormsService} from '@universis/forms';

declare interface EdmEntityTypeMapping {
  Name: string;
  Description: string;
  LongDescription?: string;
}

@Component({
  selector: 'app-settings-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit, OnDestroy {

  public model: Array<EdmEntityTypeMapping>;
  public recordsTotal: number;
  public config: TableConfiguration;
  private subscription: Subscription;
  private sectionSubscription: Subscription;
  private reloadSubscription: Subscription;
  public section: SettingsSection;
  @ViewChild('table') table: AdvancedTableComponent;

  constructor(private _context: AngularDataContext,
    private _translateService: TranslateService,
    private _activatedRoute: ActivatedRoute,
    private _errorService: ErrorService,
    private _settings: SettingsService,
    private _toastService: ToastService,
    private _modalService: ModalService,
    private _userActivityService: UserActivityService
    ) { }
  ngOnDestroy(): void {
    if (this.sectionSubscription) {
      this.sectionSubscription.unsubscribe();
    }
    if (this.reloadSubscription) {
      this.reloadSubscription.unsubscribe();
    }
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      try {
        const metadata = await this._context.getMetadata();
        // get entityType
        const re = new RegExp(`^${params.model}$`, 'ig');
        const entitySet = metadata.EntityContainer.EntitySet.find( x => {
          return re.test(x.Name);
        });
        // if entity type is null
        if (entitySet == null) {
          // navigate to error
          return this._errorService.navigateToError(new ResponseError('EntitySet Not Found', 404));
        }
        // get entity type
        const entityType = metadata.EntityType.find( x => {
          return x.Name === entitySet.EntityType;
        });
        if (entityType == null) {
          return this._errorService.navigateToError(new ResponseError('EntityType Not Found', 404));
        }

        // get sections as promise
        this.sectionSubscription = this._settings.sections.subscribe( sections => {
          this.section = sections.find( x => {
            return x.name === entityType.Name;
          });
          // clone default configuration
          const defaultConfiguration = AdvancedTableConfiguration.cast(ENUMERATION_LIST_CONFIG, true);
          // correct edit button link
          const column = defaultConfiguration.columns.find( col => {
            return col.formatter === 'ButtonFormatter' && col.formatString === '(modal:edit)';
          });
          column.formatString = '#/settings/lists/' + entitySet.Name + '/(modal:${id}/edit)';
          // create list configuration by assigning list entity set
          const tableConfiguration = Object.assign({
            model: entitySet.Name
          }, defaultConfiguration);
          // set configuration to table
          this.table.config = tableConfiguration;
          // fetch data
          this.table.fetch(true);
          // do reload by using hidden fragment e.g. /classes#reload
          this.reloadSubscription = this._activatedRoute.fragment.subscribe(fragment => {
            if (fragment && fragment === 'reload') {
              this._toastService.show(
                this.section.description,
                this._translateService.instant('Settings.OperationCompleted')
              );
              this.table.fetch(true);
            }
          });
          // finally add user activity
          this._userActivityService.setItem({
            category: this._translateService.instant('Settings.Title'),
            description: this.section.description,
            url: window.location.hash.substring(1),
            dateCreated: new Date
          });
        });
      } catch (err) {
        console.error(err);
        return this._errorService.showError(err, {
          continueLink: '/settings'
        });
      }
    }, err => {
      console.error(err);
      return this._errorService.showError(err, {
        continueLink: '/settings'
      });
    });
  }

  /**
   * Data load event handler of advanced table component
   * @param {AdvancedTableDataResult} data
   */
  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  /**
   * Remove selected items
   */
  remove() {
    if (this.table && this.table.selected && this.table.selected.length) {
      // get items to remove
      const items = this.table.selected;
      return this._modalService.showWarningDialog(
        this._translateService.instant('Tables.RemoveItemsTitle'),
        this._translateService.instant('Tables.RemoveItemsMessage'),
        DIALOG_BUTTONS.OkCancel).then(result => {
          if (result === 'ok') {
            this._context.model(this.table.config.model).remove(items).then(() => {
              this._toastService.show(
                this.section.description + ':' + this._translateService.instant('Tables.RemoveItemMessage.title'),
                this._translateService.instant((items.length === 1 ?
                  'Tables.RemoveItemMessage.one' : 'Tables.RemoveItemMessage.many')
                  , { value: items.length })
              );
              this.table.fetch(true);
            }).catch(err => {
              this._errorService.showError(err, {
                continueLink: '.'
              });
            });
          }
        });
    }
  }

}

@Injectable({ providedIn: 'root' })
export class EnumerationModelFormResolver implements Resolve<string> {
  constructor(private _translateService: TranslateService, private _forms: AdvancedFormsService) {}
  resolve(
      route: ActivatedRouteSnapshot,
      state: RouterStateSnapshot
  ): Promise<any>|any {
    const action = route.data.action || route.params.action;
    const model = route.data.model || route.params.model;
    // check if the given form exists
    return this._forms.hasForm(`${model}/${action}`).then((value) => {
      if (value) {
        // form exists, so load form
        return this._forms.loadForm(`${model}/${action}`);
      }
      // otherwise load default form
      return import(`./forms/${action}.json`).then( formConfig => {
        const finalConfig = Object.assign(cloneDeep(formConfig), {
          model: model
        });
        // translate subtitle
        if (finalConfig.subtitle) {
          finalConfig.subtitle = this._translateService.instant(finalConfig.subtitle);
        }
        return Promise.resolve(finalConfig);
      });
    });
  }
}

@Injectable({ providedIn: 'root' })
export class EnumerationModalTitleResolver implements Resolve<any> {
  constructor(private _context: AngularDataContext, private _translateService: TranslateService) {}
  resolve(
      route: ActivatedRouteSnapshot,
      state: RouterStateSnapshot
  ): Promise<any>|any {
    const model = route.data.model || route.params.model;
    return this._context.getMetadata().then( metadata => {
      const re = new RegExp(`^${model}$`, 'ig');
      const entitySet = metadata.EntityContainer.EntitySet.find( x => {
        return re.test(x.Name);
      });
      if (entitySet) {
          return  Promise.resolve({
            modalTitle: this._translateService.instant(`Settings.Lists.${entitySet.EntityType}.Description`)
          });
      }
      return Promise.resolve(null);
    });
  }
}
