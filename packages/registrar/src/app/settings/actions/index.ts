import { Action } from '@ngrx/store';

export enum SettingsActionTypes {
  LoadFilter = '[Settings] Load Filter',
  AppendFilter = '[Settings] Append Filter'
}

export class LoadFilter implements Action {
  readonly type = SettingsActionTypes.LoadFilter;
  constructor() {}
}

export class AppendFilter implements Action {
  readonly type = SettingsActionTypes.AppendFilter;
  constructor(public readonly filter: string) {}
}

export type SettingsActions = LoadFilter | AppendFilter;

