import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { ListComponent, EnumerationModelFormResolver, EnumerationModalTitleResolver } from './components/list/list.component';
import { SectionsComponent } from './components/sections/sections.component';
import {AdvancedFormModalComponent, AdvancedFormModalData, AdvancedFormItemResolver, AdvancedFormResolver} from '@universis/forms';
import { AdvancedFormModelResolver } from '@universis/forms';
import {AdvancedListComponent} from '@universis/ngx-tables';
import {AdvancedTableConfigurationResolver} from '@universis/ngx-tables';
import {AdvancedFormItemWithLocalesResolver} from '../registrar-shared/advanced-form-router/advanced-form-router.component';
import { ActiveDepartmentIDResolver } from '../registrar-shared/services/activeDepartmentService.service';
import * as DepartmentCourseAreasConfig from './components/list/courseAreas.config.list.json';
import * as DepartmentCourseSectorsConfig from './components/list/courseSectors.config.list.json';
import * as DepartmentExamPeriodsConfig from './components/list/examPeriods.config.json';
import * as DepartmentThesisSubjectsConfig from './components/list/thesisSubjects.config.list.json';

const routes: Routes = [
    {
        path: '',
        component: HomeComponent,
        data: {
            title: 'Settings'
        },
        children: [
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'sections'
            },
            {
                path: 'sections',
                component: SectionsComponent
            },
            {
                path: 'lists/AttachmentTypes',
                component: AdvancedListComponent,
                data: {
                    model: 'AttachmentTypes',
                    list: 'active',
                    description: 'Settings.Lists.AttachmentType.Description',
                    longDescription: 'Settings.Lists.AttachmentType.LongDescription',
                },
                resolve: {
                    tableConfiguration: AdvancedTableConfigurationResolver
                },
                children: [
                    {
                        path: 'add',
                        pathMatch: 'full',
                        component: AdvancedFormModalComponent,
                        outlet: 'modal',
                        data: <AdvancedFormModalData>{
                            description: null,
                            action: 'new',
                            closeOnSubmit: true
                        },
                        resolve: {
                            formConfig: AdvancedFormResolver
                        }
                    },
                    {
                        path: ':id/edit',
                        pathMatch: 'full',
                        component: AdvancedFormModalComponent,
                        outlet: 'modal',
                        data: <AdvancedFormModalData>{
                            action: 'edit',
                            closeOnSubmit: true
                        },
                        resolve: {
                            formConfig: AdvancedFormResolver,
                            data: AdvancedFormItemResolver
                        }
                    }
                ]
            },
            {
                path: 'lists/CourseTypes',
                component: AdvancedListComponent,
                data: {
                    model: 'CourseTypes',
                    list: 'active',
                    description: 'Settings.Lists.CourseTypes.Description',
                    longDescription: 'Settings.Lists.CourseTypes.LongDescription',
                },
                resolve: {
                    tableConfiguration: AdvancedTableConfigurationResolver
                },
                children: [
                    {
                        path: 'add',
                        pathMatch: 'full',
                        component: AdvancedFormModalComponent,
                        outlet: 'modal',
                        data: <AdvancedFormModalData>{
                            description: null,
                            action: 'new',
                            closeOnSubmit: true
                        },
                        resolve: {
                            formConfig: AdvancedFormResolver
                        }
                    },
                    {
                        path: ':id/edit',
                        pathMatch: 'full',
                        component: AdvancedFormModalComponent,
                        outlet: 'modal',
                        data: <AdvancedFormModalData>{
                            action: 'edit',
                            serviceQueryParams: {
                                $expand: 'locales'
                              },
                            closeOnSubmit: true
                        },
                        resolve: {
                            formConfig: AdvancedFormResolver,
                            data: AdvancedFormItemWithLocalesResolver
                        }
                    }
                ]
            },
            {
                path: 'lists/CourseAreas',
                component: AdvancedListComponent,
                data: {
                    model: 'CourseAreas',
                    list: 'active',
                    description: 'Settings.Lists.CourseAreas.Description',
                    longDescription: 'Settings.Lists.CourseAreas.LongDescription',
                    tableConfiguration: DepartmentCourseAreasConfig
                },
                children: [
                    {
                        path: 'add',
                        pathMatch: 'full',
                        component: AdvancedFormModalComponent,
                        outlet: 'modal',
                        data: <AdvancedFormModalData>{
                            model: 'CourseAreas',
                            action: 'new',
                            closeOnSubmit: true
                        },
                        resolve: {
                            formConfig: AdvancedFormResolver,
                            department: ActiveDepartmentIDResolver
                        }
                    },
                    {
                        path: ':id/edit',
                        pathMatch: 'full',
                        component: AdvancedFormModalComponent,
                        outlet: 'modal',
                        data: <AdvancedFormModalData>{
                            model: 'CourseAreas',
                            action: 'edit',
                            closeOnSubmit: true,
                        },
                        resolve: {
                            formConfig: AdvancedFormResolver,
                            data: AdvancedFormItemResolver
                        }
                    }
                ]
            },
            {
                path: 'lists/CourseSectors',
                component: AdvancedListComponent,
                data: {
                    model: 'CourseSectors',
                    list: 'active',
                    description: 'Settings.Lists.CourseSectors.Description',
                    longDescription: 'Settings.Lists.CourseSectors.LongDescription',
                    tableConfiguration: DepartmentCourseSectorsConfig
                },
                children: [
                    {
                        path: 'add',
                        pathMatch: 'full',
                        component: AdvancedFormModalComponent,
                        outlet: 'modal',
                        data: <AdvancedFormModalData>{
                            model: 'CourseSectors',
                            action: 'new',
                            closeOnSubmit: true
                        },
                        resolve: {
                            formConfig: AdvancedFormResolver,
                            department: ActiveDepartmentIDResolver
                        }
                    },
                    {
                        path: ':id/edit',
                        pathMatch: 'full',
                        component: AdvancedFormModalComponent,
                        outlet: 'modal',
                        data: <AdvancedFormModalData>{
                            model: 'CourseSectors',
                            action: 'edit',
                            closeOnSubmit: true,
                        },
                        resolve: {
                            formConfig: AdvancedFormResolver,
                            data: AdvancedFormItemResolver
                        }
                    }
                ]
            },
            {
                path: 'lists/ExamPeriods',
                component: AdvancedListComponent,
                data: {
                    model: 'ExamPeriods',
                    list: 'active',
                    description: 'Settings.Lists.ExamPeriods.Description',
                    longDescription: 'Settings.Lists.ExamPeriods.LongDescription',
                    tableConfiguration: DepartmentExamPeriodsConfig
                },
                children: [
                    {
                        path: 'add',
                        pathMatch: 'full',
                        component: AdvancedFormModalComponent,
                        outlet: 'modal',
                        data: <AdvancedFormModalData>{
                            model: 'ExamPeriods',
                            action: 'new',
                            closeOnSubmit: true,
                            data: {
                              '$state': 1
                            }
                        },
                        resolve: {
                            formConfig: AdvancedFormResolver,
                            department: ActiveDepartmentIDResolver
                        }
                    },
                    {
                        path: ':id/edit',
                        pathMatch: 'full',
                        component: AdvancedFormModalComponent,
                        outlet: 'modal',
                        data: <AdvancedFormModalData>{
                            model: 'ExamPeriods',
                            action: 'edit',
                            closeOnSubmit: true,
                            serviceQueryParams: {
                              $expand: 'locales, academicPeriods($select=id,name)'
                            }
                        },
                        resolve: {
                            formConfig: AdvancedFormResolver,
                            data: AdvancedFormItemWithLocalesResolver
                        }
                    }
                ]
            },
            {
                path: 'lists/ThesisSubjects',
                component: AdvancedListComponent,
                data: {
                    model: 'ThesisSubjects',
                    list: 'active',
                    description: 'Settings.Lists.ThesisSubjects.Description',
                    longDescription: 'Settings.Lists.ThesisSubjects.LongDescription',
                    tableConfiguration: DepartmentThesisSubjectsConfig
                },
                children: [
                    {
                        path: 'add',
                        pathMatch: 'full',
                        component: AdvancedFormModalComponent,
                        outlet: 'modal',
                        data: <AdvancedFormModalData>{
                            model: 'ThesisSubjects',
                            action: 'new',
                            closeOnSubmit: true
                        },
                        resolve: {
                            formConfig: AdvancedFormResolver,
                            department: ActiveDepartmentIDResolver
                        }
                    },
                    {
                        path: ':id/edit',
                        pathMatch: 'full',
                        component: AdvancedFormModalComponent,
                        outlet: 'modal',
                        data: <AdvancedFormModalData>{
                            model: 'ThesisSubjects',
                            action: 'edit',
                            closeOnSubmit: true,
                        },
                        resolve: {
                            formConfig: AdvancedFormResolver,
                            data: AdvancedFormItemResolver
                        }
                    }
                ]
            },
            {
                path: 'lists/:model',
                component: ListComponent,
                children: [
                    {
                        path: 'add',
                        pathMatch: 'full',
                        component: AdvancedFormModalComponent,
                        outlet: 'modal',
                        data: <AdvancedFormModalData>{
                            action: 'new',
                            closeOnSubmit: true
                        },
                        resolve: {
                            model: AdvancedFormModelResolver,
                            formConfig: EnumerationModelFormResolver,
                            modalOptions: EnumerationModalTitleResolver
                        }
                    },
                    {
                        path: ':id/edit',
                        pathMatch: 'full',
                        component: AdvancedFormModalComponent,
                        outlet: 'modal',
                        data: <AdvancedFormModalData>{
                            action: 'edit',
                            closeOnSubmit: true
                        },
                        resolve: {
                            model: AdvancedFormModelResolver,
                            data: AdvancedFormItemResolver,
                            formConfig: EnumerationModelFormResolver,
                            modalOptions: EnumerationModalTitleResolver
                        }
                    }
                ]
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    declarations: []
})
export class SettingsRoutingModule {
}
