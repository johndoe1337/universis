import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';

@Component({
  selector: 'app-enrollment-event-overview',
  templateUrl: './event-overview.component.html'
})
export class EnrollmentEventOverviewComponent implements OnInit {

  public enrollmentEvent: any;

  constructor(private _activatedRoute: ActivatedRoute, private _context: AngularDataContext) { }

  async ngOnInit() {

    const enrollmentEvent = await this._context.model('StudyProgramEnrollmentEvents')
      .where('id').equal(this._activatedRoute.snapshot.params.id)
      .expand('attachmentTypes($expand=attachmentType),studyProgram,inscriptionModes')
      .getItem();

    this.enrollmentEvent = enrollmentEvent;
  }

}
