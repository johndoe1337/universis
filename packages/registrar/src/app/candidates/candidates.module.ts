import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CandidatesSharedModule } from './candidates.shared';
import { TranslateModule } from '@ngx-translate/core';
import { TablesModule } from '@universis/ngx-tables';
import { SharedModule } from '@universis/common';
import { FormsModule } from '@angular/forms';
import { MostModule } from '@themost/angular';
import { RegistrarSharedModule } from '../registrar-shared/registrar-shared.module';
import { RouterModalModule } from '@universis/common/routing';
import {RouterModule} from '@angular/router';
import {StudyProgramsSharedModule} from '../study-programs/study-programs.shared';
import {StudentsSharedModule} from '../students/students.shared';
import {BsDatepickerModule, TabsModule, TimepickerModule} from 'ngx-bootstrap';
import {ElementsModule} from '../elements/elements.module';
import {ChartsModule} from 'ng2-charts';
import {MessagesSharedModule} from '../messages/messages.shared';
import {SendMessageToStudentComponent} from '../messages/components/send-message-to-student/send-message-to-student.component';
import {CandidatesTableComponent} from './components/candidates/candidates-table.component';
import {CandidatesRoutingModule} from './candidates.routing';
import {CandidatesHomeComponent} from './components/candidates-home/candidates-home.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule,
    CandidatesSharedModule,
    CandidatesRoutingModule,
    TablesModule,
    SharedModule,
    FormsModule,
    MostModule,
    RegistrarSharedModule,
    RouterModalModule,
    StudyProgramsSharedModule,
    StudentsSharedModule,
    TabsModule.forRoot(),
    BsDatepickerModule.forRoot(),
    TimepickerModule.forRoot(),
    ElementsModule,
    ChartsModule,
    MessagesSharedModule
  ],
  declarations: [
    CandidatesTableComponent,
    CandidatesHomeComponent
  ],
  entryComponents: [
    SendMessageToStudentComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CandidatesModule { }
