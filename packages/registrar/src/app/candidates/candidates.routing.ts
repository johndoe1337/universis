import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {CandidatesTableComponent} from './components/candidates/candidates-table.component';
import {
  CandidatesTableConfigurationResolver,
  CandidatesTableSearchResolver
} from './components/candidates/candidates-table-config.resolver';
import {ActiveDepartmentResolver, CurrentAcademicYearResolver} from '../registrar-shared/services/activeDepartmentService.service';
import {CandidatesHomeComponent} from './components/candidates-home/candidates-home.component';
import {AdvancedFormItemResolver, AdvancedFormModalComponent, AdvancedFormModalData, AdvancedFormResolver} from '@universis/forms';

const routes: Routes = [
  {
    path: '',
    component: CandidatesHomeComponent,
    data: {
      title: 'Candidate students'
    },
    children: [
      {
        path: 'list',
        pathMatch: 'full',
        redirectTo: 'list/current'
      },
      {
        path: 'list/:list',
        component: CandidatesTableComponent,
        data: {
          title: 'Candidate students list'
        },
        resolve: {
          currentYear: CurrentAcademicYearResolver,
          tableConfiguration: CandidatesTableConfigurationResolver,
          searchConfiguration: CandidatesTableSearchResolver,
          department: ActiveDepartmentResolver
        },
        children: [
          {
            path: ':id/edit',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              action: 'edit',
              closeOnSubmit: true,
              serviceQueryParams: {
                $expand: 'department,studyProgram,inscriptionMode,person($expand=gender,nationality)'
              }
            },
            resolve: {
              formConfig: AdvancedFormResolver,
              data: AdvancedFormItemResolver
            }
          }]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: []
})
export class CandidatesRoutingModule {
}
