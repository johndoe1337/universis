import {TableConfiguration} from '@universis/ngx-tables';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';

export class GradeSubmissionsTableConfigurationResolver implements Resolve<TableConfiguration> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
        return import(`./grade-submissions-table.config.${route.params.list}.json`)
            .catch( err => {
           return  import(`./grade-submissions-table.config.list.json`);
        });
    }
}

export class GradeSubmissionsTableSearchResolver implements Resolve<TableConfiguration> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
        return import(`./grade-submissions-table.search.${route.params.list}.json`)
            .catch( err => {
                return  import(`./grade-submissions-table.search.list.json`);
            });
    }
}

export class GradeSubmissionsDefaultTableConfigurationResolver implements Resolve<any> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return import(`./grade-submissions-table.config.list.json`);
    }
}
