import {Component, OnDestroy, OnInit} from '@angular/core';
import * as COURSES_LIST_CONFIG from '../courses-table/courses-table.config.list.json';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs';

declare interface TablePathConfiguration {
  name: string;
  alternateName: string;
  show?: boolean;
}

@Component({
  selector: 'app-courses-home',
  templateUrl: './courses-home.component.html'
})
export class CoursesHomeComponent implements OnInit, OnDestroy {

  public paths: Array<TablePathConfiguration> = [];
  public activePaths: Array<TablePathConfiguration> = [];
  public maxActiveTabs = 3;
  private paramSubscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.paths = (<any>COURSES_LIST_CONFIG).paths;
    this.activePaths = this.paths.filter( x => {
      return x.show === true;
    }).slice(0);
    this.paramSubscription = this._activatedRoute.firstChild.params.subscribe( params => {
      const matchPath = new RegExp('^' + params.list  + '$', 'ig');
      const findItem = this.paths.find( x => {
        return matchPath.test(x.alternateName);
      });
      if (findItem) {
        this.showTab(findItem);
      }
    });
  }


  showTab(item: any) {
    // find if path exists in active paths
    const findIndex = this.activePaths.findIndex( x => {
      return x.alternateName === item.alternateName;
    });
    if (findIndex < 0) {
      if (this.activePaths.length >= this.maxActiveTabs) {
        // remove item at index 0
        this.activePaths.splice(0, 1);
      }
      // add active path
      this.activePaths.push(Object.assign({}, item));
    }
  }

  ngOnDestroy(): void {
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
  }
}
