import {Component, Injectable, OnDestroy, OnInit, ViewChild} from '@angular/core';
import * as CLASSES_LIST_CONFIG from './courses-preview-classes.config.json';
import {
  AdvancedTableComponent,
  AdvancedTableConfiguration
} from '@universis/ngx-tables';
import {ActivatedRoute, ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { ActivatedTableService } from '@universis/ngx-tables';
import {Args} from '@themost/client';
import {AdvancedFormParentItemResolver} from '@universis/forms';
import {Subscription} from 'rxjs';

@Injectable({ providedIn: 'root' })
export class CourseTitleResolver implements Resolve<any> {
  constructor(private _context: AngularDataContext  ) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<any>|any {
  return  new AdvancedFormParentItemResolver(this._context).resolve(route, state).then(result => {
      return result.name;
    });
  }
}


@Component({
  selector: 'app-courses-preview-classes',
  templateUrl: './courses-preview-classes.component.html',
})
export class CoursesPreviewClassesComponent implements OnInit, OnDestroy {
  public readonly config = CLASSES_LIST_CONFIG;
  @ViewChild('classes') classes: AdvancedTableComponent;
  private subscription: Subscription;
  private fragmentSubscription: Subscription;
  public course;

  constructor(private _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext,
    private _activatedTable: ActivatedTableService) { }

  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.course = params.id;
      this._activatedTable.activeTable = this.classes;
      this.classes.query = this._context.model('courseClasses')
        .where('course').equal(params.id)
        .expand('course')
        .orderByDescending('year')
        .thenBy('period')
        .prepare();

      this.classes.config = AdvancedTableConfiguration.cast(CLASSES_LIST_CONFIG);
      this.classes.fetch();

      this.fragmentSubscription = this._activatedRoute.fragment.subscribe(fragment => {
        if (fragment && fragment === 'reload') {
          this.classes.fetch(true);
        }
      });
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
  }
}
