import {Component, EventEmitter, Input, OnInit, ViewChild, OnDestroy, Output} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import * as STUDENTS_LIST_CONFIG from './courses-students.config.list.json';
import {Observable, Subscription} from 'rxjs';
import {AdvancedSearchFormComponent} from '@universis/ngx-tables';
import {ActivatedTableService} from '@universis/ngx-tables';
import {
  AdvancedTableComponent,
  AdvancedTableConfiguration,
  AdvancedTableDataResult
} from '@universis/ngx-tables';
import {ErrorService, LoadingService, ModalService} from '@universis/common';
import {ClientDataQueryable} from '@themost/client';
import {AdvancedRowActionComponent} from '@universis/ngx-tables';


@Component({
  selector: 'app-courses-students',
  templateUrl: './courses-students.component.html'
})
export class CoursesStudentsComponent implements OnInit, OnDestroy {

  public readonly config: AdvancedTableConfiguration = <AdvancedTableConfiguration>STUDENTS_LIST_CONFIG;
  @ViewChild('students') students: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  public recordsTotal: any;
  private dataSubscription: Subscription;
  public courseId: any;
  private fragmentSubscription: Subscription;
  @Input() reload: EventEmitter<any> = new EventEmitter<any>();
  private subscription: Subscription;
  private selectedItems = [];
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();

  constructor(private _activatedRoute: ActivatedRoute,
              private _activatedTable: ActivatedTableService,
              private _context: AngularDataContext,
              private _loadingService: LoadingService,
              private _modalService: ModalService,
              private _errorService: ErrorService
  ) {
  }

  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.courseId = params.id;
      this._activatedTable.activeTable = this.students;
      this.students.query = this._context.model('StudentCourses')
        .asQueryable()
        .where('course').equal(params.id)
        .prepare();
      this.students.config = AdvancedTableConfiguration.cast(STUDENTS_LIST_CONFIG);
      this.students.fetch();
      // declare model for advance search filter criteria
      this.students.config.model = 'StudentCourses';
      this.fragmentSubscription = this._activatedRoute.fragment.subscribe(fragment => {
        if (fragment && fragment === 'reload') {
          this.students.fetch(true);
        }
      });

      this.dataSubscription = this._activatedRoute.data.subscribe(data => {
        if (data.tableConfiguration) {
          this.students.config = data.tableConfiguration;
          this.students.ngOnInit();
        }
        if (data.searchConfiguration) {
          this.search.form =  Object.assign(data.searchConfiguration,
            { course: this.courseId,
              department: this._activatedRoute.snapshot.data.department});
          this.search.ngOnInit();
        }
      });
    });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }

  /***
   * Change student courses attributes
   */
  async changeCourseAction() {
    try {
      this._loadingService.showLoading();
      let items = await this.getSelectedItems();
      items =  items.filter( (item) => {
        return item.status === 'active';
      });
      this.selectedItems = items;
      // get only active items
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          formTemplate: this.selectedItems && this.selectedItems.length > 0 ? 'StudentCourses/changeAttributes' : null,
          modalTitle: 'Courses.ChangeCourseAttributesAction.Title',
          description: 'Courses.ChangeCourseAttributesAction.Description',
          refresh: this.refreshAction,
          execute: this.executeChangeCourseAttributes()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  executeChangeCourseAttributes() {
    return new Observable((observer) => {
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0,
        additionalMessage: ''
      };
      // get values from modal component
      const component = this._modalService.modalRef.content as AdvancedRowActionComponent;
      const data = component.formComponent.form.formio.data;
      // filter empty values
       Object.keys(data).forEach(e => {
         if (data[e] == null || data[e] === '' ||  (typeof data[e] === 'object' && data[e].id == null)) {
           delete data[e];
         }
       });
      if (Object.keys(data).length === 0) {
        this.selectedItems = [];
        result.errors = result.total;
        return observer.next(result);
      }
      this.refreshAction.emit({
        progress: 1
      });
      const items = {
        students: this.selectedItems,
        attributes: data
      };
      this._context.model(`Courses/${this.courseId}/updateStudents`).save(items).then(() => {
        result.success = result.total;
        observer.next(result);
      }).catch((err) => {
        this.selectedItems = [];
        result.errors = result.total;
        result.additionalMessage = err.message;
        return observer.next(result);
      });
    });
  }

  async getSelectedItems() {
    let items = [];
    if (this.students && this.students.lastQuery) {
      const lastQuery: ClientDataQueryable = this.students.lastQuery;
      // search for document attributes (if table has published column)

      if (lastQuery != null) {
        if (this.students.smartSelect) {
          // get items
          const selectArguments = ['student/id as id', 'student/studentStatus/alternateName as status'];

          // query items
          const queryItems = await lastQuery.select.apply(lastQuery, selectArguments)
            .take(-1)
            .skip(0)
            .getItems();
          if (this.students.unselected && this.students.unselected.length) {
            // remove items that have been unselected by the user
            items = queryItems.filter(item => {
              return this.students.unselected.findIndex((x) => {
                return x.id === item.id;
              }) < 0;
            });
          } else {
            items = queryItems;
          }
        } else {
          // get selected items only
          items = this.students.selected.map((item) => {
            return {
              id: item.id,
              status: item.studentStatus
            };
          });
        }
      }
    }
    return items;
  }

}
