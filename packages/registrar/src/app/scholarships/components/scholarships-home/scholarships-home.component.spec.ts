import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScholarshipsHomeComponent } from './scholarships-home.component';

describe('ScholarshipsHomeComponent', () => {
  let component: ScholarshipsHomeComponent;
  let fixture: ComponentFixture<ScholarshipsHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScholarshipsHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScholarshipsHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
