import { Component, Directive, EventEmitter, Input, OnDestroy, OnInit } from '@angular/core';

import { AdvancedTableModalBaseComponent, AdvancedTableModalBaseTemplate } from '@universis/ngx-tables';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { Subscription } from 'rxjs';
import { ErrorService, ToastService } from '@universis/common';
import { TranslateService } from '@ngx-translate/core';
import {AdvancedFilterValueProvider} from '@universis/ngx-tables';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-scholarships-add-student',
  template: AdvancedTableModalBaseTemplate
})
export class ScholarshipsAddStudentComponent extends AdvancedTableModalBaseComponent {

  @Input() scholarship: any;

  constructor(_router: Router, private _activatedRoute: ActivatedRoute,
              protected _context: AngularDataContext,
              private _errorService: ErrorService,
              private _toastService: ToastService,
              private _translateService: TranslateService,
              protected advancedFilterValueProvider: AdvancedFilterValueProvider,
              protected datePipe: DatePipe) {
    super(_router, _activatedRoute, _context, advancedFilterValueProvider, datePipe);
    // set default title
    this.modalTitle = 'Scholarships.AddStudent';
  }

  hasInputs(): Array<string> {
    return ['scholarship'];
  }

  ok(): Promise<any> {
    // get selected items
    const selected = this.advancedTable.selected;
    let items = [];
    if (selected && selected.length > 0) {
      // try to add classes
      items = selected.map(student => {
        return {
          student: student,
          scholarship: this.scholarship
        };
      });
      return this._context.model('studentScholarships')
        .save(items)
        .then(result => {
          // add toast message
          this._toastService.show(
            this._translateService.instant('Scholarships.AddStudentsMessage.title'),
            this._translateService.instant((items.length === 1 ?
              'Scholarships.AddStudentsMessage.one' : 'Scholarships.AddStudentsMessage.many')
              , { value: items.length })
          );
          return this.close({
            fragment: 'reload',
            skipLocationChange: true
          });
        }).catch(err => {
          this._errorService.showError(err, {
            continueLink: '.'
          });
        });
    }
    return this.close();
  }
}
