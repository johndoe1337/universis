import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { environment } from '../../environments/environment';
import { SettingsService } from '../settings-shared/services/settings.service';
import { SettingsSharedModule } from '../settings-shared/settings-shared.module';
import { DegreeTemplateService } from './services/degree-template.service';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    SettingsSharedModule
  ],
  declarations: []
})
export class DegreeTemplatesSharedModule {
    static forRoot(): ModuleWithProviders<DegreeTemplatesSharedModule> {
        return {
          ngModule: DegreeTemplatesSharedModule,
          providers: [
          ],
        };
      }
    constructor(private translateService: TranslateService, private settings: SettingsService) {
          const sources = environment.languages.map((language: string) => {
            return import(`./i18n/degree-templates.${language}.json`).then((translations) => {
              this.translateService.setTranslation(language, translations, true);
              return Promise.resolve();
            }).catch((err: Error) => {
              console.error('DegreeTemplatesSharedModule', `An error occurred while loading translations for language ${language}`);
              console.error(err);
              return Promise.resolve();
            });
          });
          Promise.all(sources).then(() => {
            this.translateService.onLangChange.subscribe(() => {
              const Settings: any = this.translateService.instant('Settings');
              this.settings.addSection({
                name: 'DegreeTemplates',
                description: Settings.Lists.DegreeTemplates.Description,
                longDescription: Settings.Lists.DegreeTemplates.LongDescription,
                category: 'Departments',
                url: '/departments/configuration/degree-templates/list',
                dependencies: [
                  'eDiplomasService'
                ]
              });
            });
          });
    }
}
