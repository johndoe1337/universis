import { Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { AdvancedSearchFormComponent,
  AdvancedTableComponent, AdvancedTableConfiguration, AdvancedTableDataResult } from '@universis/ngx-tables';
import * as DegreeTemplatesTableConfiguration from './list.json';
import * as DegreeTemplatesSearchConfiguration from './search.json';
import { AppEventService, DIALOG_BUTTONS } from '@universis/common';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { ModalService, ToastService, ErrorService, LoadingService } from '@universis/common';
import { AngularDataContext } from '@themost/angular';
@Component({
  selector: 'app-degree-templates-list',
  templateUrl: './list.component.html',
  styles: [
    `
    /* app-degree-templates-list app-advanced-table-search .s--list-navbar {
        background-color: inherit;
      } */
    `
  ],
  encapsulation: ViewEncapsulation.None
})
export class ListComponent implements OnInit, OnDestroy {

  @ViewChild('table') table: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  recordsTotal: number;
  addSubscription: Subscription;
  constructor(private appEvent: AppEventService,
    private context: AngularDataContext,
    private modalService: ModalService,
    private errorService: ErrorService,
    private toastService: ToastService,
    private translateService: TranslateService,
    private loadingService: LoadingService) {
    this.addSubscription = this.appEvent.add.subscribe((event: any) => {
      if (event && event.model === 'DegreeTemplates') {
        this.table.fetch();
      }
    });
  }
  ngOnDestroy(): void {
    if (this.addSubscription) {
      this.addSubscription.unsubscribe();
    }
  }

  ngOnInit() {
    this.table.config = AdvancedTableConfiguration.cast(DegreeTemplatesTableConfiguration);
    this.search.form = DegreeTemplatesSearchConfiguration;
  }

  /**
   * Data load event handler of advanced table component
   * @param {AdvancedTableDataResult} data
   */
   onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  remove() {
    if (this.table && this.table.selected && this.table.selected.length) {
      // get items to remove
      const items = this.table.selected;
      return this.modalService.showWarningDialog(
        this.translateService.instant('Tables.RemoveItemsTitle'),
        this.translateService.instant('Tables.RemoveItemsMessage'),
        DIALOG_BUTTONS.OkCancel).then(result => {
          if (result === 'ok') {
            this.loadingService.showLoading();
            this.context.model(this.table.config.model).remove(items).then(() => {
              this.toastService.show(
                this.translateService.instant('Tables.RemoveItemMessage.title'),
                this.translateService.instant((items.length === 1 ?
                  'Tables.RemoveItemMessage.one' : 'Tables.RemoveItemMessage.many')
                  , { value: items.length })
              );
              this.table.fetch(true);
              this.loadingService.hideLoading();
            }).catch(err => {
              this.loadingService.hideLoading();
              this.errorService.showError(err, {
                continueLink: '.'
              });
            });
          }
        });
    }
  }

}
