import { Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild, ViewEncapsulation } from '@angular/core';
import { AdvancedRowActionComponent, AdvancedSearchFormComponent,
  AdvancedTableComponent, AdvancedTableConfiguration, AdvancedTableDataResult } from '@universis/ngx-tables';
import * as SpecializationTableConfiguration from './list.json';
import * as SpecializationSearchConfiguration from './search.json';
import { AppEventService, DIALOG_BUTTONS, ErrorService, LoadingService, ModalService } from '@universis/common';
import { Observable, Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';
import { ActiveDepartmentService } from '../../../registrar-shared/services/activeDepartmentService.service';
@Component({
  selector: 'app-degree-template-specialization-list',
  templateUrl: './list.component.html',
  encapsulation: ViewEncapsulation.None
})
export class SpecializationListComponent implements OnInit, OnDestroy {

  @ViewChild('table') table: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();
  private selectedItems: any[];

  recordsTotal: number;
  addSubscription: Subscription;
  constructor(private appEvent: AppEventService,
    private modalService: ModalService,
    private errorService: ErrorService,
    private translateService: TranslateService,
    private context: AngularDataContext,
    private activeDepartment: ActiveDepartmentService,
    private loadingService: LoadingService) {
    this.addSubscription = this.appEvent.add.subscribe((event: any) => {
      if (event && event.model === 'DegreeTemplates') {
        this.table.fetch();
      }
    });
  }
  ngOnDestroy(): void {
    if (this.addSubscription) {
      this.addSubscription.unsubscribe();
    }
  }

  ngOnInit() {
    this.table.config = AdvancedTableConfiguration.cast(SpecializationTableConfiguration);
    this.activeDepartment.getActiveDepartment().then((activeDepartment: { id?: string }) => {
      this.search.form = Object.assign({
        department: activeDepartment && activeDepartment.id
      }, SpecializationSearchConfiguration);
      this.search.ngOnInit();
    });
  }

  /**
   * Data load event handler of advanced table component
   * @param {AdvancedTableDataResult} data
   */
   onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  setDegreeTemplateAction() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      const component = this.modalService.modalRef.content as AdvancedRowActionComponent;
      const data = component.formComponent.form.formio.data;
      if (data.degreeTemplate == null) {
        this.selectedItems = [];
        result.errors = result.total;
        return observer.next(result);
      }
      this.refreshAction.emit({
        progress: 1
      });
      const selectedItems = this.selectedItems.map((item) => {
        return {
          id: item.id
        }
      });
      (async () => {
        let degreeTemplate = data.degreeTemplate;
        if (degreeTemplate === '') {
          degreeTemplate = null;
        }
        for (let index = 0; index < selectedItems.length; index++) {
          try {
            const item = selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            await this.context.model('StudyProgramSpecialties').save({
              id: item.id,
              degreeTemplate: degreeTemplate
            });
            try {
              await this.table.fetchOne({
                id: item.id
              });
            } catch (err) {
              //
            }
          } catch (err) {
            // log error
            console.error(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  setDegreeTemplate() {
    this.activeDepartment.getActiveDepartment().then((department) => {
      this.selectedItems = this.table.selected;
        const data = {
          department: department && department.alternativeCode
        };
        this.modalService.openModalComponent(AdvancedRowActionComponent, {
          class: 'modal-lg',
          keyboard: false,
          ignoreBackdropClick: true,
          initialState: {
            items: this.selectedItems, // set items
            data: data,
            modalTitle: this.translateService.instant('DegreeTemplates.SetDegreeTemplateAction'), // set title
            description: this.translateService.instant('DegreeTemplates.SetDegreeTemplateDescription'),
            okButtonText: this.translateService.instant('Tables.Apply'),
            okButtonClass: 'btn btn-success',
            formTemplate: 'StudyProgramSpecialties/setDegreeTemplate',
            progressType: 'success',
            refresh: this.refreshAction, // refresh action event
            execute: this.setDegreeTemplateAction()
          }
        });
    }).catch((err) => {
      console.error(err);
        this.errorService.showError(err, {
          continueLink: '.'
        });
    });
  }

  clearDegreeTemplate() {
    if (this.table && this.table.selected && this.table.selected.length) {
      // get items to remove
      const items = this.table.selected.map((item) => {
        return {
          id: item.id,
          degreeTemplate: null
        }
      });
      return this.modalService.showWarningDialog(
        this.translateService.instant('DegreeTemplates.ClearDegreeTemplateTitle'),
        this.translateService.instant('DegreeTemplates.ClearDegreeTemplateMessage'),
        DIALOG_BUTTONS.OkCancel).then(result => {
          if (result === 'ok') {
            this.loadingService.showLoading();
            this.context.model('StudyProgramSpecialties').save(items).then(() => {
              this.table.fetch(true);
              this.loadingService.hideLoading();
            }).catch(err => {
              this.loadingService.hideLoading();
              this.errorService.showError(err, {
                continueLink: '.'
              });
            });
          }
        });
    }
  }


}
