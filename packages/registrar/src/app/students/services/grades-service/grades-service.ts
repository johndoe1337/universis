import { Injectable } from "@angular/core";
import { round } from '@universis/common';

export declare interface CourseGradeBase {
  // defines the structure of the course (simple, complex, coursePart etc)
  // can either be an object or a number
  courseStructureType: any;
  // the number of units the course provides
  units: number;
  // defines if the grade is calculated in the degree grade
  calculateGrade: boolean;
  // defines if the units the course provides are counted in the total units
  calculateUnits: boolean;
  // defines if the course is passed
  isPassed: boolean;
  // the number of ects the course provides
  ects: number;
  // the coefficient of the course toward degree-getting
  coefficient?: number;
  // the grade of the course
  grade: number;
}

export declare interface GradeAverageResult {
  // the total number of courses
  courses: number;
  // the total number of passed courses
  passed: number;
  // the sum of grades used for calculating average
  grades: number;
  // the sum of coefficients used for calculating a weighted average grade
  coefficients?: number;
  // the calculated average grade
  average: number;
  // the total number of ects of passed courses
  ects: number;
  // the total number of units of passed courses
  units: number;
}


@Injectable()
export class GradesService {

  /**
 *
 * @param courses
 * @returnType: GradesAverageResult
 */
  getGradesSimpleAverage(courses: Array<CourseGradeBase>): GradeAverageResult {
    const average: GradeAverageResult = {
      courses: 0,
      passed: 0,
      grades: 0,
      coefficients: 0,
      average: 0,
      ects: 0,
      units: 0
    };
    if (!Array.isArray(courses)) {
      throw new Error('Courses must be an Array');
    }
    average.courses = courses.length;
    if (average.courses > 0) {
      // removes course parts
      const coursesArray = courses.filter(studentCourse => {
        const courseStructureType = studentCourse.courseStructureType &&
          ((typeof studentCourse.courseStructureType === 'number' && studentCourse.courseStructureType)
            || (typeof studentCourse.courseStructureType.id === 'number' && studentCourse.courseStructureType.id));
        return courseStructureType === 1 || courseStructureType === 4;
      });
      // we should add this statement in case the user only passed course parts in an exam period but hasn't
      // passed the other course parts so parent course grade will be null
      if (coursesArray.length > 0) {
        // gets the passed grades whose grade counts to the grade of the degree
        const courseUnits = coursesArray.filter(course => {
          return course.isPassed && course.calculateUnits;
        });
        if (courseUnits.length > 0) {
          average.units = courseUnits.map(studentCourse => {
            return studentCourse.units;
          }).reduce((sum, units) => sum + units);
          average.ects = courseUnits.map(course => {
            return course.ects;
          }).reduce((sum, ects) => sum + ects);

          average.passed = courseUnits.length;
        }
        const passedCourses = coursesArray.filter(course => {
          return course.calculateGrade && course.isPassed;
        });
        if (passedCourses.length > 0) {
          average.grades = Number(round((passedCourses.map(course => course.grade).reduce((sum, courseGrade) => sum + courseGrade)), 4));
          average.average = Number(round(average.grades / passedCourses.length, 4));
        }
      }
    }
    return average;
  }


  /**
   * This function takes a course array as a parameter and returns an
   * object which contains the total courses in the array, the passed grades,
   * the sumOfGrades, the sum of the coefficients, the average and the sum
   * of ECTS and Units that the passed courses provide to the student.
   * @param courses
   * @returnType GradeAverageResult
   */
  getGradesWeightedAverage(courses: Array<CourseGradeBase>): GradeAverageResult {
    const average: GradeAverageResult = {
      courses: 0,
      passed: 0,
      grades: 0,
      coefficients: 0,
      average: 0,
      ects: 0,
      units: 0
    };
    if (!Array.isArray(courses)) {
      throw new Error('Courses must be an array');
    }
    if (courses.length === 0) {
      return average;
    } else {
      // removes course parts
      const filteredCourses = courses.filter(studentCourse => {
        const courseStructureType = studentCourse.courseStructureType &&
          ((typeof studentCourse.courseStructureType === 'number' && studentCourse.courseStructureType)
            || (typeof studentCourse.courseStructureType.id === 'number' && studentCourse.courseStructureType.id));
        return courseStructureType === 1 || courseStructureType === 4;
      });
      // we should add this statement in case the user only passed course parts in an exam period but hasn't
      // passed the other course parts so parent course grade will be null
      if (filteredCourses.length > 0) {
        average.courses = filteredCourses.length;
        // We are filtering the courses that count to getting a degree
        const coursesUnits = filteredCourses.filter(studentCourse => {
          return studentCourse.calculateUnits && studentCourse.isPassed;
        });
        if (coursesUnits.length > 0) {
          average.units = coursesUnits.map(studentCourse => {
            return studentCourse.units;
          }).reduce((sum, units) => sum + units);
          average.passed = coursesUnits.length;
          average.ects = coursesUnits.map(course => {
            return course.ects;
          }).reduce((sum, ects) => sum + ects);
        }
        // We are filtering the passed courses that count to the grade of the degree
        const courseArray = filteredCourses.filter(course => {
          return course.calculateGrade &&
            course.coefficient &&
            course.coefficient > 0 &&
            course.isPassed;
        });
        if (courseArray.length > 0) {
          average.coefficients = courseArray.map(course => course.coefficient).reduce((sum, course) => sum + course);
          if (average.coefficients > 0) {
            average.grades = Number(round(courseArray.map(course => course.grade * course.coefficient)
              .reduce((sum, grade) => sum + grade), 4));
            average.average = Number(round(average.grades / average.coefficients, 4));
          } else {
            return average;
          }
        }
      }
    }
    return average;
  }
}