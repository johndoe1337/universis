// tslint:disable:max-line-length
import {CUSTOM_ELEMENTS_SCHEMA, NgModule, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {environment} from '../../environments/environment';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {StudentsOverviewCoursesComponent} from './components/students-dashboard/students-overview/students-overview-courses/students-overview-courses.component';
import {StudentsOverviewLastRegistrationComponent} from './components/students-dashboard/students-overview/students-overview-lastregistration/students-overview-last-registration.component';
import {StudentsOverviewCurrentgradesComponent} from './components/students-dashboard/students-overview/students-overview-currentgrades/students-overview-currentgrades.component';
import {StudentsOverviewRequestsComponent} from './components/students-dashboard/students-overview/students-overview-requests/students-overview-requests.component';
import {StudentsOverviewInternshipsComponent} from './components/students-dashboard/students-overview/students-overview-internships/students-overview-internships.component';
import {TooltipModule} from 'ngx-bootstrap';
import {StudentsOverviewScholarshipsComponent} from './components/students-dashboard/students-overview/students-overview-scholarships/students-overview-scholarships.component';
import {StudentsOverviewThesesComponent} from './components/students-dashboard/students-overview/students-overview-theses/students-overview-theses.component';
import { StudentsOverviewProfileComponent } from './components/students-dashboard/students-overview/students-overview-profile/students-overview-profile.component';
import {FormsModule} from '@angular/forms';
import {StudentsPreviewFormComponent} from './components/students-dashboard/students-general/students-preview-form.component';
import { StudentsMessagesPreviewFormComponent } from './components/students-dashboard/students-messages-preview-form/students-messages-preview-form.component';
import { StudentsStatsComponent } from './components/students-dashboard/students-courses/students-stats/students-stats.component';
import { MessagesSharedModule } from '../messages/messages.shared';
import { StudentsOverviewGraduationComponent } from './components/students-dashboard/students-overview/students-overview-profile/students-overview-graduation.component';
import { StudentsOverviewActiveComponent } from './components/students-dashboard/students-overview/students-overview-profile/students-overview-active.component';
import { StudentsOverviewRemovalComponent } from './components/students-dashboard/students-overview/students-overview-profile/students-overview-removal.component';
import {
  StudentDefaultTableConfigurationResolver, StudentTableConfigurationResolver,
  StudentTableSearchResolver
} from './components/students-table/student-table-config.resolver';
import * as STUDENTS_LIST from './components/students-table/students-table.config.list.json';
import * as ACTIVE_STUDENTS_LIST from './components/students-table/students-table.config.active.json';
import * as STUDENTS_COURSE_EXEMPTION from './components/students-dashboard/students-courses/students-courses-exemption/students-courses-exemption-table.config.list.json';
import * as ACTIVE_DECLARED_STUDENTS_LIST from './components/students-table/students-table.config.active-declared.json';

import { StudentsRequestsConfigurationResolver, StudentsRequestsSearchResolver, StudentsDefaultRequestsConfigurationResolver } from './components/students-dashboard/students-requests/students-requests-config.resolver';
import {StudentsThesesConfigurationResolver, StudentsDefaultThesesConfigurationResolver} from './components/students-dashboard/students-theses/students-theses-config.resolver';
import {StudentsCoursesExemptionComponent} from './components/students-dashboard/students-courses/students-courses-exemption/students-courses-exemption.component';
import {CoursesSharedModule} from '../courses/courses.shared';
import {AdvancedFormsModule} from '@universis/forms';
import {RouterModalModule} from '@universis/common/routing';
import {SettingsSharedModule} from '../settings-shared/settings-shared.module';
import {SelectCourseComponent} from '../courses/components/select-course/select-course-component';
import {GradePipe, SharedModule, GradeScale, ServerEventService} from '@universis/common';
import {StudentsOverviewSnapshotComponent} from './components/students-dashboard/students-overview/students-overview-profile/students-overview-snapshot.component';
import {
  StudentsInformationsConfigurationResolver
} from './components/students-dashboard/students-information/students-informations-config.resolver';
import { AddAttachmentComponent } from './components/students-dashboard/students-attachments/add-attachment/add-attachment.component';
import { NgxDropzoneModule } from 'ngx-dropzone';
import {PdfViewerModule} from 'ng2-pdf-viewer';
import { RouterModule } from '@angular/router';

@NgModule({
    imports: [
        AdvancedFormsModule,
        CommonModule,
        TranslateModule,
        RouterModule,
        TooltipModule.forRoot(),
        FormsModule,
        MessagesSharedModule,
        CoursesSharedModule,
        RouterModalModule,
        AdvancedFormsModule,
        SettingsSharedModule,
        SharedModule,
        NgxDropzoneModule,
        PdfViewerModule
    ],
  declarations: [
    StudentsOverviewCoursesComponent,
    StudentsOverviewLastRegistrationComponent,
    StudentsOverviewCurrentgradesComponent,
    StudentsOverviewRequestsComponent,
    StudentsOverviewInternshipsComponent,
    StudentsOverviewScholarshipsComponent,
    StudentsOverviewThesesComponent,
    StudentsOverviewProfileComponent,
    StudentsPreviewFormComponent,
    StudentsMessagesPreviewFormComponent,
    StudentsStatsComponent,
    StudentsOverviewGraduationComponent,
    StudentsOverviewActiveComponent,
    StudentsOverviewRemovalComponent,
    StudentsCoursesExemptionComponent,
    StudentsOverviewSnapshotComponent,
    AddAttachmentComponent
  ],
  exports: [
    StudentsOverviewCoursesComponent,
    StudentsOverviewLastRegistrationComponent,
    StudentsOverviewCurrentgradesComponent,
    StudentsOverviewRequestsComponent,
    StudentsOverviewInternshipsComponent,
    StudentsOverviewScholarshipsComponent,
    StudentsOverviewThesesComponent,
    StudentsOverviewProfileComponent,
    StudentsPreviewFormComponent,
    StudentsMessagesPreviewFormComponent,
    StudentsStatsComponent,
    StudentsOverviewGraduationComponent,
    StudentsOverviewActiveComponent,
    StudentsOverviewRemovalComponent,
    StudentsOverviewSnapshotComponent,
    AddAttachmentComponent
  ],
  providers: [
    StudentsOverviewCoursesComponent,
    StudentsOverviewLastRegistrationComponent,
    StudentsOverviewCurrentgradesComponent,
    StudentsOverviewRequestsComponent,
    StudentsOverviewInternshipsComponent,
    StudentsOverviewScholarshipsComponent,
    StudentsOverviewThesesComponent,
    StudentsStatsComponent,
    StudentsOverviewProfileComponent,
    StudentDefaultTableConfigurationResolver,
    StudentTableSearchResolver,
    StudentTableConfigurationResolver,
    StudentsRequestsConfigurationResolver,
    StudentsRequestsSearchResolver,
    StudentsDefaultRequestsConfigurationResolver,
    StudentsThesesConfigurationResolver,
    StudentsDefaultThesesConfigurationResolver,
    GradePipe,
    StudentsInformationsConfigurationResolver
  ],
  entryComponents: [
    StudentsCoursesExemptionComponent,
    SelectCourseComponent,
    AddAttachmentComponent,
    StudentsOverviewProfileComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class StudentsSharedModule implements OnInit {
  public static readonly StudentsList = STUDENTS_LIST;
  public static readonly ActiveStudentsList = ACTIVE_STUDENTS_LIST;
  public  static readonly  StudentsStudyProgramCoursesList = STUDENTS_COURSE_EXEMPTION;
  public static readonly ActiveDeclaredStudentsList = ACTIVE_DECLARED_STUDENTS_LIST;


  constructor(private _translateService: TranslateService, private serverEvent: ServerEventService) {
    this.ngOnInit().catch(err => {
      console.error('An error occurred while loading students shared module');
      console.error(err);
    });
  }

  async ngOnInit() {
    environment.languages.forEach( language => {
      import(`./i18n/students.${language}.json`).then((translations) => {
        this._translateService.setTranslation(language, translations, true);
      });
    });
  }

}
