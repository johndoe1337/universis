import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { StudentsHomeComponent } from './components/students-home/students-home.component';
import {StudentsRoutingModule} from './students.routing';
import {StudentsSharedModule} from './students.shared';
import { StudentsTableComponent } from './components/students-table/students-table.component';
import {TablesModule} from '@universis/ngx-tables';
import {TranslateModule} from '@ngx-translate/core';
import { StudentsRootComponent } from './components/students-root/students-root.component';
import { StudentsGeneralComponent} from './components/students-dashboard/students-general/students-general.component';
import { StudentsGradesComponent} from './components/students-dashboard/students-grades/students-grades.component';
import { StudentsThesesComponent} from './components/students-dashboard/students-theses/students-theses.component';
import {FormsModule} from '@angular/forms';
import {SharedModule} from '@universis/common';
import { StudentsOverviewComponent } from './components/students-dashboard/students-overview/students-overview.component';
import { StudentsCoursesComponent } from './components/students-dashboard/students-courses/students-courses.component';
import {TooltipModule} from 'ngx-bootstrap';
import { StudentsRegistrationsComponent } from './components/students-dashboard/students-registrations/students-registrations.component';
import { StudentsRequestsComponent } from './components/students-dashboard/students-requests/students-requests.component';
import { StudentsInternshipsComponent } from './components/students-dashboard/students-internships/students-internships.component';
import { StudentsScholarshipsComponent } from './components/students-dashboard/students-scholarships/students-scholarships.component';
import {ElementsModule} from '../elements/elements.module';
import {InternshipsSharedModule} from '../internships/internships.shared';
import {RequestsSharedModule} from '../requests/requests.shared';
import {ScholarshipsSharedModule} from '../scholarships/scholarships.shared';
import { ThesesSharedModule } from './../theses/theses.shared';
import { RegistrationsSharedModule } from '../registrations/registrations.shared';
import { StudentsGraduatedComponent } from './components/students-dashboard/students-graduated/students-graduated.component';
import { StudentsMessagesComponent } from './components/students-dashboard/students-messages/students-messages.component';
import {NgPipesModule} from 'ngx-pipes';
import { StudentsCoursesByGroupComponent } from './components/students-dashboard/students-courses/students-courses-by-group/students-courses-by-group.component';
import {StudentsAdvancedTableSearchComponent} from './components/students-table/students-advanced-table-search.component';
import {AngularDataContext, MostModule} from '@themost/angular';
import { StudentsGraduatedAdvancedTableSearchComponent } from './components/students-dashboard/students-graduated/students-graduated-advanced-table-search.component';
import { AdvancedFormsModule } from '@universis/forms';
import {RegistrarSharedModule} from '../registrar-shared/registrar-shared.module';
import { StudentsDashboardComponent } from './components/students-dashboard/students-dashboard.component';
import {RouterModalModule} from '@universis/common/routing';
import { ReportsSharedModule } from '../reports-shared/reports-shared.module';
import { CoursesSharedModule } from '../courses/courses.shared';
import { StudentsOverviewSuspensionsComponent } from './components/students-dashboard/students-overview/students-overview-suspensions/students-overview-suspensions.component';
import {StudentsRulesComponent} from './components/students-dashboard/students-rules/students-rules.component';
import {GraduationSharedModule} from '@universis/graduation';
import {StudentsGradesByGroupComponent} from './components/students-dashboard/students-grades/students-grades-by-group/students-grades-by-group.component';
import {GradesService} from './services/grades-service/grades-service';
import { StudentsOverviewProgramGroupsComponent } from './components/students-dashboard/students-overview/students-overview-program-groups/students-overview-program-groups.component';
import {StudentsService} from './services/students-service/students.service';
import {StudentsOverviewCounselorsComponent} from './components/students-dashboard/students-overview/students-overview-counselors/students-overview-counselors.component';
import {CounselorsSharedModule} from '../counselors/counselors.shared';
import {StudentsInformationsComponent} from './components/students-dashboard/students-information/students-informations.component';
import {RulesModule} from '../rules';
import { StudentsAttachmentsComponent } from './components/students-dashboard/students-attachments/students-attachments.component';
import { RegisterSharedModule } from '../register/register.shared';
import {StudentProgramGroupsResolver} from './student-program-groups-resolver';
import { StudentsOverviewProgramGroupsPercentComponent } from './components/students-dashboard/students-overview/students-overview-program-groups/students-overview-program-groups-percent/students-overview-program-groups-percent.component';
import { StudentsRegistrationNewComponent } from './components/students-dashboard/students-registrations/students-registration-new/students-registration-new.component';
import { StudentsDegreeCalculationComponent } from './components/students-dashboard/students-rules/students-degree-calculation/students-degree-calculation.component';
import { EditStudentDeclarationComponent } from './components/students-dashboard/edit-student-declaration/edit-student-declaration/edit-student-declaration.component';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    StudentsSharedModule,
    TablesModule,
    StudentsRoutingModule,
    ElementsModule,
    SharedModule,
    FormsModule,
    TooltipModule.forRoot(),
    InternshipsSharedModule,
    RequestsSharedModule,
    ScholarshipsSharedModule,
    ThesesSharedModule,
    NgPipesModule,
    MostModule,
    RegistrationsSharedModule,
    MostModule,
    AdvancedFormsModule,
    RegistrarSharedModule,
    RouterModalModule,
    ReportsSharedModule,
    CoursesSharedModule,
    GraduationSharedModule,
    CounselorsSharedModule,
    RulesModule,
    RegisterSharedModule,
  ],
  declarations: [
    StudentsHomeComponent,
    StudentsTableComponent,
    StudentsGeneralComponent,
    StudentsGradesComponent,
    StudentsThesesComponent,
    StudentsRootComponent,
    StudentsOverviewComponent,
    StudentsCoursesComponent,
    StudentsRegistrationsComponent,
    StudentsRequestsComponent,
    StudentsInternshipsComponent,
    StudentsScholarshipsComponent,
    StudentsGraduatedComponent,
    StudentsMessagesComponent,
    StudentsCoursesByGroupComponent,
    StudentsAdvancedTableSearchComponent,
    StudentsGraduatedAdvancedTableSearchComponent,
    StudentsDashboardComponent,
    StudentsOverviewSuspensionsComponent,
    StudentsRulesComponent,
    StudentsGradesByGroupComponent,
    StudentsOverviewProgramGroupsComponent,
    StudentsOverviewCounselorsComponent,
    StudentsInformationsComponent,
    StudentsAttachmentsComponent,
    StudentsOverviewProgramGroupsPercentComponent,
    StudentsRegistrationNewComponent,
    StudentsDegreeCalculationComponent,
    EditStudentDeclarationComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [
    GradesService,
    StudentsService,
    {
      provide: StudentProgramGroupsResolver,
      useFactory: studentProgramGroupFactory,
      deps: [AngularDataContext]
    }
  ]
})
export class StudentsModule { }

export function studentProgramGroupFactory(_context: AngularDataContext){
  return new StudentProgramGroupsResolver(_context);
}
