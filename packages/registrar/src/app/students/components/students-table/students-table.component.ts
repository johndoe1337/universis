import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import { Observable, Subscription} from 'rxjs';
import {
  AdvancedTableComponent,
  AdvancedTableConfiguration,
  AdvancedTableDataResult
} from '@universis/ngx-tables';
import {AdvancedSearchFormComponent} from '@universis/ngx-tables';
import {ConfigurationService, ErrorService, LoadingService, ModalService, UserActivityService} from '@universis/common';
import { TranslateService } from '@ngx-translate/core';
import {AngularDataContext} from '@themost/angular';
import {AdvancedTableSearchComponent} from '@universis/ngx-tables';
import { ActivatedTableService } from '@universis/ngx-tables';
import {AdvancedRowActionComponent} from '@universis/ngx-tables';
import {ClientDataQueryable} from '@themost/client';
import {ActiveDepartmentService} from '../../../registrar-shared/services/activeDepartmentService.service';
import {ElotConverter} from '@universis/elot-converter';
import {GenitiveFormatter, GenderEnum} from '@universis/grammar';

@Component({
  selector: 'app-students-table',
  templateUrl: './students-table.component.html',
  styles: []
})
export class StudentsTableComponent implements OnInit, OnDestroy {
  private selectedItems = [];
  private dataSubscription: Subscription;
  private paramSubscription: Subscription;
  public isLoading = true;
  public defaultLocale: string;

  @Input() department: any;
  @Input() tableConfiguration: any;
  @Input() searchConfiguration: any;
  @ViewChild('table') table: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  @ViewChild('advancedSearch') advancedSearch: AdvancedTableSearchComponent;
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();
  public recordsTotal: any;
  private fromProgram: any;
  private fromSpecialty: any;


  constructor(
    private _activatedRoute: ActivatedRoute,
    private _userActivityService: UserActivityService,
    private _translateService: TranslateService,
    private _context: AngularDataContext,
    private _activatedTable: ActivatedTableService,
    private _modalService: ModalService,
    private _loadingService: LoadingService,
    private _errorService: ErrorService,
    private _activeDepartmentService: ActiveDepartmentService,
    private _configurationService: ConfigurationService
  ) { }

  ngOnInit() {
    this.dataSubscription = this._activatedRoute.data.subscribe( data => {
      this.isLoading = true;
      this.defaultLocale = this._configurationService.settings
        && this._configurationService.settings.i18n
        && this._configurationService.settings.i18n.defaultLocale;
      this._activatedTable.activeTable = this.table;
      // set search form
      if (data.searchConfiguration) {
        this.search.form = data.searchConfiguration;
        Object.assign(this.search.form, { department: this._activatedRoute.snapshot.data.department.id });
        /*this.search.formComponent.formLoad.subscribe((res: any) => {
          Object.assign(res, { department: this._activatedRoute.snapshot.data.department.id });
        });*/
        this.search.ngOnInit();
      }
      // set table config and recall data
      if (data.tableConfiguration) {
        // set config
        this.table.config = AdvancedTableConfiguration.cast(data.tableConfiguration);
        // reset search text
        this.advancedSearch.text = null;
        // reset table
        this.table.reset(false);
      }
      if (this.table.config) {
        return this._userActivityService.setItem({
          category: this._translateService.instant('Students.StudentTitle'),
          description: this._translateService.instant(this.table.config.title),
          url: window.location.hash.substring(1),
          dateCreated: new Date()
        });
      }
    });
  }

  /**
   * Change study program for selected students
   */
  async changeStudyProgram() {
    try {
      this._loadingService.showLoading();
      let items = await this.getSelectedItems();
      items =  items.filter( (item) => {
        return item.status === 'active';
      });
      // change study program only for students that attend same specialty and study program
      let fromProgram =  [...items.map(item => item.studyProgram)];
      fromProgram = fromProgram.filter( function( item, index, inputArray ) {
        return inputArray.indexOf(item) === index;
      });
      let fromSpecialty =  [...items.map(item => item.studyProgramSpecialty)];
      fromSpecialty = fromSpecialty.filter( function( item, index, inputArray ) {
        return inputArray.indexOf(item) === index;
      });
      if (fromProgram.length !== 1 || fromSpecialty.length !== 1) {
        this.selectedItems = [];
      } else {
        this.selectedItems = items;
        this.fromProgram = fromProgram;
        this.fromSpecialty = fromSpecialty;
      }
      this._loadingService.hideLoading();
      // get active department
      const activeDepartment = await this._activeDepartmentService.getActiveDepartment();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-xl',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          data: {
            'currentProgram': this.fromProgram && this.fromProgram.length ? this.fromProgram[0] : null,
            'currentSpecialty': this.fromSpecialty && this.fromSpecialty.length ? this.fromSpecialty[0] : null,
            'department': activeDepartment.id
          },
          formTemplate: this.selectedItems && this.selectedItems.length > 0 ? 'Students/changeProgram' : null,
          modalTitle: 'Students.ChangeProgramAction.Title',
          description: 'Students.ChangeProgramAction.Description',
          errorMessage: 'Students.ChangeProgramAction.CompletedWithErrors',
          additionalMessage:  this._translateService.instant('Students.ChangeProgramAction.NoItems'),
          refresh: this.refreshAction,
          execute: this.executeChangeProgramAction()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  /**
   * Change study program for selected students
   */
  async changeSpecialty() {
    try {
      this._loadingService.showLoading();
      let message = this._translateService.instant('Students.ChangeSpecialtyAction.NoItems');
      let items = await this.getSelectedItems();
      items =  items.filter( (item) => {
        return item.status === 'active';
      });
      // change study program only for students that attend same study program
      let fromProgram =  [...items.map(item => item.studyProgram)];
      fromProgram = fromProgram.filter( function( item, index, inputArray ) {
        return inputArray.indexOf(item) === index;
      });
      if (fromProgram.length !== 1) {
        this.selectedItems = [];
      } else {
        this.selectedItems = items;
        this.fromProgram = fromProgram;
          // check if study program has specialties
          const specialties = await this._context.model('StudyProgramSpecialties').select('count(id) as total')
            .where('studyProgram').equal(this.fromProgram[0])
            .and('specialty').notEqual(-1).getItem();
          if (specialties && specialties.total === 0) {
            this.selectedItems = [];
            message = this._translateService.instant('Students.ChangeSpecialtyAction.NoSpecialties');
          }

      }
      this._loadingService.hideLoading();
      // get active department
      const activeDepartment = await this._activeDepartmentService.getActiveDepartment();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          data: {
            'currentProgram': this.fromProgram && this.fromProgram.length ? this.fromProgram[0] : null,
            'department': activeDepartment.id
          },
          formTemplate: this.selectedItems && this.selectedItems.length > 0 ? 'Students/changeSpecialty' : null,
          modalTitle: 'Students.ChangeSpecialtyAction.Title',
          description: 'Students.ChangeSpecialtyAction.Description',
          errorMessage: 'Students.ChangeSpecialtyAction.CompletedWithErrors',
          additionalMessage:  message,
          refresh: this.refreshAction,
          execute: this.executeChangeSpecialtyAction()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }
  /**
   * Add study program groups
   */
  async addProgramGroup() {
    try {
      this._loadingService.showLoading();
      let items = await this.getSelectedItems();
      items =  items.filter( (item) => {
        return item.status === 'active';
      });
      let message = this._translateService.instant('Students.AddProgramGroupAction.NoItems');
      // create exams can be made only for course classes that belong to same year and period
      let fromProgram =  [...items.map(item => item.studyProgram)];
      fromProgram = fromProgram.filter( function( item, index, inputArray ) {
        return inputArray.indexOf(item) === index;
      });
      if (fromProgram.length !== 1) {
        this.selectedItems = [];
      } else {
        this.selectedItems = items;
        this.fromProgram = fromProgram;
        // check if study program has groups
        const groups = await this._context.model('ProgramGroups').select('count(id) as total').where('program').equal(this.fromProgram[0])
          .and('groupType/alternateName').equal('simple').and('parentGroup').notEqual(null).getItem();
        if (groups && groups.total === 0) {
          this.selectedItems = [];
          message = this._translateService.instant('Students.AddProgramGroupAction.NoGroups');
        }
      }
      this._loadingService.hideLoading();
      // get active department
      const activeDepartment = await this._activeDepartmentService.getActiveDepartment();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          data: {
            'currentProgram': this.fromProgram && this.fromProgram.length ? this.fromProgram[0] : null,
             'department': activeDepartment.id
          },
          formTemplate: this.selectedItems && this.selectedItems.length > 0 ? 'Students/addStudentGroup' : null,
          modalTitle: 'Students.AddProgramGroupAction.Title',
          description: 'Students.AddProgramGroupAction.Description',
          errorMessage: 'Students.AddProgramGroupAction.CompletedWithErrors',
          additionalMessage:  message,
          refresh: this.refreshAction,
          execute: this.executeAddProgramGroup()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }
  /**
   * Add document request
   */
  async addDocumentRequest() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      this.selectedItems = items;
      this._loadingService.hideLoading();
      // get active department
      const activeDepartment = await this._activeDepartmentService.getActiveDepartment();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          formTemplate: this.selectedItems && this.selectedItems.length > 0 ? 'Students/addDocumentRequest' : null,
          modalTitle: 'Students.AddDocumentRequest.Title',
          description: 'Students.AddDocumentRequest.Description',
          errorMessage: 'Students.AddDocumentRequest.CompletedWithErrors',
          refresh: this.refreshAction,
          execute: this.executeAddDocumentRequest()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }


  /**
   * Executes add document request for students
   */
  executeAddDocumentRequest() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      // get values from modal component
      const component = this._modalService.modalRef.content as AdvancedRowActionComponent;
      const data = component.formComponent.form.formio.data;
      if (data.object == null || data.object === '') {
        this.selectedItems = [];
        result.errors = result.total;
        return observer.next(result);
      }
      this.refreshAction.emit({
        progress: 1
      });

      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // load document request
            data.student = item.id;
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            const action = await this._context.model(`RequestDocumentActions`).save(data);
            result.success += 1;
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  /**
   * Check scholarship rules
   */
  async checkScholarshipRules() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      this.selectedItems = items;
      this._loadingService.hideLoading();
      // get active department
      const activeDepartment = await this._activeDepartmentService.getActiveDepartment();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          data: {
            'department': activeDepartment.id
          },
          formTemplate: this.selectedItems && this.selectedItems.length > 0 ? 'Students/checkScholarshipRules' : null,
          modalTitle: 'Students.CheckScholarshipRulesAction.Title',
          description: 'Students.CheckScholarshipRulesAction.Description',
          errorMessage: 'Students.CheckScholarshipRulesAction.CompletedWithErrors',
          refresh: this.refreshAction,
          execute: this.executeCheckScholarshipRules()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }
  /**
   * Executes check scholarship rules for students
   */
  executeCheckScholarshipRules() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      // get values from modal component
      const component = this._modalService.modalRef.content as AdvancedRowActionComponent;
      const data = component.formComponent.form.formio.data;
      if (data.scholarship == null || data.scholarship === '') {
        this.selectedItems = [];
        result.errors = result.total;
        return observer.next(result);
      }
      this.refreshAction.emit({
        progress: 1
      });
        this._context.model(`Scholarships/${data.scholarship}/checkStudents`).save( this.selectedItems).then(() => {
        result.success = result.total;
        observer.next(result);
      }).catch((err) => {
        this.selectedItems = [];
        result.errors = result.total;
        return observer.next(result);
      });
    });
  }

  /**
   * Executes add program group for students
   */
  executeAddProgramGroup() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      // check if student already has student group
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      // get values from modal component
      const component = this._modalService.modalRef.content as AdvancedRowActionComponent;
      const data = component.formComponent.form.formio.data;
      if (data.group == null || data.group === '') {
        this.selectedItems = [];
        result.errors = result.total;
        return observer.next(result);
      }
      this.refreshAction.emit({
        progress: 1
      });

      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // load program group
            const group = await this._context.model('ProgramGroups').where('id').equal(data.group).getItem();
            data.object = item.id;
            data.groups = [group];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            const exists = await this._context.model('StudentProgramGroups')
              .where('student').equal(item.id).and('programGroup').equal(data.group).select('id').getItem();
            if (!exists) {
              const action = await this._context.model(`UpdateStudentGroupActions`).save(data);
              if (action && action.actionStatus && action.actionStatus.alternateName === 'CompletedActionStatus') {
                result.success += 1;
              } else {
                result.errors += 1;
              }
            } else {
              result.errors += 1;
            }
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }
  /**
   * Executes update of specialty for students
   */
  executeChangeSpecialtyAction() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      // get values from modal component
      const component = this._modalService.modalRef.content as AdvancedRowActionComponent;
      const data = component.formComponent.form.formio.data;
      if (data.specialty == null || data.specialty === '') {
        this.selectedItems = [];
        result.errors = result.total;
        return observer.next(result);
      }
      this.refreshAction.emit({
        progress: 1
      });
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            data.object = item.id;
            data.specialty = data.specialty;
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            const action = await this._context.model(`UpdateSpecialtyActions`).save(data);
            if (action && action.actionStatus && action.actionStatus.alternateName === 'CompletedActionStatus') {
              result.success += 1;
              try {
                await this.table.fetchOne({
                  id: item.id
                });
              } catch (err) {
                //
              }
            } else {
              result.errors += 1;
            }
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }
  /**
   * Executes change program action for students
   */
  executeChangeProgramAction() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      // get values from modal component
      const component = this._modalService.modalRef.content as AdvancedRowActionComponent;
      const data = component.formComponent.form.formio.data;
      if (data.specialty == null || data.studyProgram == null || data.specialty === '' || data.studyProgram === '') {
        this.selectedItems = [];
        result.errors = result.total;
        return observer.next(result);
      }
      this.refreshAction.emit({
        progress: 1
      });
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            data.object = item.id;
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            await this._context.model(`UpdateProgramActions`).save(data);
            result.success += 1;
            // do not throw error while updating row
            // (user may refresh view)
            try {
              await this.table.fetchOne({
                id: item.id
              });
            } catch (err) {
              //
            }
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }


  /**
   * Add student counselor
   */
  async addStudentCounselor() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      // get only active students
      this.selectedItems = items.filter( (item) => {
        return item.status === 'active';
      });
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          formTemplate: this.selectedItems && this.selectedItems.length > 0 ? 'Students/addStudentCounselor' : null,
          modalTitle: 'Students.AddStudentCounselorAction.Title',
          description: 'Students.AddStudentCounselorAction.Description',
          errorMessage: 'Students.AddStudentCounselorAction.CompletedWithErrors',
          refresh: this.refreshAction,
          execute: this.executeAddStudentCounselor()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }


  /**
   * Executes add student counselors
   */
  executeAddStudentCounselor() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      // get values from modal component
      const component = this._modalService.modalRef.content as AdvancedRowActionComponent;
      const data = component.formComponent.form.formio.data;
      if (!data.instructor.hasOwnProperty('id')
        || !data.fromYear.hasOwnProperty('id') || !data.fromPeriod.hasOwnProperty('id')
        || data.startDate == null || data.startDate === '' ) {
        this.selectedItems = [];
        result.errors = result.total;
        return observer.next(result);
      }
      this.refreshAction.emit({
        progress: 1
      });

      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // load document request
            data.student = item.id;
            data.counselor = data.instructor;
            data.active = 1;
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            const action = await this._context.model(`StudentCounselors`).save(data);
            result.success += 1;
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  /**
   * Add related department snapshot
   */
  async addDepartmentSnapshot() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      this.selectedItems = items;
      const activeDepartment = await this._activeDepartmentService.getActiveDepartment();
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          data: {
            'department': activeDepartment.id
          },
          items: this.selectedItems,
          formTemplate: this.selectedItems && this.selectedItems.length > 0 ? 'Students/addDepartmentSnapshot' : null,
          modalTitle: 'Students.AddDepartmentSnapshotAction.Title',
          description: 'Students.AddDepartmentSnapshotAction.Description',
          errorMessage: 'Students.AddDepartmentSnapshotAction.CompletedWithErrors',
          refresh: this.refreshAction,
          execute: this.executeAddDepartmentSnapshot()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }


  /**
   * Executes add student counselors
   */
  executeAddDepartmentSnapshot() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      // get values from modal component
      const component = this._modalService.modalRef.content as AdvancedRowActionComponent;
      const data = component.formComponent.form.formio.data;
      if (data.departmentSnapshot == null) {
        this.selectedItems = [];
        result.errors = result.total;
        return observer.next(result);
      }
      this.refreshAction.emit({
        progress: 1
      });

      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // load document request
            data.id = item.id;
            data.departmentSnapshot = data.departmentSnapshot;
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            const action = await this._context.model(`Students`).save(data);
            result.success += 1;
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }
  async calculateSemester() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      // get only active students
      this.selectedItems = items.filter( (item) => {
        return item.status === 'active';
      });
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Students.CalculateSemesterAction.Title',
          description: 'Students.CalculateSemesterAction.Description',
          refresh: this.refreshAction,
          execute: this.executeCalculateSemester()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }
  /**
   * Calculates semester for selected students
   */
  executeCalculateSemester() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      this.refreshAction.emit({
        progress: 1
      });
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            const studentSemester = await this._context.model(`Students/${item.id}/currentSemester`).getItems();
            if (studentSemester && item.semester !== studentSemester.value) {
              item.semester = studentSemester.value;
              await this._context.model('Students').save(item);
              result.success += 1;
            }
            // do not throw error while updating row
            // (user may refresh view)
            try {
              await this.table.fetchOne({
                id: item.id
              });
            } catch (err) {
              //
            }
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  async convertToElot() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItemsWithLocales();
      const elotMap = new Map<number, {familyName: string, givenName: string,
        fatherName: {isNull: boolean, value: string},
        motherName: {isNull: boolean, value: string}, enLocale: any}>();
      // validate familyName, givenName, fatherName, motherName
      this.selectedItems = items.filter(student => {
        const convertedFamilyName = ElotConverter.convert(student.person.familyName);
        const convertedGivenName = ElotConverter.convert(student.person.givenName);
        const convertedFatherName = {isNull: true, value: null};
        const convertedMotherName = {isNull: true, value: null};
        // father name and mother name are nullable
        // validate before passing to elot converter, which throws error for null input
        if (student.person.fatherName != null) {
          Object.assign(convertedFatherName, {
            isNull: false,
            value: ElotConverter.convert(student.person.fatherName)
          });
        }
        if (student.person.motherName != null) {
          Object.assign(convertedMotherName, {
            isNull: false,
            value: ElotConverter.convert(student.person.motherName)
          });
        }
        // try to find en locale
        const enLocale = student.person.locales.find(locale => locale.inLanguage === 'en');
        // add to map to save conversion for later
        elotMap.set(student.id, {familyName: convertedFamilyName, givenName: convertedGivenName,
          fatherName: convertedFatherName, motherName: convertedMotherName, enLocale: enLocale});
        if (enLocale) {
          return (enLocale.familyName !== convertedFamilyName
              || enLocale.givenName !== convertedGivenName
              || (!convertedFatherName.isNull && enLocale.fatherName !== convertedFatherName.value)
              || (!convertedMotherName.isNull && enLocale.motherName !== convertedMotherName.value));
        }
        return student;
      });
      this._loadingService.hideLoading();
      // open modal
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Students.ConvertToElotAction.Title',
          description: 'Students.ConvertToElotAction.Description',
          errorMessage: 'Students.ConvertToElotAction.CompletedWithErrors',
          refresh: this.refreshAction,
          execute: this.executeConvertToElotAction(elotMap)
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  executeConvertToElotAction(elotMap: Map<number,
    {familyName: string, givenName: string, fatherName: {isNull: boolean, value: string},
    motherName: {isNull: boolean, value: string}, enLocale: any}>) {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      this.refreshAction.emit({
        progress: 1
      });
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            // get converted value from map
            const convertedValues = elotMap.get(item.id);
            // update locales
            if (convertedValues.enLocale) {
              // if locale exists, update it
              Object.assign(convertedValues.enLocale, {
                familyName: convertedValues.familyName,
                givenName: convertedValues.givenName,
                fatherName: !convertedValues.fatherName.isNull ? convertedValues.fatherName.value : null,
                motherName: !convertedValues.motherName.isNull ? convertedValues.motherName.value : null
              });
            } else {
              // create en locale
              if (Array.isArray(item.person.locales)) {
                item.person.locales.push({
                  inLanguage: 'en',
                  familyName: convertedValues.familyName,
                  givenName: convertedValues.givenName,
                  fatherName: !convertedValues.fatherName.isNull ? convertedValues.fatherName.value : null,
                  motherName: !convertedValues.motherName.isNull ? convertedValues.motherName.value : null
                });
              }
            }
            // and save
            await this._context.model('Students').save(item);
            result.success += 1;
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  async changeStatus() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      // get only erased, declared or graduated students
      this.selectedItems = items.filter((item) => {
        return ['erased', 'declared', 'graduated'].includes(item.status);
      });
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Students.ChangeStatusAction.Title',
          description: 'Students.ChangeStatusAction.Description',
          errorMessage: 'Students.ChangeStatusAction.CompletedWithErrors',
          refresh: this.refreshAction,
          execute: this.executeChangeStatus()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }
  
  executeChangeStatus() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      this.refreshAction.emit({
        progress: 1
      });
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            const action = await this._context.model('UpdateStudentStatusActions').save({
              object: item.id,
              studentStatus: {
                alternateName: 'active'
              }
            });
            if (action && action.actionStatus && action.actionStatus.alternateName === 'CompletedActionStatus') {
              result.success += 1;
              try {
                if (this.table.config && this.table.config.model === 'StudentDeclarations') {
                  // adapt item student for fetch one
                  item.student = item.id;
                  await this.table.fetchOne({
                    student: item.student
                  });
                } else {
                  await this.table.fetchOne({
                    id: item.id
                  });
                }
              } catch (err) {
                //
              }
            } else {
              result.errors += 1;
            }
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  async getSelectedItems() {
    let items = [];
    if (this.table && this.table.lastQuery) {
      const lastQuery: ClientDataQueryable = this.table.lastQuery;
      if (lastQuery != null) {
        if (this.table.smartSelect) {
          // get items
          let selectArguments = ['id', 'studentStatus/alternateName as status', 'semester',
            'studyProgram/id as studyProgram', 'studyProgramSpecialty/id as studyProgramSpecialty', 'user'];
          if (this.table.config.model === 'StudentDeclarations' || this.table.config.model === 'StudentSuspensions') {
            selectArguments = ['student/id as id', 'student/studentStatus/alternateName as status', 'student/semester as semester',
              'student/studyProgram/id as studyProgram', 'student/studyProgramSpecialty/id as studyProgramSpecialty', 'student/user as user'];
          }
          // query items
          const queryItems = await lastQuery.select.apply(lastQuery, selectArguments)
            .take(-1)
            .skip(0)
            .getItems();
          if (this.table.unselected && this.table.unselected.length) {
            // remove items that have been unselected by the user
            items = queryItems.filter( item => {
              return this.table.unselected.findIndex( (x) => {
                return x.id === item.id;
              }) < 0;
            });
          } else {
            items = queryItems;
          }
        } else {
          // get selected items only
          items = this.table.selected.map( (item) => {
            return {
              id: item.id,
              status: item.studentStatus,
              semester: item.semester,
              studyProgram : item.studyProgramId,
              studyProgramSpecialty: item.studyProgramSpecialtyId,
              user: item.user
            };
          });
        }
      }
    }
    return items;
  }

  async createUser() {
    try {
      this._loadingService.showLoading();
      // get active department
      const activeDepartment = await this._activeDepartmentService.getActiveDepartment();
      if (!(activeDepartment && activeDepartment.departmentConfiguration && activeDepartment.departmentConfiguration.studentUsernameFormat)) {
        this._loadingService.hideLoading();
        // show informative message to the user about the required settings, if they are not set
        return this._modalService.showInfoDialog(this._translateService.instant('Students.CreateUserAction.SettingsRequired'),
          this._translateService.instant('Students.CreateUserAction.SettingsRequiredHelp'));
      }
      const items = await this.getSelectedItems();
      // get only active students
      this.selectedItems = items.filter(item => item.user == null);
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Students.CreateUserAction.Title',
          description: 'Students.CreateUserAction.Description',
          refresh: this.refreshAction,
          errorMessage: 'Students.CreateUserAction.CompletedWithErrors',
          execute: this.executeCreateUser()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }
  
  async unlinkUser() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      this.selectedItems = items.filter(item => item.user != null);
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Students.UnlinkUserAction.Title',
          description: 'Students.UnlinkUserAction.Description',
          refresh: this.refreshAction,
          errorMessage: 'Students.UnlinkUserAction.CompletedWithErrors',
          execute: this.executeUnlinkUser()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  executeUnlinkUser() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      this.refreshAction.emit({
        progress: 1
      });
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            const unlinkUser = {
              id: item.id,
              user: null,
              $state: 2
            };
            // unlink user
            await this._context.model('Students').save(unlinkUser);
            result.success += 1;
            try {
              await this.table.fetchOne({
                id: item.id
              });
            } catch (err) {
              //
            }
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  executeCreateUser() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      this.refreshAction.emit({
        progress: 1
      });
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            // create user
            await this._context.model(`Students/${item.id}/CreateUser`).save(null);
            result.success += 1;
            try {
              await this.table.fetchOne({
                id: item.id
              });
            } catch (err) {
              //
            }
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  async convertToGenitive() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItemsWithLocales();
      // filter out items that have both genitives already filled
      // also validate father and mother names, so there is no confusion for null values
      this.selectedItems = items.filter(item => item.person
        && ((item.person.fatherName != null && item.person.fatherNameGenitive == null) 
        || (item.person.motherName != null && item.person.motherNameGenitive == null)));
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Students.ConvertToGenitiveAction.Title',
          description: 'Students.ConvertToGenitiveAction.Description',
          refresh: this.refreshAction,
          errorMessage: 'Students.ConvertToGenitiveAction.CompletedWithErrors',
          execute: this.executeConvertToGenitive()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  executeConvertToGenitive() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      this.refreshAction.emit({
        progress: 1
      });
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            let locale: string;
            // modify only blank names
            if (item.person.motherNameGenitive == null && item.person.motherName != null) {
              locale = /^[A-Za-z0-9 ]*$/.test(item.person.motherName) ? 'en' : 'el';
              item.person.motherNameGenitive = GenitiveFormatter.format(item.person.motherName, locale, {
                gender: GenderEnum.Female
              });
            }
            if (item.person.fatherNameGenitive == null && item.person.fatherName != null) {
              locale = /^[A-Za-z0-9 ]*$/.test(item.person.motherName) ? 'en' : 'el';
              item.person.fatherNameGenitive = GenitiveFormatter.format(item.person.fatherName, locale, {
                gender: GenderEnum.Male
              });
            }
            // and save
            await this._context.model('Students').save(item);
            result.success += 1;
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  async getSelectedItemsWithLocales() {
    let items = [];
    if (this.table && this.table.lastQuery) {
      const lastQuery: ClientDataQueryable = this.table.lastQuery;
      if (lastQuery != null) {
        if (this.table.smartSelect) {
          // get items
          let selectArguments = ['id', 'person'];
          let expandExpression = 'person($expand=locales)';
          let shouldAdaptResultsMapping = false;
          if (this.table && this.table.config &&
            (this.table.config.model === 'StudentDeclarations' || this.table.config.model === 'StudentSuspensions')) {
              // reset select arguments
              selectArguments = [''];
              // alter expand expression
              expandExpression = 'student($expand=person($expand=locales))';
              // set to adapt mapping
              shouldAdaptResultsMapping = true;
            }
          // query items
          let queryItems = await lastQuery.select.apply(lastQuery, selectArguments)
            .take(-1)
            .expand(expandExpression)
            .skip(0)
            .getItems();
          if (shouldAdaptResultsMapping) {
            // map items to follow elot action pattern
            queryItems = queryItems.map(queryItem => {
              return {
                id: queryItem.student.id,
                person: queryItem.student.person
              };
            });
          }
          if (this.table.unselected && this.table.unselected.length) {
            // remove items that have been unselected by the user
            items = queryItems.filter( item => {
              return this.table.unselected.findIndex( (x) => {
                return x.id === item.id;
              }) < 0;
            });
          } else {
            items = queryItems;
          }
        } else {
          const itemPromises = this.table.selected.map(async item => await this._context.model('Students')
            .where('id').equal(item.id).select('id, person').expand('person($expand=locales)').getItem());
          items = await Promise.all(itemPromises);
        }
      }
    }
    return items;
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
    this.isLoading = false;
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
  }
}
