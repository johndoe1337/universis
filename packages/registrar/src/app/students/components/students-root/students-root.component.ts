import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import * as STUDENTS_LIST_CONFIG from '../students-table/students-table.config.list.json';
import {TableConfiguration} from '@universis/ngx-tables';
import {cloneDeep} from 'lodash';
import {AppEventService, DIALOG_BUTTONS, ErrorService, LoadingService, ToastService, UserActivityService} from '@universis/common';
import {TranslateService} from '@ngx-translate/core';
import {TemplatePipe} from '@universis/common';
import {Subscription} from 'rxjs';
import { ModalService } from '@universis/common';

@Component({
  selector: 'app-students-root',
  templateUrl: './students-root.component.html',
  styleUrls: ['./students-root.component.scss'],
  providers: [TemplatePipe]
})
export class StudentsRootComponent implements OnInit, OnDestroy {
  public student: any;
  public studentID: any;
  public tabs: any[];
  public actions: any[];
  public config: any;
  public isCreate = false;
  public allowedActions: any[];
  public edit: any;
  private subscription: Subscription;
  private changeSubscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _userActivityService: UserActivityService,
              private _translateService: TranslateService,
              private _template: TemplatePipe,
              private _loadingService: LoadingService,
              private _errorService: ErrorService,
              private _router: Router,
              private _modalService: ModalService,
              private _toastService: ToastService,
              private _appEvent: AppEventService
  ) {
  }

  async ngOnInit() {
    if (this._activatedRoute.snapshot.url.length > 0 &&
      this._activatedRoute.snapshot.url[0].path === 'create') {
      this.isCreate = true;
    } else {
      this.isCreate = false;
    }

    this.tabs = this._activatedRoute.routeConfig.children.filter(route => typeof route.redirectTo === 'undefined');
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.studentID = params.id;
      this.student = await this._context.model('Students')
        .where('id').equal(params.id)
        .expand('person, department, studyProgram')
        .getItem();

      // @ts-ignore
      this.config = cloneDeep(STUDENTS_LIST_CONFIG as TableConfiguration);

      if (this.config.columns && this.student) {
        // get actions from config file
        this.actions = this.config.columns.filter(x => {
          return x.actions;
        })
          // map actions
          .map(x => x.actions)
          // get list items
          .reduce((a, b) => b, 0);

        // filter actions with student permissions
        this.allowedActions = this.actions.filter(x => {
          if (x.role) {
            if (x.role === 'action' || x.role === 'method') {
              if (x.access && x.access.length > 0) {
                let access = x.access;
                access = access.filter(y => {
                  if (y.studentStatuses) {
                    return y.studentStatuses.find(z => z.alternateName === this.student.studentStatus.alternateName);
                  }
                });
                if (access && access.length > 0) {
                  return x;
                }
              } else {
                return x;
              }
            }
          }
        });

        this.edit = this.actions.find(x => {
          if (x.role === 'edit') {
            x.href = this._template.transform(x.href, this.student);
            return x;
          }
        });

        this.actions = this.allowedActions;
        this.actions.forEach(action => {
          if (action.href) {
            if (action.href.includes('${studyProgramId}')) {
              action.href = action.href.replace('${studyProgramId}', '${studyProgram.id}');
            }
            action.href = this._template.transform(action.href, this.student);
          }
          if (action.invoke) {
            action.click = () => {
              if (typeof this[action.invoke] === 'function') { this[action.invoke](); }
            };
          }
        });

        // save user activity for student
        return this._userActivityService.setItem({
          category: this._translateService.instant('Students.StudentTitle'),
          description: this._translateService.instant(this.student.person.givenName + ' ' + this.student.person.familyName),
          url: window.location.hash.substring(1),
          dateCreated: new Date()
        });
      }
    });
    this.changeSubscription = this._appEvent.change.subscribe(async change => {
      if (change && change.model === 'UpdateStudentStatusActions' && this.studentID) {
        this.student = await this._context.model('Students')
          .where('id').equal(this.studentID)
          .expand('person, department, studyProgram')
          .getItem();
          if (this.config && this.config.columns && this.student) {
            // get actions from config file
            this.actions = this.config.columns.filter(x => {
              return x.actions;
            })
              // map actions
              .map(x => x.actions)
              // get list items
              .reduce((a, b) => b, 0);
            // filter actions with student permissions
            this.allowedActions = this.actions.filter(x => {
              if (x.role) {
                if (x.role === 'action' || x.role === 'method') {
                  if (x.access && x.access.length > 0) {
                    let access = x.access;
                    access = access.filter(y => {
                      if (y.studentStatuses) {
                        return y.studentStatuses.find(z => z.alternateName === this.student.studentStatus.alternateName);
                      }
                    });
                    if (access && access.length > 0) {
                      return x;
                    }
                  } else {
                    return x;
                  }
                }
              }
            });
            this.edit = this.actions.find(x => {
              if (x.role === 'edit') {
                x.href = this._template.transform(x.href, this.student);
                return x;
              }
            });
            this.actions = this.allowedActions;
            this.actions.forEach(action => {
              if (action.href) {
                if (action.href.includes('${studyProgramId}')) {
                  action.href = action.href.replace('${studyProgramId}', '${studyProgram.id}');
                }
                action.href = this._template.transform(action.href, this.student);
              }
              if (action.invoke) {
                action.click = () => {
                  if (typeof this[action.invoke] === 'function') { this[action.invoke](); }
                };
              }
            });
          }
      }
    });
  }

  async changeStatus(status: string) {
      const allowedMethodStatuses = ['active', 'declared', 'graduated'];
      // pseudo validate status argument
      if (!allowedMethodStatuses.includes(status)) {
        return;
      }
      // map statuses to titles
      // accept that in the first position is the modal title
      // followed by the modal message, the toast title and finally the toast message
      // of course this can be mapped more analytically
      const statusToHelperMessagesMapper: Map<string, Array<string>> = new Map([
        ['active', [this._translateService.instant('Students.CancelDeclarationInfo.Modal.Title'), 
                    this._translateService.instant('Students.CancelDeclarationInfo.Modal.Message'),
                    this._translateService.instant('Students.CancelDeclarationInfo.Toast.Title'),
                    this._translateService.instant('Students.CancelDeclarationInfo.Toast.Message')]],
        ['declared', [this._translateService.instant('Students.CancelGraduationInfo.Modal.Title'), 
                      this._translateService.instant('Students.CancelGraduationInfo.Modal.Message'),
                      this._translateService.instant('Students.CancelGraduationInfo.Toast.Title'),
                      this._translateService.instant('Students.CancelGraduationInfo.Toast.Message')]],
        ['graduated', [this._translateService.instant('Students.CompleteDeclarationInfo.Modal.Title'), 
                      this._translateService.instant('Students.CompleteDeclarationInfo.Modal.Message'),
                      this._translateService.instant('Students.CompleteDeclarationInfo.Toast.Title'),
                      this._translateService.instant('Students.CompleteDeclarationInfo.Toast.Message')]]
      ]);
      const modalTitle = statusToHelperMessagesMapper.get(status)[0];
      const modalMessage = statusToHelperMessagesMapper.get(status)[1];
      const toastTitle = statusToHelperMessagesMapper.get(status)[2];
      const toastMessage = statusToHelperMessagesMapper.get(status)[3];
      // ensure loading is hidden
      this._loadingService.hideLoading();
      // show modal dialog and proceed
      const dialogResult = await this._modalService.showDialog(modalTitle, modalMessage, DIALOG_BUTTONS.YesNo);
      if (dialogResult === 'no') {
        return;
      }
      // show loading
      this._loadingService.showLoading();
      const changeStatus = {
        object: this.studentID,
        studentStatus: {
          alternateName: status
        }
      };
      // try to update student status
      await this._context.model('UpdateStudentStatusActions').save(changeStatus);
      // hide loading
      this._loadingService.hideLoading();
      // show success toast message
      this._toastService.show(toastTitle, toastMessage);
      // and finally fire app event
      return this._appEvent.change.next({
        model: 'UpdateStudentStatusActions'
      });
  }

  async completeDeclaration() {
    try {
      this._loadingService.showLoading();
      // try to find if an active GraduationRequestAction exists for this student
      const graduationRequestAction = await this._context
        .model('GraduationRequestActions')
        .where('student').equal(this.studentID)
        .and('actionStatus/alternateName').equal('ActiveActionStatus')
        .select('id')
        .getItem();
      if (graduationRequestAction) {
        await this.redirectToGraduationRequest(graduationRequestAction.id);
        return;
      }
      await this.changeStatus('graduated');
    } catch (err) {
      this._errorService.showError(err, {
        continueLink: '.'
      });
    } finally {
      this._loadingService.hideLoading();
    }
  }

  async cancelDeclaration() {
    try {
      this._loadingService.showLoading();
      await this.changeStatus('active');
    } catch (err) {
      this._errorService.showError(err, {
        continueLink: '.'
      });
    } finally {
      this._loadingService.hideLoading();
    }
  }

  async cancelGraduation() {
    try {
      this._loadingService.showLoading();
      await this.changeStatus('declared');
    } catch (err) {
      this._errorService.showError(err, {
        continueLink: '.'
      });
    } finally {
      this._loadingService.hideLoading();
    }
  }

  async redirectToGraduationRequest(request: number | string): Promise<boolean> {
    if (request == null) {
      return;
    }
    // show an informative dialog for redirection
    const modalTitle = this._translateService.instant('Students.CompleteDeclarationInfo.Modal.Title');
    const modalMessage = this._translateService.instant('Students.CompleteDeclarationInfo.Modal.RequestExists');
    // ensure loading is hidden
    this._loadingService.hideLoading();
    const dialogResult = await this._modalService.showDialog(modalTitle, modalMessage, DIALOG_BUTTONS.YesNo);
    if (dialogResult === 'no') {
      return;
    }
    // navigate to graduation request page
    return this._router.navigate(['/requests', request, 'edit', {outlets: {as: 'GraduationRequestAction'}}]);
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.changeSubscription) {
      this.changeSubscription.unsubscribe();
    }
  }
}
