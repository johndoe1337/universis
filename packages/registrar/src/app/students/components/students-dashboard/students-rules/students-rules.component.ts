import {Component, OnDestroy, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ActivatedRoute} from '@angular/router';
import {AppEventService, DIALOG_BUTTONS, ErrorService, LoadingService, ModalService} from '@universis/common';
import {Subscription} from 'rxjs';
import {TranslateService} from '@ngx-translate/core';
import {GraduationProgressComponent} from '@universis/graduation';


@Component({
  selector: 'app-students-rules',
  templateUrl: './students-rules.component.html'
})

export class StudentsRulesComponent implements OnInit, OnDestroy {

  private subscription: Subscription;
  private changeSubscription: Subscription;
  public student: any;
  public studentRules: any;
  public requirementsAreMet: boolean = null;
  @ViewChild('graduationProgress') graduationProgress: GraduationProgressComponent;
  private fragmentSubscription: Subscription;

  constructor(private _context: AngularDataContext, private _activatedRoute: ActivatedRoute,
              private _loadingService: LoadingService,
              private _translateService: TranslateService,
              private _modalService: ModalService,
              private _errorService: ErrorService,
              private _appEvent: AppEventService) {
  }

  async ngOnInit() {
    this._loadingService.showLoading();
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.student = params.id;
      // check if student has personal graduation rules
      this.studentRules = await this._context.model(`Students/${this.student}/StudentGraduationRules`).getItems();
    });
    this.fragmentSubscription = this._activatedRoute.fragment.subscribe(async (fragment) => {
      if (fragment && fragment === 'reload') {
        this.studentRules = await this._context.model(`Students/${this.student}/StudentGraduationRules`).getItems();
        this.graduationProgress.ngOnChanges
        (<any>{student: {currentValue: this.student, previousValue: this.student, firstChange: false}});
      }
    });
    this.changeSubscription = this._appEvent.change.subscribe(async (event) => {
      if (event && (event.model === 'StudentGraduationRules')) {
        // reload
        this.studentRules = await this._context.model(`Students/${this.student}/StudentGraduationRules`).getItems();
        this.graduationProgress.ngOnChanges(
          <any>{student: {currentValue: this.student, previousValue: this.student, firstChange: false}});
      }
    });
  }


  async resetRules() {
    try {
      const title = this._translateService.instant('Students.RemoveRules');
      const message = this._translateService.instant('Students.RemoveRulesMessage');
      const dialogResult = await this._modalService.showDialog(title, message, DIALOG_BUTTONS.YesNo);
      if (dialogResult === 'no') {
        return;
      }
      this._loadingService.showLoading();
      // get rules and add delete $state
      const rules = await this._context.model(`Students/${this.student}/StudentGraduationRules`).getItems();
      rules.map(rule => {
        rule.$state = 4;
        return rule;
      });
      // and finally reset rules
      await this._context.model(`Students/${this.student}/StudentGraduationRules`).save(rules);
      this._appEvent.change.next({
        model: 'StudentGraduationRules',
        target: this.student
      });
      this._loadingService.hideLoading();
    } catch (err) {
      this._loadingService.hideLoading();
      return this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  setRequirements(areMet: boolean) {
    this.requirementsAreMet = areMet;
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.student = null;
      this.subscription.unsubscribe();
    }
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
    if (this.changeSubscription) {
      this.student = null;
      this.changeSubscription.unsubscribe();
    }
  }
}
