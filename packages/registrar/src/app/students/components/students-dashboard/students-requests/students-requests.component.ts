import {Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {AngularDataContext} from '@themost/angular';
import {LoadingService} from '@universis/common';
import * as STUDENTS_REQUESTS_LIST_CONFIG from './students-requests.config.list.json';
import { ActivatedTableService } from '@universis/ngx-tables';
// tslint:disable-next-line:max-line-length
import {
  AdvancedTableComponent,
  AdvancedTableConfiguration,
  AdvancedTableDataResult
} from '@universis/ngx-tables';
import { Subscription } from 'rxjs';
import { ErrorService, ModalService } from '@universis/common';
import {AdvancedSearchFormComponent} from '@universis/ngx-tables';

@Component({
  selector: 'app-students-requests',
  templateUrl: './students-requests.component.html',
  styleUrls: ['./students-requests.component.scss']
})
export class StudentsRequestsComponent implements OnInit, OnDestroy {

  public readonly config: AdvancedTableConfiguration = <AdvancedTableConfiguration>STUDENTS_REQUESTS_LIST_CONFIG;

  public recordsTotal: any;
  private dataSubscription: Subscription;
  @ViewChild('requests') requests: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  // @ViewChild('advancedSearch') advancedSearch: AdvancedTableSearchComponent;

  studentID: any = this._activatedRoute.snapshot.params.id;
  private fragmentSubscription: Subscription;
  @Input() reload: EventEmitter<any> = new EventEmitter<any>();
  @Input() tableConfiguration: any;
  @Input() searchConfiguration: any;
  private subscription: Subscription;
  // public model: any;

  constructor(private _element: ElementRef,
              private _activatedRoute: ActivatedRoute,
              private _translate: TranslateService,
              private _loadingService: LoadingService,
              private _context: AngularDataContext,
              private _activatedTable: ActivatedTableService,
              private _errorService: ErrorService,
              private _modalService: ModalService) { }

  async ngOnInit() {

    this._activatedTable.activeTable = this.requests;

    this._loadingService.showLoading();
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.requests.query = this._context.model('StudentRequestActions')
        .where('student').equal(params.id)
        .expand('agent')
        .orderByDescending('dateCreated')
        .prepare();
      this._loadingService.hideLoading();

      this.requests.config = AdvancedTableConfiguration.cast(STUDENTS_REQUESTS_LIST_CONFIG);
      this.requests.fetch();


    // do reload by using hidden fragment e.g. /requests#reload
    this.fragmentSubscription = this._activatedRoute.fragment.subscribe(fragment => {
      if (fragment && fragment === 'reload') {
        this.requests.fetch(true);
      }
    });

    this.dataSubscription = this._activatedRoute.data.subscribe(data => {
      if (data.tableConfiguration) {
        this.requests.config = data.tableConfiguration;
        this.requests.ngOnInit();
      }
      if (data.searchConfiguration) {
        this.search.form = data.searchConfiguration;
        this.search.ngOnInit();
      }
     });
    });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }

    if (this.subscription) {
      this.subscription.unsubscribe();
    }

    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }
}

