import {Component, OnDestroy, OnInit} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ActivatedRoute} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-students-general',
  templateUrl: './students-general.component.html'
})
export class StudentsGeneralComponent implements OnInit, OnDestroy {
  public model: any;
  private subscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
              private _translate: TranslateService,
              private _context: AngularDataContext) {
  }

  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.model = await this._context.model('Students')
        .where('id').equal(params.id)
        .expand('user,graduationType,studyProgram,department,inscriptionModeCategory,person($expand=nationality,familyStatus,homeAddressRegion,' +
          'temporaryAddressRegion,citizenRegistrarRegion, maleRegistrarRegion, birthPlaceRegion, identityType, insuranceProvider),militaryStatus')
        .getItem();
      if(this.model.studentStatus && this.model.studentStatus.alternateName === 'suspended'){
          this.model = {...this.model, suspensionDetails: await this._context.model('StudentSuspensions').where('student').equal(this.model.id).orderByDescending('identifier').thenByDescending('suspensionDate').getItem()};
      }
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
