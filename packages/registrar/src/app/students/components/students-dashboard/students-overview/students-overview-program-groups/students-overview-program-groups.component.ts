import { Component, OnDestroy, OnInit } from '@angular/core';
import {AppEventService, ToastService} from '@universis/common';
import { Subscription } from 'rxjs';
import { StudentsService } from './../../../../services/students-service/students.service';
import {TranslateService} from '@ngx-translate/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-students-overview-program-groups',
  templateUrl: './students-overview-program-groups.component.html'
})
export class StudentsOverviewProgramGroupsComponent implements OnInit, OnDestroy {

  public studentId: number;
  public programGroups: Array<any> = [];
  private appEventSubscription: Subscription;
  private routeSubscription: Subscription;

  constructor(
    private readonly _studentService: StudentsService,
    private readonly _appEventService: AppEventService,
    private readonly _toastService: ToastService,
    private readonly _translateService: TranslateService,
    private readonly _activatedRoute: ActivatedRoute
  ) { }

  async ngOnInit() {
    this.routeSubscription = this._activatedRoute.parent.params.subscribe(x => {
      this.studentId = x.id ? x.id : null;
      this.fetchProgramGroup();
    });
    this.appEventSubscription = this._appEventService.change.subscribe((data) => {
      if (data && data.model === "UpdateStudentGroupActions" ) {
          this.fetchProgramGroup(data.target);
      }
      if (data && this.studentId && data.model === "StudentProgramGroups"){
        this.fetchProgramGroup();
      }
    });
  }

  ngOnDestroy() {
    if (this.appEventSubscription) {
      this.appEventSubscription.unsubscribe();
    }
    if(this.routeSubscription){
      this.routeSubscription.unsubscribe();
    }
  }

  async fetchProgramGroup(postData: any = null) {
    if(!this.programGroups){
      this.programGroups = [];
    }
    try {
      let items = await this._studentService.getProgramGroups(this.studentId);
      if (!postData){
        this.programGroups = items;
        return;
      }
      if(postData && postData.actionStatus && postData.actionStatus.alternateName && postData.actionStatus.alternateName !== 'CompletedActionStatus' && postData.groups && postData.groups.length){
        let diff = postData.groups.map(x=> x.id).filter(x => !items.map(y=> y.programGroup.id).includes(x));
        // this is an unrealistic case
        if(diff && diff.length === 0){
          this._toastService.show("Students.ProgramGroupsOverview.GeneralError.Title", "Students.ProgramGroupsOverview.GeneralError.Description", true, 10000);
        }
        this._toastService.show(this._translateService.instant('Students.ProgramGroupsOverview.FailedToAdd.Title'), postData.description, true, 10000);
      } else if(postData.actionStatus.alternateName === 'CompletedActionStatus') {
        this._toastService.show(this._translateService.instant('Students.ProgramGroupsOverview.AddedSuccessfully.Title'), this._translateService.instant('Students.ProgramGroupsOverview.AddedSuccessfully.Description')+  postData.groups.map(x=> x.name).join(", "), true, 10000);
      }
      this.programGroups = items;
    } catch (err) {
      console.error('err: ', err);
      this.programGroups = [];
    }
  }
}
