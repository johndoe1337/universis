import {
  ChangeDetectorRef,
  Component,
  ElementRef,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs';
import {PdfViewerComponent} from 'ng2-pdf-viewer';
import { AppEventService } from '@universis/common';

@Component({
  selector: 'app-students-overview-profile',
  templateUrl: './students-overview-profile.component.html',
  styleUrls: ['./students-overview-profile.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class StudentsOverviewProfileComponent implements OnInit, OnDestroy, OnChanges {

  public student;
  @Input() studentId: number;
  public imageLoading = true;
  public imageExists = false;
  //  Value to indicate whether the message send form should be visible or not (This is passed to child component)
  public showMessageForm = false;
  @ViewChild('studentPhoto') studentPhoto: ElementRef;
  @ViewChild('studentPhotoPdfViewer') studentPhotoPdfViewer: PdfViewerComponent;
  private fragmentSubscription: Subscription;
  private changeSubscription: Subscription;
  public mimeType: string;
  public imageSrc: string;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _ref: ChangeDetectorRef,
              private _appEvent: AppEventService) { }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.studentId) {
      if (changes.studentId.currentValue == null) {
        this.student = null;
        return;
      }
      // hide student photo
      if(this.studentPhoto) {
        this.studentPhoto.nativeElement.classList.add('d-none');
        this.studentPhoto.nativeElement.src = '';
      }

      this._context.model('Students')
        .where('id').equal(changes.studentId.currentValue)
        .expand('person($expand=gender), department, studyProgram, user')
        .getItem()
        .then((value) => {
          this.student = value;
          // get student photo
          const headers = new Headers({});
          const url = `Students/${changes.studentId.currentValue}/photo`;
          // get service headers
          const serviceHeaders = this._context.getService().getHeaders();
          // manually assign service headers
          Object.keys(serviceHeaders).forEach((key) => {
            if (Object.prototype.hasOwnProperty.call(serviceHeaders, key)) {
              headers.set(key, serviceHeaders[key]);
            }
          });
          const imageUrl = this._context.getService().resolve(url);
          // get image blob
          return fetch(imageUrl, {
            method: 'GET',
            headers: headers,
            credentials: 'include'
          }).then((response) => {
            if (response.ok === false) {
              this.imageLoading = false;
              this.imageExists = false;
            }
            response.blob().then(blob => {
              //  this.studentPhoto = window.URL.createObjectURL(blob);
              if (blob.size === 0) {
                this.imageLoading = false;
                this.imageExists = false;
              } else {
                const reader = new FileReader();
                reader.readAsDataURL(blob);
                reader.onload = _event => {
                  this.imageExists = true;
                  this.imageLoading = false;
                  if(reader.result.includes("image/png") || reader.result.includes("image/jpeg")){
                    this.mimeType = reader.result.includes("image/png") ? "image/png" : "image/jpeg";
                    this._ref.detectChanges();
                    this.imageSrc = reader.result;
                    this.studentPhoto.nativeElement.src = this.imageSrc;
                    this.studentPhoto.nativeElement.classList.remove('d-none');
                  } else if(reader.result.includes("application/pdf")){
                    this.mimeType = "application/pdf";
                    this._ref.detectChanges();
                    this.imageSrc = reader.result;
                    this.studentPhotoPdfViewer.src = this.imageSrc;
                    this.studentPhotoPdfViewer.ngOnInit();
                    this.studentPhotoPdfViewer.afterLoadComplete.subscribe(() => {
                      this.studentPhotoPdfViewer.pdfViewer.Container.nativeElement.classList.add('overflow-hidden', 'position-relative');
                    });
                    this._ref.detectChanges();
                  }
                };
              }
            });
          });
        });
    }
  }


  ngOnInit() {
    this.fragmentSubscription = this._activatedRoute.fragment.subscribe(async (fragment) => {
      if (fragment && fragment === 'reload') {
        this.student =  await this._context.model('Students')
          .where('id').equal(this.studentId)
          .expand('person($expand=gender), department, studyProgram, user')
          .getItem();
      }
    });
    this.changeSubscription = this._appEvent.change.subscribe(async change => {
      if (change && change.model === 'UpdateStudentStatusActions') {
        this.student = await this._context.model('Students')
          .where('id').equal(this.studentId)
          .expand('person($expand=gender), department, studyProgram, user')
          .getItem();
      }
    });
  }

  /*
   *  This Functions toggles Message Send Form Visibility
   */
  enableMessages() {
    this.showMessageForm = !this.showMessageForm;
  }

  /*
   *  This functions is used to receive message sent status from child component
   */
  onsuccesfulSend(succesful: boolean) {
    //  Then toggles message form visibility accordingly
    this.showMessageForm = !succesful;
  }

  ngOnDestroy(): void {
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
    if (this.changeSubscription) {
      this.changeSubscription.unsubscribe();
    }
  }
}
