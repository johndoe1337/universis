import {Component, OnInit, ViewChild} from '@angular/core';
import * as STUDENTS_GRADUATED_LIST_CONFIG from './students-graduated.config.json';
import {AdvancedTableComponent, AdvancedTableConfiguration} from '@universis/ngx-tables';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';

@Component({
  selector: 'app-students-graduated',
  templateUrl: './students-graduated.component.html',
  styleUrls: ['./students-graduated.component.scss']
})
export class StudentsGraduatedComponent implements OnInit {

  @ViewChild('students') students: AdvancedTableComponent;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) { }

  async ngOnInit() {
    this.students.query = this._context.model(`Students`)
      .asQueryable()
      .where('studentStatus/alternateName').equal('declared')
      .prepare();

    this.students.config = AdvancedTableConfiguration.cast(STUDENTS_GRADUATED_LIST_CONFIG);
  }

}
