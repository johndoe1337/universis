import {Component, OnDestroy, OnInit} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ActivatedRoute} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {Subscription} from 'rxjs';


@Component({
  selector: 'app-students-courses',
  templateUrl: './students-courses.component.html',
  styleUrls: ['./students-courses.component.scss']
})

export class StudentsCoursesComponent implements OnInit, OnDestroy {
  public studentId;
  public showMore = false;
  private subscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
              private _translate: TranslateService,
              private _context: AngularDataContext) {
  }

  ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.studentId = params.id;
    });
  }

  ngOnDestroy(): void {

    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
