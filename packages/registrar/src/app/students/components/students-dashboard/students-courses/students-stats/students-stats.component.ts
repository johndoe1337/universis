import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {Subscription} from 'rxjs';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-students-stats',
  templateUrl: './students-stats.component.html',
  styleUrls: ['./students-stats.component.scss']
})
export class StudentsStatsComponent implements OnInit, OnDestroy  {

  public countCourses = 0;
  public countECTS = 0;
  public courses: any;
  @Input() studentId: number;
  @Input() showMore: boolean;
  private subscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) { }

  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.studentId = params.id;
      this.courses = await this._context.model('Students/' + this.studentId + '/courses')
        .select('count(id) as total', 'sum(ects) as ects', 'courseType')
        .expand('courseType')
        .groupBy('courseType')
        .where('isPassed').equal(1)
        .getItems();


      if (this.courses && this.courses.length > 0) {
        this.countCourses = this.courses.reduce((acc, course) => acc + course.total, 0);
        this.countECTS = this.courses.reduce((acc, course) => acc + course.ects, 0);
      }
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
