import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentsScholarshipsComponent } from './students-scholarships.component';

describe('StudentsScholarshipsComponent', () => {
  let component: StudentsScholarshipsComponent;
  let fixture: ComponentFixture<StudentsScholarshipsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentsScholarshipsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentsScholarshipsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
