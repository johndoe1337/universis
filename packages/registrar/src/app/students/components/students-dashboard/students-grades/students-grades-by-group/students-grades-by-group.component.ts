import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ActivatedRoute, Router} from '@angular/router';
import { AdvancedSearchFormComponent } from '@universis/ngx-tables';
import * as SEARCH_CONFIG from './student-grades-table.search.list.json';
import {AppEventService, ErrorService, LoadingService, ModalService, TemplatePipe} from '@universis/common';
import { Subscription} from 'rxjs';
import {AdvancedFilterValueProvider} from '@universis/ngx-tables';

@Component({
  selector: 'app-students-grades-by-group',
  templateUrl: './students-grades-by-group.component.html'
})
export class StudentsGradesByGroupComponent implements OnInit , OnDestroy {
  public model: any;
  public data: any;
  public groups = [];
  public studentId;
  private subscription: Subscription;
  private eventSubscription: Subscription;

  @ViewChild('search') search: AdvancedSearchFormComponent;
  collapsed = true;

  public groupTypes = [
    {
      key: 'year',
      prop: 'courseExam.year.id',
      name: 'Students.AcademicYear'
    }
  ];
  public selectedGroupType = this.groupTypes[0];
  private searchExpression = '(student eq ${student} and (indexof(courseExam/name, \'${text}\') ge 0 or indexof(courseExam/course/displayCode, \'${text}\') ge 0))';
  private allStudentGrades: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _template: TemplatePipe,
              private _modalService: ModalService,
              private _loadingService: LoadingService,
              private _router: Router,
              private _errorService: ErrorService,
              private _advancedFilter: AdvancedFilterValueProvider,
              private _appEvent: AppEventService
              ) {
  }

  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      try {
        await this.fetch(params.id);
        this.eventSubscription = this._appEvent.change.subscribe(async change => {
          if (change && change.model === 'StudentGradeRemarkActions') {
            await this.fetch(params.id);
          }
        });
      } catch (error) {
        this._loadingService.hideLoading();
        this._errorService.showError(error, {
          continueLink: '.'
        });
      }
    });
  }

  async fetch(student) {
    this._loadingService.showLoading();
    this.model = await this._context.model('StudentGrades')
      .where('student').equal(student)
      .expand('courseClass,status,remark,courseExam($expand=course,year,examPeriod,instructors($expand=instructor))')
      .take(-1)
      .getItems();
    this.allStudentGrades = this.model;
    this.selectedGroupType = this.groupTypes[0];
    this.search.form = SEARCH_CONFIG;
    this.search.ngOnInit();
    this.data = this.model;
    this._loadingService.hideLoading();
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.eventSubscription) {
      this.eventSubscription.unsubscribe();
    }
  }

  async onSearchKeyDown(event: any) {
    const q = this._context.model('StudentGrades').asQueryable();

    const searchText = (<HTMLInputElement>event.target);
    if (searchText && event.keyCode === 13) {
      this.subscription = this._activatedRoute.params.subscribe(async (params) => {
        this.studentId = params.id;
        if (searchText && searchText.value && this.searchExpression) {
          // validate search text in double quotes
          if (/^"(.*?)"$/.test(searchText.value)) {
            q.setParam('$filter',
              this._template.transform(this.searchExpression, {
                student: this.studentId,
                text: searchText.value.replace(/^"|"$/g, '')
              }));
          } else {
            // try to split words
            const words = searchText.value.split(' ');
            // join words to a single filter
            const filter = words.map(word => {
              return this._template.transform(this.searchExpression, {
                student: this.studentId,
                text: word
              });
            }).join(' and ');
            // set filter
            q.setParam('$filter', filter);
          }
        } else {
          // use only student filter
          q.setParam('$filter', 'student eq ' + this.studentId);
        }
        this.model = await q.expand('courseClass,status,remark,courseExam($expand=course,year,examPeriod,instructors($expand=instructor))')
          .take(-1).getItems();
        this.data = this.model;
      });
    }
  }

  async advancedSearch(event?: any) {
    if (Array.isArray(this.search.form.criteria)) {
      const expressions = [];
      const filter = this.search.filter;
      const values = this.search.formComponent.formio.data;
      this.subscription = this._activatedRoute.params.subscribe(async (params) => {
        this.studentId = params.id;
        this.search.form.criteria.forEach((x) => {
          if (Object.prototype.hasOwnProperty.call(filter, x.name)) {
            // const nameFilter = this.convertToString(filter[x.name]);
            if (filter[x.name] !== 'undefined') {
              expressions.push(this._template.transform(x.filter, Object.assign({
                value: filter[x.name]
              }, values)));
            }
          }
        });
        expressions.push('(student eq ' + this.studentId + ')');

        // create client query
        const q = this._context.model('StudentGrades').asQueryable();
        q.setParam('filter', expressions.join(' and '));

        this.model = await q.expand('courseClass,status,remark,courseExam($expand=course,year,examPeriod,instructors($expand=instructor))')
          .take(-1).getItems();
        this.data = this.model;
      });
    }
  }
}
