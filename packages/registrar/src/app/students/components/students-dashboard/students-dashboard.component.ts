import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-students-dashboard',
  templateUrl: './students-dashboard.component.html',
  styleUrls: ['./students-dashboard.component.scss']
})
export class StudentsDashboardComponent implements OnInit {

  // public studentId;
  // public showMore = true;
  public model: any;
  public tabs: any[];

  constructor(private _activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.tabs = this._activatedRoute.routeConfig.children.filter( route => typeof route.redirectTo === 'undefined' );
  }
}
